<?php
Route::group(['middleware' => 'checkSession'], function () {
    /*
    |--------------------------------------------------------------------------
    | Rooting For Control Panel
    |--------------------------------------------------------------------------
    |
    */

    Route::get('/','dashboardController@index');
    Route::get('/controlPanel','controlPanelController@index');
    Route::get('/controlPanel/tabCustomer','controlPanelController@tabCustomer');
    Route::get('/controlPanel/tabRoom','controlPanelController@tabRoom');
    Route::get('/controlPanel/tabAccount','controlPanelController@tabAccount');
    Route::get('/controlPanel/tabExtraItem','controlPanelController@tabExtraItem');

    Route::get('/controlPanel/dataTablesCustomer','controlPanelController@dataTablesCustomer');
    Route::get('/controlPanel/showRoom','controlPanelController@showRoom');
    Route::get('/controlPanel/showListRoom','controlPanelController@showListRoom');
    Route::get('/controlPanel/showAddRoom','controlPanelController@showAddRoom');
    Route::get('/controlPanel/showRoomType','controlPanelController@showRoomType');
    Route::get('/controlPanel/showListRoomType','controlPanelController@showListRoomType');
    Route::get('/controlPanel/getRoomUpdate','controlPanelController@getRoomUpdate');
    Route::get('/controlPanel/listAccount','controlPanelController@listAccount');
    Route::get('/controlPanel/typeExtraItem','controlPanelController@typeExtraItem');
    Route::get('/controlPanel/showListTypeExtraItems','controlPanelController@showListTypeExtraItems');
    Route::get('/controlPanel/extraItem','controlPanelController@extraItem');
    Route::get('/controlPanel/showListExtraItems','controlPanelController@showListExtraItems');


    Route::get('/controlPanel/tabRoomRate','controlPanelController@tabRoomRate')->middleware('satpam');
    Route::get('/controlPanel/tableListSpecialPrice','controlPanelController@tableListSpecialPrice');
    Route::get('/controlPanel/datatablesSpecialPrice','controlPanelController@datatablesSpecialPrice');

    /*
    |--------------------------------------------------------------------------
    | Rooting For Reservation
    |--------------------------------------------------------------------------
    |
    */
    Route::get('/reservation','reservationController@index');
    Route::get('/reservation/addReservation/','reservationController@addReservation');
    Route::get('/reservation/addReservationRoom','reservationController@addReservationRoom');
    Route::get('/reservation/editReservation','reservationController@editReservation');
    Route::get('/reservation/deleteReservationRoom','reservationController@deleteReservationRoom');
    Route::get('/reservation/deleteReservation','reservationController@deleteReservation');

    Route::get('/reservation/getRoomNumber','reservationController@getRoomNumber');
    Route::get('/reservation/addDummyRoomReservation','reservationController@addDummyRoomReservation');
    Route::get('/reservation/showDummyRoomReservation','reservationController@showDummyRoomReservation');
    Route::get('/reservation/saveReservation','reservationController@saveReservation');
    Route::get('/reservation/deleteDummyReservation','reservationController@deleteDummyReservation');
    Route::get('/reservation/getSumRoomBillDummyReservation','reservationController@getSumRoomBillDummyReservation');
    Route::get('/reservation/serachCustomer','reservationController@serachCustomer');

    /*
    |--------------------------------------------------------------------------
    | Rooting For room
    |--------------------------------------------------------------------------
    |
    */
    Route::get('/room','roomController@index');
    Route::post('/room/addRoomType','roomController@addRoomType');
    Route::get('/room/addSpecialPrice','roomController@addSpecialPrice');
    Route::get('/room/addRoom','roomController@addRoom');
    Route::post('/room/editTypeRoom','roomController@editTypeRoom');
    Route::get('/room/editSpecialPrice','roomController@editSpecialPrice');
    Route::get('/room/editRoom','roomController@editRoom');
    Route::get('/room/deleteRoom','roomController@deleteRoom');
    Route::post('/room/deleteRoomType','roomController@deleteRoomType');
    Route::get('/room/deleteSpecialPrice','roomController@deleteSpecialPrice');


    /*
    |--------------------------------------------------------------------------
    | Routing For extra items
    |--------------------------------------------------------------------------
    |
    */
    Route::get('/extraItem/addTypeExtraItem','extraItemController@addTypeExtraItem');
    Route::get('/extraItem/addExtraItem','extraItemController@addExtraItem');

    /*
    |--------------------------------------------------------------------------
    | Rooting For customer
    |--------------------------------------------------------------------------
    |
    */
    Route::get('/customer','customerController@index');
    Route::get('/customer/updateCusomer','customerController@updateCusomer');
    Route::get('/customer/addCustomer','customerController@addCustomer');

    /*
    |--------------------------------------------------------------------------
    | Rooting For customer
    |--------------------------------------------------------------------------
    |
    */
    Route::get('/account/addAccount','accountController@addAccount');
    Route::get('/account/deleteAccount','accountController@deleteAccount');

    //


    /*
    |--------------------------------------------------------------------------
    | Rooting For payment
    |--------------------------------------------------------------------------
    |
    */
    Route::get('/payment','paymentController@index');
    Route::get('/payment/showListReservation','paymentController@showListReservation');
    Route::get('/payment/reservationDetail/{id}','paymentController@reservationDetail');
    Route::get('/payment/processPayment','paymentController@processPayment');
    Route::get('/payment/showAllListReservation','paymentController@showAllListReservation');
    Route::get('/payment/addPayment/','paymentController@addPayment');
    Route::get('/payment/getDeposit/','paymentController@getDeposit');
    Route::get('/payment/getOutstanding/','paymentController@getOutstanding');
    Route::get('/payment/historyPaymentPerReservation/','paymentController@historyPaymentPerReservation');
    Route::get('/payment/getTotalReceived/','paymentController@getTotalReceived');


    /*
    |--------------------------------------------------------------------------
    | Rooting For reservation list
    |--------------------------------------------------------------------------
    |
    */
    Route::get('/reservationList','reservationListController@index');
    Route::get('/reservationList/checkin/{id}','reservationListController@checkin');
    Route::get('/reservationList/processCheckin/{id}','reservationListController@processCheckin');
    Route::get('/reservationList/processCheckout/{id}','reservationListController@processCheckout');
    Route::get('/reservationList/list','reservationListController@list');
    Route::get('/reservationList/addExtraItem/{id}','reservationListController@addExtraItem');
    Route::get('/reservationList/showOptionExtraItem','reservationListController@showOptionExtraItem');
    Route::get('/reservationList/getPriceExtraItem','reservationListController@getPriceExtraItem');
    Route::get('/reservationList/processAddItem','reservationListController@processAddItem');
    Route::get('/reservationList/activeExtraItem','reservationListController@activeExtraItem');
    Route::get('/reservationList/deleteReservationExtraItem','reservationListController@deleteReservationExtraItem');



    // Route::get('/payment/showCustomerReservation','paymentController@showCustomerReservation');

    /*
    |--------------------------------------------------------------------------
    | Rooting For Calendar
    |--------------------------------------------------------------------------
    |
    */
    Route::get('/calendar','calendarController@index');
    Route::get('/calendar/roomData','calendarController@roomData');
    Route::get('/calendar/eventData','calendarController@eventData');
    // Route::get('/calendar/test','calendarController@test');

    /*
    |--------------------------------------------------------------------------
    | Routing For Detail
    |--------------------------------------------------------------------------
    | berisi :
    |       Invoice
    |       summary
    */
    Route::get('/detail/invoice/{id}','detailController@invoice');


    /*
    |--------------------------------------------------------------------------
    | Routing For Report
    |--------------------------------------------------------------------------
    */
    Route::get('/report','reportController@index');
    Route::get('/report/tabRevenue','reportController@tabRevenue');
    Route::get('/report/tableRevenue','reportController@tableRevenue');


    Route::get('/report/tabCheckin','reportController@tabCheckin');
    Route::get('/report/tableCheckin','reportController@tableCheckin');


    Route::get('/report/tabPayment','reportController@tabPayment');
    Route::get('/report/tablePayment','reportController@tablePayment');


    Route::get('/report/tabExtraItem','reportController@tabExtraItem');
    Route::get('/report/tableExtraItem','reportController@tableExtraItem');

    Route::get('/report/tabStatistic','reportController@tabStatistic');
    Route::get('/report/tableStatistic','reportController@tableStatistic');


    Route::get('/report/tabDailyTransaction','reportController@tabDailyTransaction');
    Route::get('/report/tableDailyTransaction','reportController@tableDailyTransaction');

    Route::get('/report/tabGuest','reportController@tabGuest');
    Route::get('/report/tableGuest','reportController@tableGuest');


    // ------------------------------logout-------------------------------------
    Route::get('/login/logout','loginController@logout');


    /*
    |--------------------------------------------------------------------------
    | Percobaan
    |--------------------------------------------------------------------------
    */
    Route::get('percobaan/listPriceRoom','detailController@listPriceRoom');
    Route::get('percobaan/tableListPriceRoom','detailController@tableListPriceRoom');


});

/*
|--------------------------------------------------------------------------
| Rooting For Login
|--------------------------------------------------------------------------
|
*/
Route::get('/login','loginController@index');
Route::post('/login/processLogin','loginController@processLogin');


Route::get('/getSession',function(){
  return session()->all();
});

Route::get('/delSession',function(){
  session()->flush();
});
