<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class detailController extends Controller
{
    public function invoice($reservationId){
        # cutomer id

        # data company profile
        $company = DB::table('company_profile')
                      ->select(['street',
                                'province',
                                'post_code',
                                'province',
                                'country',
                                'phone',
                                'email'])
                      ->first();

        # data customer
        $customer = DB::table('customer')
                      ->select(['first_name',
                                'last_name',
                                'street',
                                'post_code',
                                'country'])
                      ->join('reservation','reservation.customer_id','=','customer.customer_id')
                      ->where('reservation_id','=',$reservationId)
                      ->first();

        # data reservation
        $reservation = DB::table('reservation')
                      ->select(['invoice',
                                'status',
                                'checkin_date',
                                'checkout_date'])
                      ->where('reservation_id','=',$reservationId)
                      ->first();

        # total people in reservation
        $people = DB::table('reservation')
                      ->selectRaw('sum(adult) as adult')
                      ->selectRaw('sum(child) as child')
                      ->join('reservation_room','reservation_room.reservation_id','=','reservation.reservation_id')
                      ->join('room','room.room_id','=','reservation_room.room_id')
                      ->join('room_type','room_type.type_id','=','room.type_id')
                      ->where('reservation.reservation_id','=',$reservationId)
                      ->first();

        // return var_dump($people);

        # detail reservation room
        $detail = DB::select("SELECT room_type.type,
                                     room.room_number,
                                     reservation_room.room_bill + reservation_room.default_price_breakfast as night_rate
                              FROM reservation JOIN reservation_room USING(reservation_id)
                                 JOIN room USING(room_id)
                                 JOIN room_type USING(type_id)
                              WHERE reservation.reservation_id = ?
                              ",[$reservationId]);
        # total room bill
        $totalRoomBill = DB::table('reservation_room')
                            ->selectRaw('sum(reservation_room.room_bill + reservation_room.default_price_breakfast)
                                         *
                                         (checkout_date - checkin_date)
                                         as total_room_bill')
                            ->join('reservation','reservation.reservation_id','=','reservation_room.reservation_id')
                            ->where('reservation.reservation_id','=',$reservationId)
                            ->groupBy('checkout_date','checkin_date')
                            ->first();

        # detail reservation Extra
        $detailExtra = DB::select("SELECT type_extra_item.extra_type,
                                      	   extra_item.item,
                                      	   reservation_extra.qty,
                                             (reservation_extra.extra_bill / reservation_extra.qty) as price,
                                             reservation_extra.extra_bill  as subTotal
                                      FROM reservation_extra JOIN extra_item USING(extra_id)
                                                             JOIN type_extra_item USING(extra_type_id)
                                      WHERE reservation_extra.reservation_id = ?
                              ",[$reservationId]);


        # total extra bill
        $totalReservationExtra = DB::table('reservation_extra')
                                    ->selectRaw('sum(extra_bill) as total_extra_bill')
                                    ->where('reservation_id','=',$reservationId)
                                    ->first();

        # total extra bill
        $night = DB::table('reservation')
                      ->selectRaw('checkout_date - checkin_date as night')
                      ->where('reservation_id','=',$reservationId)
                      ->first();


        // return var_dump($naight);

        return view('detail.invoice',[
          'company' => $company,
          'customer' => $customer,
          'reservation' => $reservation,
          'detailReservation' => $detail,
          'people' => $people,
          'totalRoomBill' => $totalRoomBill,
          'detailExtra' => $detailExtra,
          'totalReservationExtra' => $totalReservationExtra,
          'night' => $night,
          'reservationId' => $reservationId
        ]);
    }


    public function listPriceRoom(){
        $today = new Carbon();

        # today
        $dateNow = $today->toDateString();
        # one Week form to day
        $oneWeekFromNow = $today->addWeek(1)->toDateString();

        // return $oneWeekFromNow;
        return view('percobaan.listPriceRoom',[
          'dateNow' => $dateNow,
          'oneWeekFromNow' => $oneWeekFromNow
        ]);
    }

    public function tableListPriceRoom(Request $request){
        $inputDateBegin = $request->inputDateBegin;
        $inputDateEnd = $request->inputDateEnd;

        $dateBegin = new Carbon($inputDateBegin);
        $dateEnd = new Carbon($inputDateEnd);
        $dateForLoop = $dateBegin->subDay();



        // cari interval hari
        $intervalDay = $dateBegin->diffInDays($dateEnd);

        // hari dari interval date begin dan date end
        $days = [];
        $pricesRoom = [];

        for ($i=0; $i < $intervalDay; $i++) {
            $day = $dateForLoop->addDay();
            $days[] = $day->toDateString();
        }


        $room = DB::table('room')
                    ->select(['room_id',
                              'room_number',
                              'default_price'])
                    ->join('room_type','room_type.type_id','=','room.type_id')
                    ->get();


        return view('percobaan.tableListPriceRoom',[
           'days' => $days,
           'rooms' => $room
        ]);


    }

    public function getPriceRoom($roomId,$date){
        $pricesRoom  =  DB::select('select getPriceRoom(?,?) as price ',[
                                $roomId,$date
                              ]);
        return $pricesRoom[0]->price;
    }
}
