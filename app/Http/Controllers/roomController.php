<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class roomController extends Controller
{
    public function addRoomType(Request $request){
        $type = $request->type;
        $maxAdult = $request->maxAdult;
        $maxChild = $request->maxChild;
        $defaultPrice = $request->defaultPrice;
        $defaultPriceBreakfast = $request->defaultPriceBreakfast;

        if ($type == null){
           return 'type not allow empty';
        }

        if ($maxAdult == null){
           return 'max adult not allow empty';
        }

        if (!is_numeric($maxAdult)){
           return 'max adult must numberic';
        }

        if ($maxChild == null){
           return 'max child not allow empty';
        }

        if (!is_numeric($maxChild)){
           return 'max child must numberic';
        }

        if ($defaultPrice == null){
           return 'price not allow empty';
        }

        if (!is_numeric($defaultPrice)){
           return 'defaultPrice must numberic';
        }

        if ($defaultPriceBreakfast == null){
           return 'price breakfas not allow empty';
        }

        if (!is_numeric($defaultPriceBreakfast)){
           return 'defaultPriceBreakfas must numberic';
        }

        try {
          DB::select('call insert_room_type(?,?,?,?,?)',[
            $type,$maxAdult,$maxChild,$defaultPrice,$defaultPriceBreakfast
          ]);
        } catch(\Illuminate\Database\QueryException $ex){
            return $ex->getMessage();
        }

        return 'berhasil';
    }

    public function addSpecialPrice(Request $request){
        $typeId = $request->typeId;
        $timeBegin = $request->timeBegin;
        $timeEnd = $request->timeEnd;
        $price = $request->price;

        DB::select('call insert_special_price(?,?,?,?)',[
             $typeId,$timeBegin,
             $timeEnd,$price
        ]);
    }

    public function addRoom(Request $request){
        $typeId = $request->typeId;
        $roomName = $request->roomName;

        try {
          DB::select('call insert_room(?,?)',[
               $typeId,
               $roomName
          ]);
        } catch(\Illuminate\Database\QueryException $ex){
          return $ex->getMessage();
        }
    }

    public function editTypeRoom(Request $request){
      $typeId = $request->typeId;
      $type = $request->type;
      $maxAdult = $request->maxAdult;
      $maxChild = $request->maxChild;
      $defaultPrice = $request->defaultPrice;
      $defaultPriceBreakfast = $request->defaultPriceBreakfast;

      // return "
      // $typeId
      // $type
      // $maxAdult
      // $maxChild
      // $defaultPrice
      // $defaultPriceBreakfast
      //          ";
      try {
        DB::select('call update_room_type(?,?,?,?,?,?)',[
            $typeId,
            $type,
            $maxAdult,
            $maxChild,
            $defaultPrice,
            $defaultPriceBreakfast
        ]);
      } catch(\Illuminate\Database\QueryException $ex){
        return $ex->getMessage();
      }
    }

    public function editSpecialPrice(Request $request){
        $specialId = $request->specialId;
        $timeBegin = $request->timeBegin;
        $timeEnd = $request->timeEnd;
        $defaultPrice = $request->defaultPrice;
        $defaultPriceBreakfast = $request->defaultPriceBreakfast;

        DB::select('call update_special_price(?,?,?,?,?)',[
             $specialId,$timeBegin,$timeEnd,
             $defaultPrice,$defaultPriceBreakfast
        ]);
    }

    public function editRoom(Request $request){
        $roomId = $request->roomId;
        $typeId = $request->typeId;
        $specialId = $request->specialId;
        $roomNumber = $request->roomNumber;

        DB::select('call update_room(?,?,?,?)',[
             $roomId,$typeId,$specialId,$roomNumber
        ]);
    }

    public function deleteRoom(Request $request){
        $roomId = $request->roomId;
        DB::select('call delete_room(?)',[$roomId]);
    }

    public function deleteRoomType(Request $request){
        $typeId = $request->typeId;
        DB::select('call delete_room_type(?)',[$typeId]);
    }

    public function deleteSpecialPrice(Request $request){
        $specialId = $request->specialId;
        DB::select('call delete_special_price(?)',[$specialId]);
    }

}
