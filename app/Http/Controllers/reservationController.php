<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;

class reservationController extends Controller
{
    public function index(){
        $roomType = DB::select("SELECT type_id,
                                       type
                                FROM room_type
                                WHERE status = 'aktif'");

        $now = Carbon::now();
        $today = $now->toDateString();
        $tomorow = $now->addDays(1)->toDateString();

        return view('reservation.reservation',[
            'roomTypes' => $roomType,
            'today' => $today,
            'tomorow' => $tomorow
        ]);
    }

    public function showDummyRoomReservation(){
        $dummyReservation = DB::select('SELECT a.room_id,
                                               a.room_type,
                                               room.room_number,
                                               adult,
                                               child
                                        FROM dummy_reservation_room as a
                                        JOIN room USING (room_id)
                                        JOIN room_type USING(type_id)');
        return view('reservation.dummyReservation',[
              'dummyReservations' => $dummyReservation
          ]);

    }


    public function editReservation(Request $request){
        $reservationId = $request->reservationId;
        $customerId = $request->customerId;
        $username = $request->username;
        $checkinDate = $request->checkinTime;
        $checkoutDate = $request->checkoutTime;

        DB::select('call update_reservation(?,?,?,?,?)',[
           $reservationId,$customerId,$username,$checkinDate,$checkoutDate
        ]);
    }

    public function deleteReservationRoom(Request $request){
        $reservationId = $request->reservationId;
        $roomId = $request->roomId;
        DB::select('call delete_reservation_room(?,?)',[
           $reservationId,$roomId
        ]);
    }

    public function deleteReservation(Request $request){
        $reservationId = $request->reservationId;
        DB::select('call delete_reservation(?)',[$reservationId]);
    }

    public function getRoomNumber(Request $request){
        $checkinDate = date("Y-m-d",strtotime($request->checkinDate));
        $checkoutDate = date("Y-m-d",strtotime($request->checkoutDate));
        $idRoomType = $request->idRoomType;

        // return $checkinDate. ' ' . $checkoutDate.' '.$idRoomType;
        try {
          $rooms= DB::select('SELECT room.room_id,
                                     room_type.type,
                                     room.room_number
                              FROM   room join room_type using(type_id)
                              WHERE room_type.type_id = :roomTypeId
                                AND room_number not in (select reservation_room.room_number
                                          				      from reservation join reservation_room
                                          						  on reservation.reservation_id = reservation_room.reservation_id
                              					                where (
                                              							   ((reservation.checkin_date >= :checkinDate1) and (reservation.checkin_date < :checkoutDate1))
                                                               OR
                                                               ((reservation.checkin_date < :checkoutDate2 and (reservation.checkout_date >= :checkoutDate3))
                                                               OR
                                                               ((reservation.checkin_date <= :checkinDate2) and (reservation.checkout_date > :checkinDate3))
                                                             )))',[
            'roomTypeId' => $idRoomType,
            'checkinDate1' => $checkinDate,
            'checkoutDate1' => $checkoutDate,
            'checkoutDate2' => $checkoutDate,
            'checkoutDate3' => $checkoutDate,
            'checkinDate2' => $checkinDate,
            'checkinDate3' =>$checkinDate
          ]);
        } catch(\Illuminate\Database\QueryException $ex){
            return $ex->getMessage();
        }

        return view('reservation.optionForRoomNumber',['rooms'=>$rooms]);
    }

    public function addDummyRoomReservation(Request $request){
        $roomType = $request->roomType;
        $roomId = $request->roomId;
        $checkinDate = $request->checkinDate;
        $breakfast = $request->breakfast;
        $adult = $request->adult;
        $child = $request->child;
        $checkinDate = $request->checkinDate;
        $checkoutDate = $request->checkoutDate;

        if ($adult == null){
            return 'adult empty';
        }

        if ($child == null){
            return 'child empty';
        }

        if($this->validateNumberAdult($roomId,$adult) == 'not correct'){
            return response()->json(['status' => 'failedAdult',
                                     'message' => 'overload']);
        };

        if($this->validateNumberChild($roomId,$child) == 'not correct'){
            return response()->json(['status' => 'failedChild',
                                     'message' => 'overload']);
        };

        try {
            DB::select('call insert_dummy_reservation(?,?,?,
                                                      ?,?,?)',[
              $roomId,
              $breakfast,
              $checkinDate,
              $checkoutDate,
              $adult,
              $child
            ]);
        } catch(\Illuminate\Database\QueryException $ex){
            return $ex->getMessage();
        }
    }

    public function saveReservation(Request $request){
        $checkinDate = date("Y-m-d",strtotime($request->checkinDate));
        $checkoutDate = date("Y-m-d",strtotime($request->checkoutDate));
        $identityId = $request->identityId;
        $firstName = $request->firstName;
        $lastName = $request->lastName;
        $gender = $request->gender;
        $email = $request->email;
        $phoneNumber = $request->phoneNumber;
        $dateOfBirth = $request->dateOfBirth;

        $organisation = $request->organisation;
        $street = $request->street;
        $city = $request->city;
        $province = $request->province;
        $postcode = $request->postcode;
        $country = $request->country;
        $note = $request->note;
        $customerId = null;
        // username di isi account_id dari tabel account , kenapa tidak dirubah
        // karna malas , hahahaha ,by oky saputra 23-maret-2018 20:02
        $usernameAccount = $request->session()->get('account_id');
        $idReservation = null;

        # validasi dummy reservation
        if($this->checkDummy()){
            return 'dummy kosong';
        };

        # validasi date of birth
        if(!$this->validateDate($dateOfBirth)){
            return 'birthofdate false';
        }

        $check = DB::table('customer')->where('identity_id', $identityId)->first();
        if($check == null){
            try {
                # Tambah data customer
                DB::select('call insert_customer(?,?,?,?,?,
                                                 ?,?,?,?,?,
                                                 ?,?,?)',[
                  $identityId,$firstName,$lastName,$gender,$email,
                  $phoneNumber,$dateOfBirth,$organisation,$street,$city,
                  $province,$postcode,$country
                ]);

                # set id customer yang baru
                $getLastInsertId = DB::select('SELECT customer_id
                                               FROM customer
                                               ORDER BY create_at DESC LIMIT 1');
                $customerId = $getLastInsertId[0]->customer_id;

            } catch(\Illuminate\Database\QueryException $ex){
                return $ex->getMessage();
            }
        }

        if( $check != null ){
          $customerId = $check->customer_id;
        }

        # add reservation
        try {
            DB::select('call insert_reservation(?,?,?,?,?)',[
               $customerId,$usernameAccount,$checkinDate,$checkoutDate,$note
            ]);

            $lastIdReservation = DB::select('SELECT reservation_id FROM reservation ORDER BY reservation_time DESC LIMIT 1');
            $idReservation = $lastIdReservation[0]->reservation_id;

        } catch(\Illuminate\Database\QueryException $ex){
            return $ex->getMessage();
        }

        // add reservation room
        # select semua data di dummy reservation
        $dummys = DB::table('dummy_reservation_room')->get();
        foreach ($dummys as $dummy ) {
            try {
                  # Tambah data customer
                  DB::select('call insert_reservation_room(?,?,?,?,?,?,?,?);',[
                      $idReservation,
                      $dummy->room_id,
                      $dummy->room_bill,
                      $dummy->default_price_breakfast,
                      $dummy->room_type,
                      $dummy->room_number,
                      $dummy->adult,
                      $dummy->child

                  ]);

                  DB::select('TRUNCATE dummy_reservation_room');

            } catch(\Illuminate\Database\QueryException $ex){
                    return $ex->getMessage();
            }
        }

    }

    public function deleteDummyReservation(Request $request){
        DB::select('DELETE from dummy_reservation_room
                    WHERE room_id = ?',[$request->roomId]);
    }

    public function getSumRoomBillDummyReservation(Request $request){
        $rommTotal = DB::select('select sum(room_bill + default_price_breakfast)
                                        *
                                        (checkout_date - checkin_date)
                                  as room_total
                                from dummy_reservation_room
                                group by checkout_date,checkin_date');

        if (empty($rommTotal)){
          return '0';
        }else {
          return $rommTotal[0]->room_total;
        }


    }

    public function serachCustomer(Request $request){
        $identityId = trim($request->identityId);
        $cari = DB::table('customer')
                    ->select(['first_name',
                              'last_name',
                              'gender',
                              'email',
                              'phone',
                              'date_of_birth',
                              'organisation',
                              'street',
                              'city',
                              'province',
                              'post_code',
                              'country'])
                    ->where('identity_id','=',$identityId)
                    ->first();
        if($cari != null){
          return response()->json($cari);
        }else{
          return 'not found';
        }

    }

    public function validateDate($date){
      $d = DateTime::createFromFormat('Y-m-d', $date);
      return $d && $d->format('Y-m-d') == $date;
    }


    /**
     * [validateNumberOfGuesst description]
     * @param  int $roomId
     * @param  int $adult
     * @return string    return 'correct' if adult <= max_adult in type room
     *                   return 'not correct' if adult > max_adult
     */
    public function validateNumberAdult($roomId,$adult){
      $validate = DB::table('room_type')
                  ->select('max_adult')
                  ->join('room','room.type_id','=','room_type.type_id')
                  ->where('room.room_id','=',$roomId)
                  ->first();

      if($adult <= $validate->max_adult){
          return 'correct';
      }else{
          return 'not correct';
      }

    }

    /**
     * [validateNumberOfGuesst description]
     * @param  int $roomId
     * @param  int $child
     * @return string    return 'correct' if child <= max_child in type room
     *                   return 'not correct' if child > max_child
     */
    public function validateNumberChild($roomId,$child){
      $validate = DB::table('room_type')
                  ->select('max_child')
                  ->join('room','room.type_id','=','room_type.type_id')
                  ->where('room.room_id','=',$roomId)
                  ->first();

      if($child <= $validate->max_child){
          return 'correct';
      }else{
          return 'not correct';
      }

    }

    /**
     * [checkDummy description]
     * @return [boolean] [return true if dummy reservation is null
     *                    return false if dummy reservation not null]
     */
    public function checkDummy(){
      $validate = DB::table('dummy_reservation_room')
                      ->get();

      if ($validate->isEmpty()){
          return true;
      }else{
          return false;
      }
      //
      // $val = (array) $validate;
      // return var_dump($val);

    }

}
