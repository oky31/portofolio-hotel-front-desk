<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class controlPanelController extends Controller
{
    public function index(){
        return view('controlPanel.controlPanel');
    }

    public function tabCustomer(){
        return view('controlPanel.tabCustomer');
    }

    public function tabRoom(){
        return view('controlPanel.tabRoom');
    }

    public function tabAccount(){
        return view('controlPanel.tabAccount');
    }

    public function tabExtraItem(){
        return view('controlPanel.tabExtraItem');
    }

    public function dataTablesCustomer(){
        $customer = DB::table('customer')
                       ->select(['identity_id',
                                 'first_name',
                                 'last_name',
                                 'email',
                                 'phone']);

       return Datatables::of($customer)->make();
    }

    public function showRoom(){
      $roomsType = DB::table('room_type')
                     ->select(['type_id',
                               'type',
                               'max_adult',
                               'max_child',
                               'default_price',
                               'default_price_breakfast'])
                     ->where('status','=','aktif')
                     ->get();
       return view('controlPanel.room',[
         'roomsType' => $roomsType
       ]);
    }

    public function showAddRoom(Request $request){
      $typeId = $request->typeId;
      // return $typeId;
       return view('controlPanel.showAddRoom',[
         'typeId' => $typeId
       ]);
    }

    public function showRoomType(){
        return view('controlPanel.roomType');
    }

    public function showListRoomType(){
      $roomsType = DB::table('room_type')
                     ->select(['type_id',
                               'type',
                               'max_adult',
                               'max_child',
                               'default_price',
                               'default_price_breakfast'])
                     ->where('status','=','aktif')
                     ->get();
       return view('controlPanel.listRoomType',[
          'roomsType' => $roomsType
       ]);
    }

    public function getRoomUpdate(Request $request){
        $typeId = $request->typeId;
        $roomsType = DB::table('room_type')
                       ->select(['type_id',
                                 'type',
                                 'max_adult',
                                 'max_child',
                                 'default_price',
                                 'default_price_breakfast'])
                       ->where('type_id','=',$typeId)
                       ->get();
        // $roomsType->t;
        return $roomsType;
    }

    public function showListRoom(Request $request){
      $typeId = $request->typeId;
      $rooms = DB::table('room')
                 ->select(['room_id',
                           'room_number'])
                 ->where('type_id','=',$typeId)
                 ->where('status_insystem','=','aktif')
                 ->get();

      return view('controlPanel.listRoom',['rooms' => $rooms]);
    }

    public function listAccount(){
      $account = DB::table('account')
                    ->select(['account_id',
                              'username',
                              'password',
                              'level'])
                    ->get();
        return view('controlPanel.listAccount',['accounts' => $account]);
    }

    public function typeExtraItem(){
        return view('controlPanel.typeExtraItem');
    }

    public function showListTypeExtraItems(){
        $extraType = DB::table('type_extra_item')
                      ->select(['extra_type_id',
                                'extra_type'])
                      ->get();
        return view('controlPanel.listTypeExtraItem',['extraType' => $extraType]);
    }

    public function extraItem(Request $request){
        $typeExtraId = $request->typeExtraId;

        return view('controlPanel.extraItem',[
          'typeExtraId' => $typeExtraId
        ]);
    }

    public function showListExtraItems(Request $request){
      $typeExtraItemId = $request->typeExtraItemId;

      // return $typeExtraItemId;
      $extraItems = DB::table('extra_item')
                    ->select(['extra_id',
                              'item',
                              'price'])
                    ->where('extra_type_id','=',$typeExtraItemId)
                    ->get();
        return view('controlPanel.listExtraItems',[
            'extraItems' => $extraItems
        ]);
    }

    public function tabRoomRate(Request $request){
        $typeId = DB::table('room_type')
                    ->select(['type_id',
                              'type'])
                    ->get();
        return view('controlPanel.tabRoomRate',[
          'typesId' => $typeId
        ]);
    }

    public function datatablesSpecialPrice(){
        $specialPrice = DB::table('special_price')
                       ->join('room_type','room_type.type_id','=','special_price.type_id')
                       ->select(['room_type.type',
                                 'time_begin',
                                 'time_end',
                                 'special_price.default_price',
                                 'special_price.create_at'])
                       ->where('special_price.status','=','aktif');
        return Datatables::of($specialPrice)->make();
    }

    public function tableListSpecialPrice(){
        return view('controlPanel.tableListSpecialPrice');
    }
}
