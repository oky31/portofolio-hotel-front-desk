<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class calendarController extends Controller
{
    public function index(){
        return view('calendar.mainCalendar');
    }

    public function roomData(){
      $data = DB::select('SELECT room_id as id,
                                 type,
                                 room_number as title
                          FROM room JOIN room_type USING(type_id)');
      return $data;
    }

    public function eventData(){
      $data = DB::select("SELECT room_id as resourceId,
                                 CONCAT(mainReservation.checkin_date,' ',mainReservation.checkin_time) as start,
                                 CONCAT(mainReservation.checkout_date,' ',mainReservation.checkout_time) as end,
                                 CONCAT(customer.last_name,',',customer.first_name) as title,
                                 if(mainReservation.status = 'booked','orange',if(mainReservation.status = 'checkin','#11c47f','black')) as color,
                                 if(mainReservation.status = 'booked','white',if(mainReservation.status = 'checkin','white','white')) as textColor
                          FROM reservation as mainReservation
                          	   JOIN customer USING (customer_id)
                               JOIN reservation_room USING (reservation_id)
                               JOIN room USING(room_id)");
      return $data;
    }

    public function test(){
      return view('calendar.mainCalendar');
    }
}
