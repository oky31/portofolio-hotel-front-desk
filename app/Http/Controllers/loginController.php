<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class loginController extends Controller
{
    public function index(){
      return view('login.login');
    }

    public function processLogin(Request $request){
        $username = $request->username;
        $password = $request->password;
        // return $password;
        #get username and level
        $user = DB::table('account')
                    ->select('username','level','account_id')
                    ->where('username',$username)
                    ->where('password',$password)
                    ->where('status','aktif')
                    ->first();

        if ($user == null){
           return 'password or username wrong !';
        }

        if (isset($user)){
           $request->session()->put('account_id',$user->account_id);
           $request->session()->put('username',$user->username);
           $request->session()->put('level',$user->level);
           return 'berhasil';
        }

    }

    public function logout(Request $request){
        $request->session()->flush();
        return redirect('/login');
    }
}
