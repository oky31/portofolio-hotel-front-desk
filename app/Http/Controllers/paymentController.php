<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class paymentController extends Controller
{
    public function index(){
        return view('payment.payment');
    }

    public function addPayment(Request $request){
        $reservationId = $request->reservationId;
        $paymentType = $request->paymentType;
        $cardId = $request->cardId;
        $cardName = $request->cardName;
        $cardExp = $request->cardExp;
        $totalBill = $request->totalBill;


        DB::select('call insert_payment(?,?,?,?,?,?)',[
            $reservationId,$paymentType,$cardId,$cardName,
            $cardExp,$totalBill
        ]);
    }

    public function showListReservation(Request $request){
        $dateBegin = $request->dateBegin;
        $dateEnd = $request->dateEnd;

        $reservations = DB::select('SELECT byr.reservation_id,byr.payment_id,
                                           byr.total_bill,
                                           byr.total_bayar,
                                           byr.first_name,
                                           byr.last_name
                                    FROM  (SELECT   reservation.reservation_id,
                                                    payment.payment_id,
                                    				payment.total_bill,
                                                    customer.first_name,
                                                    customer.last_name,
                                                    sum(customer_payment.total) as total_bayar
                                    		    FROM 	reservation left join payment on reservation.reservation_id = payment.reservation_id
                                    				            left join customer_payment on payment.payment_id = customer_payment.payment_id
                                                                join customer on reservation.customer_id = customer.customer_id
                                    		    WHERE reservation.checkin_time between ? and ?
                                    		    GROUP BY payment.payment_id,
                                    				         reservation.reservation_id,
                                                     payment.total_bill,
                                                     customer.first_name,
                                                     customer.last_name) as byr

                                    WHERE byr.total_bayar < byr.total_bill or byr.total_bayar IS NULL',
                      [$dateBegin,$dateEnd]);

        return view('payment.showListReservation',[
          'reservations' => $reservations
        ]);
    }

    public function reservationDetail($id){
        $reservationId = $id;
        $roomBill = DB::select('SELECT sum(room_bill + default_price_breakfast) * (checkout_date- checkin_date) as total
                                FROM reservation_room JOIN reservation USING (reservation_id)
                                WHERE reservation_id = ?
                                GROUP BY checkout_date,checkin_date',[$reservationId]);

        $extraBill = DB::select("SELECT sum(extra_bill) as extra_bill
                               FROM reservation_extra
                               WHERE reservation_id = ? ",[$reservationId]);

        $total =  $roomBill[0]->total + $extraBill[0]->extra_bill;

        // return $total;
        return view('payment.reservationDetail',[
          'roomBill'=>$roomBill,
          'reservationId' => $reservationId,
          'extraBill' => $extraBill,
          'total' => $total
        ]);
    }

    public function processPayment(Request $request){
        $reservationId = $request->reservationId;
        $accountId = $request->session()->get('account_id');
        $transactionType = $request->transactionType;
        $description = $request->description;
        $paymentType = $request->paymentType;
        $totalBill = $request->totalBill;
        $totalPayment = $request->totalPayment;
        $acceptance = $request->acceptance;
        $cardId = null;
        $cardName = null;
        $cardExp = null;

        // return $reservationId.' '.$transactionType.' '.$description.' '.
        //        $paymentType.' '.$totalBill.' '.$totalPayment.' '.$acceptance;

        // return $paymentId = $this->getPaymentId(1);
        //
        // if ($totalPayment == null){
        //    return 'total payment not allow empty';
        // }
        //
        // if (!is_numeric($totalPayment)){
        //    return 'total must payment numberic';
        // }
        //
        // if ($acceptance == null){
        //    return 'acceptance not allow empty';
        // }
        //
        // if (!is_numeric($acceptance)){
        //    return 'acceptance must numberic';
        // }


        if($this->checkPayment($reservationId)){
            $this->payment($reservationId,
                           $accountId,
                           $transactionType,
                           $paymentType,
                           $cardId,
                           $cardName,
                           $cardExp,
                           $totalPayment,
                           $acceptance,
                           $description);
        }else{
            $paymentId = $this->getPaymentId($reservationId);
            #update tabel bayar

            try {
              $paymentId = DB::select('call update_payment(?,?,?,?,
                                                           ?,?,?)',[
                  $paymentId,
                  $accountId,
                  $transactionType,
                  $paymentType,
                  $totalPayment,
                  $acceptance,
                  $description
              ]);
            } catch(\Illuminate\Database\QueryException $ex){
                return $ex->getMessage();
            }
        };


        return 'berhasil';
    }

    private function payment($reservationId,
                             $transactionType,
                             $accountId,
                             $paymentType,
                             $cardId,
                             $cardName,
                             $cardExp,
                             $totalPayment,
                             $acceptance,
                             $description){
         try {

             DB::select('call insert_payment(?,?,?,?,?,
                                             ?,?,?,?,?)',[
                  $reservationId,
                  $accountId,
                  $transactionType,
                  $paymentType,
                  $cardId,
                  $cardName,
                  $cardExp,
                  $totalPayment,
                  $acceptance,
                  $description
             ]);

         } catch(\Illuminate\Database\QueryException $ex){
             return $ex->getMessage();
         }
    }


    public function showAllListReservation(){
        $allReservations = DB::select('SELECT byr.reservation_id,byr.payment_id,
                                              byr.total_bill,
                                              byr.total_bayar,
                                              byr.first_name,
                                              byr.last_name
                                       FROM  (select   reservation.reservation_id,
                                                      payment.payment_id,
                                      				payment.total_bill,
                                                      customer.first_name,
                                                      customer.last_name,
                                                      sum(customer_payment.total)as total_bayar
                                       FROM 	reservation left join payment on reservation.reservation_id = payment.reservation_id
                                      				            left join customer_payment on payment.payment_id = customer_payment.payment_id
                                                          join customer on reservation.customer_id = customer.customer_id
                                       GROUP BY payment.payment_id,
                                      				 reservation.reservation_id,
                                                       payment.total_bill,
                                                       customer.first_name,
                                                       customer.last_name) as byr
                                       WHERE byr.total_bayar < byr.total_bill or byr.total_bayar is null');
        return view('payment.showAllListReservation',['allReservations' => $allReservations]);
    }

    private function checkPayment($reservationId){
        try {
            $checkPyament = DB::select('SELECT customer_id
                                        FROM reservation LEFT JOIN payment ON reservation.reservation_id = payment.reservation_id
                                        WHERE payment.payment_id is null
                                        AND reservation.reservation_id = ?',[
                               $reservationId]);

        } catch(\Illuminate\Database\QueryException $ex){
            return $ex->getMessage();
        }

        if (count($checkPyament) == 1){
            return true;
        }
        return false;
    }

    private function getPaymentId($reservationId){
        try {

          $paymentId = DB::table('payment')->where('reservation_id', $reservationId)->value('payment_id');

        } catch(\Illuminate\Database\QueryException $ex){
            return $ex->getMessage();
        }

         return $paymentId;
    }

    public function getDeposit(Request $request){
        $reservationId = $request->reservationId;

        $deposit = DB::select("SELECT sum(total) as deposit
                                FROM customer_payment JOIN payment USING(payment_id)
                                WHERE reservation_id = ? AND transaction_type = 'deposit'",[
                                 $reservationId
                               ]);
        return $deposit[0]->deposit;
    }

    public function getOutstanding(Request $request){
        $reservationId = $request->reservationId;
        $qrRestOfBill = DB::select('select getTotalBill(?) -
                                           getTotalCustomerPayment(?)
                                           as sisa_bayar',[$reservationId,$reservationId]);


          return $qrRestOfBill[0]->sisa_bayar;
    }

    public function historyPaymentPerReservation(Request $request){
        $reservationId = $request->reservationId;
        $historyPayment = DB::select("SELECT total,
                                             transaction_type,
                                             payment_type,
                                             description,
                                             customer_payment.create_at,
                                             account.username as username
                                      FROM customer_payment JOIN payment USING(payment_id)
                                                            JOIN account USING(account_id)
                                      WHERE reservation_id = ?
                                      ORDER BY customer_payment_id DESC",[
                                        $reservationId
                                      ]);
        return view('payment.historyPaymentPerResrvation',[
            'historyPayment' => $historyPayment
        ]);
    }

    public function getTotalReceived(Request $request){
        $reservationId = $request->reservationId;
        $totalReceived = DB::select('SELECT sum(total) as total_received
                                     FROM customer_payment JOIN payment USING(payment_id)
                                     WHERE reservation_id = ?',[
                                        $reservationId
                                    ]);
        return $totalReceived[0]->total_received;
    }
}
