<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class reservationListController extends Controller
{
    public function index(){
        $now = Carbon::now();
        $today = $now->toDateString();
        $tomorow = $now->addDays(1)->toDateString();
        return view('reservationList.reservationList',[
          'today' => $today,
          'tomorow' => $tomorow
        ]);
    }

    public function list(Request $request){
       $dateBegin = $request->dateBegin;
       $dateEnd = $request->dateEnd;
       $sortBy = $request->sortBy;
       $roomListPerReservation = [];

       // return $sortBy;
       if($sortBy == 'all'){
          $dataReservation = DB::select("SELECT mainReservation.reservation_id,
                                                mainReservation.invoice,
                                                mainReservation.checkin_date,
                                                mainReservation.checkout_date,
                                                mainReservation.status,
                                                customer.customer_id,
                                                customer.first_name,
                                                customer.last_name,
                                                (SELECT sum(room_bill + default_price_breakfast) * (sub1Reservation.checkout_date - sub1Reservation.checkin_date)
                                                 FROM reservation sub1Reservation JOIN reservation_room ON sub1Reservation.reservation_id = reservation_room.reservation_id
                                                 WHERE sub1Reservation.reservation_id = mainReservation.reservation_id
                                                 GROUP BY sub1Reservation.checkout_date,sub1Reservation.checkin_date)
                                                 +
                                                (SELECT if(sum(extra_bill) is null,0,sum(extra_bill))
                                                 FROM reservation sub2Reservation JOIN reservation_extra ON sub2Reservation.reservation_id = reservation_extra.reservation_id
                                                 WHERE sub2Reservation.reservation_id = mainReservation.reservation_id)
                                               as total
                                          FROM reservation mainReservation JOIN customer USING(customer_id)
                                            			 JOIN reservation_room USING(reservation_id)
                                            			 LEFT JOIN reservation_extra USING(reservation_id)
                                          WHERE mainReservation.status IN ('checkin','checkout','booked')
                                          AND date_format(mainReservation.checkin_date,'%Y-%m-%d')  BETWEEN ? AND ?
                                          GROUP BY mainReservation.reservation_id,
                                                   mainReservation.invoice,
                                                   mainReservation.checkin_date,
                                                   mainReservation.checkout_date,
                                                   mainReservation.status,
                                                   customer.customer_id,
                                                   customer.first_name,
                                                   customer.last_name",[
                                     $dateBegin,$dateEnd
                                  ]);
       }

       // return $sortBy;
       if($sortBy == 'guess-in-house'){
          $dataReservation = DB::select("SELECT mainReservation.reservation_id,
                                          	    mainReservation.invoice,
                                                mainReservation.checkin_date,
                                                mainReservation.checkout_date,
                                                mainReservation.status,
                                                customer.customer_id,
                                                customer.first_name,
                                          	    customer.last_name,
                                                (SELECT sum(room_bill + default_price_breakfast) * (sub1Reservation.checkout_date - sub1Reservation.checkin_date)
                                                 FROM reservation sub1Reservation JOIN reservation_room ON sub1Reservation.reservation_id = reservation_room.reservation_id
                                                 WHERE sub1Reservation.reservation_id = mainReservation.reservation_id
                                                 GROUP BY sub1Reservation.checkout_date,sub1Reservation.checkin_date)
                                                 +
                                          		  (SELECT if(sum(extra_bill) is null,0,sum(extra_bill))
                                          		   FROM reservation sub2Reservation JOIN reservation_extra ON sub2Reservation.reservation_id = reservation_extra.reservation_id
                                          		   WHERE sub2Reservation.reservation_id = mainReservation.reservation_id) as total
                                          FROM reservation as mainReservation
                                          	   JOIN customer USING (customer_id)
                                               JOIN reservation_room USING (reservation_id)
                                          WHERE (
                                          	     ((mainReservation.checkin_date >= ?) AND (mainReservation.checkin_date <= ?))
                                          	     OR
                                          	     ((mainReservation.checkin_date < ?) AND (mainReservation.checkout_date >= ?))
                                          	     OR
                                          	     ((mainReservation.checkin_date <= ?) AND (mainReservation.checkout_date >= ?))
                                                )
                                          GROUP BY mainReservation.reservation_id,
                                          	       mainReservation.invoice,
                                                   mainReservation.checkin_date,
                                                   mainReservation.checkout_date,
                                                   mainReservation.status,
                                                   customer.customer_id,
                                                   customer.first_name,
                                          	       customer.last_name",[
                                     $dateBegin,
                                     $dateEnd,
                                     $dateEnd,
                                     $dateEnd,
                                     $dateBegin,
                                     $dateBegin
                                  ]);
       }

       if($sortBy == 'checkin'){
         $dataReservation = DB::select("SELECT mainReservation.reservation_id,
                                               mainReservation.invoice,
                                               mainReservation.checkin_date,
                                               mainReservation.checkout_date,
                                               mainReservation.status,
                                               customer.customer_id,
                                               customer.first_name,
                                               customer.last_name,
                                               (SELECT sum(room_bill + default_price_breakfast) * (sub1Reservation.checkout_date - sub1Reservation.checkin_date)
                                                FROM reservation sub1Reservation JOIN reservation_room ON sub1Reservation.reservation_id = reservation_room.reservation_id
                                                WHERE sub1Reservation.reservation_id = mainReservation.reservation_id
                                                GROUP BY sub1Reservation.checkout_date,sub1Reservation.checkin_date)
                                                +
                                               (SELECT if(sum(extra_bill) is null,0,sum(extra_bill))
                                                FROM reservation sub2Reservation JOIN reservation_extra ON sub2Reservation.reservation_id = reservation_extra.reservation_id
                                                WHERE sub2Reservation.reservation_id = mainReservation.reservation_id)
                                              as total
                                         FROM reservation mainReservation JOIN customer USING(customer_id)
                                                  JOIN reservation_room USING(reservation_id)
                                                  LEFT JOIN reservation_extra USING(reservation_id)
                                         WHERE mainReservation.status IN ('checkin')
                                         AND date_format(mainReservation.checkin_date,'%Y-%m-%d')  BETWEEN ? AND ?
                                         GROUP BY mainReservation.reservation_id,
                                                  mainReservation.invoice,
                                                  mainReservation.checkin_date,
                                                  mainReservation.checkout_date,
                                                  mainReservation.status,
                                                  customer.customer_id,
                                                  customer.first_name,
                                                  customer.last_name",[
                                    $dateBegin,$dateEnd
                                 ]);

       }

       if($sortBy == 'booked'){
         $dataReservation = DB::select("SELECT mainReservation.reservation_id,
                                               mainReservation.invoice,
                                               mainReservation.checkin_date,
                                               mainReservation.checkout_date,
                                               mainReservation.status,
                                               customer.customer_id,
                                               customer.first_name,
                                               customer.last_name,
                                               (SELECT sum(room_bill + default_price_breakfast) * (sub1Reservation.checkout_date - sub1Reservation.checkin_date)
                                                FROM reservation sub1Reservation JOIN reservation_room ON sub1Reservation.reservation_id = reservation_room.reservation_id
                                                WHERE sub1Reservation.reservation_id = mainReservation.reservation_id
                                                GROUP BY sub1Reservation.checkout_date,sub1Reservation.checkin_date)
                                                +
                                               (SELECT if(sum(extra_bill) is null,0,sum(extra_bill))
                                                FROM reservation sub2Reservation JOIN reservation_extra ON sub2Reservation.reservation_id = reservation_extra.reservation_id
                                                WHERE sub2Reservation.reservation_id = mainReservation.reservation_id)
                                              as total
                                         FROM reservation mainReservation JOIN customer USING(customer_id)
                                                  JOIN reservation_room USING(reservation_id)
                                                  LEFT JOIN reservation_extra USING(reservation_id)
                                         WHERE mainReservation.status IN ('booked')
                                         AND date_format(mainReservation.checkin_date,'%Y-%m-%d')  BETWEEN ? AND ?
                                         GROUP BY mainReservation.reservation_id,
                                                  mainReservation.invoice,
                                                  mainReservation.checkin_date,
                                                  mainReservation.checkout_date,
                                                  mainReservation.status,
                                                  customer.customer_id,
                                                  customer.first_name,
                                                  customer.last_name",[
                                    $dateBegin,$dateEnd
                                 ]);

       }



       foreach ($dataReservation as $key => $value) {
              $listRoom = DB::select('select room_number
                                  from reservation_room
                                  where reservation_id = ? ',[
                                     $value->reservation_id // $value->reservation_id
                                  ]);

              $roomListPerReservation[] = $listRoom;
       }

       // return var_dump($roomListPerReservation);
       //
       // return 'berhaisl';

       return view('reservationList.listReservation',[
          'dataReservation' => $dataReservation,
          'roomListPerReservation' => $roomListPerReservation
       ]);
    }

    /**
     * [checkPayment description]
     * @param  int $reservationId [description]
     * @return boolean            return true if aleady pay
     *                            return false if not pay
     */
    private function checkPayment($reservationId){
        try {
            $checkPayment = DB::select('SELECT reservation.reservation_id
                                        FROM reservation JOIN payment ON reservation.reservation_id = payment.reservation_id
                                        WHERE reservation.reservation_id = ?
                                        ',[$reservationId]);

        } catch(\Illuminate\Database\QueryException $ex){
            return $ex->getMessage();
        }

        if (empty($checkPayment)){
            return false;
        }else{
            return true;
        }
    }

    /**
     * [getOutstanding description]
     * @param  int $reservationId [description]
     * @return int         [description]
     */
    public function getOutstanding($reservationId){
        $qrRestOfBill = DB::select('select getTotalBill(?) -
                                           getTotalCustomerPayment(?)
                                           as sisa_bayar',[$reservationId,$reservationId]);


          return $qrRestOfBill[0]->sisa_bayar;
    }


    /**
     * [processCheckin description]
     * @param   int    $idReservation [description]
     * @return  string
     */
    public function processCheckin($reservationId){
        # get checkin date
        $checkinDate = DB::table('reservation')
                           ->select('checkin_date')
                           ->where('reservation_id','=',$reservationId)
                           ->first();

        $dateNow = date('Y-m-d');

        # only checkin when now == date checkin
        if ($dateNow != $checkinDate->checkin_date){
            return 'checkin only in date checkin';
            die();
        }

        if(!$this->checkPayment($reservationId)){
            return 'not yet paid';
        }

        try {
            DB::select('call checkin(?)',[$reservationId]);
        } catch(\Illuminate\Database\QueryException $ex){
            return $ex->getMessage();
        }

        return 'successful';
    }

    /**
     * [processCheckout description]
     * @param  int $idReservation [description]
     * @return string             [description]
     */
    public function processCheckout($reservationId){
        # get checkout date
        $checkoutDate = DB::table('reservation')
                           ->select('checkout_date')
                           ->where('reservation_id','=',$reservationId)
                           ->first();

        $dateNow = date('Y-m-d');

        # only checkout when now == date checkout
        if ($dateNow != $checkoutDate->checkout_date){
            return 'checkout only in date checkout';
            die();
        }

        if ($this->getOutstanding($reservationId) == 0){
            try {
                DB::select('call checkout(?)',[$reservationId]);
            } catch(\Illuminate\Database\QueryException $ex){
                return $ex->getMessage();
            }

            return 'successful';
        }else {
            return 'not yet paid off';
        }
    }

    public function checkin($reservationId){
        $reservationDate = DB::table('reservation')
                     ->select('checkin_date','checkout_date')
                     ->where('reservation_id','=',$reservationId)
                     ->first();

        return view('reservationList.checkin',[
          'reservationDate' => $reservationDate,
          'reservationId' => $reservationId
        ]);
    }

    public function addExtraItem($idReservation){
        $typesExtra = DB::table('type_extra_item')
                          ->select('extra_type_id','extra_type')
                          ->get();

        return view('reservationList.addExtraItem',[
          'typesExtra' => $typesExtra,
          'idReservation' => $idReservation
        ]);
    }

    public function showOptionExtraItem(Request $request){
        $extraTypeId = $request->extraTypeId;

        $items = DB::table('extra_item')
                     ->select('extra_id','item','price')
                     ->where('extra_type_id','=',$extraTypeId)
                     ->get();

        // return $items;
        return view('reservationList.optionExtraItem',['items' => $items]);
    }

    public function getPriceExtraItem(Request $request){
        $extraId = $request->extraId;

        $items = DB::table('extra_item')
                     ->select('price')
                     ->where('extra_id','=',$extraId)
                     ->first();

        return $items->price;
    }

    public function processAddItem(Request $request){
        $reservationId = $request->reservationId;
        $accountId = $request->session()->get('account_id');
        $extraId = $request->extraId;
        $qty = $request->qty;

        if ($qty == null){
          return 'Qty not allow empty';
        }

        if (!is_numeric($qty)){
          return 'Qty must numeric';
        }


        try {
            DB::select('call insert_reservation_extra(?,?,?,?)',[
              $reservationId,
              $accountId,
              $extraId,
              $qty
            ]);
        } catch(\Illuminate\Database\QueryException $ex){
            return $ex->getMessage();
        }
        return 'berhasil';
    }

    public function activeExtraItem(Request $request){
        $reservationId = $request->reservationId;

        $extraItem = DB::select("SELECT reservation_extra_id,
                                        extra_type,
                                        reservation_extra.item,
                                        extra_bill,
                                        qty,
                                        date_format(reservation_extra.create_at,'%Y-%m-%d') as create_at
                                 FROM reservation_extra JOIN extra_item USING(extra_id)
                                                        JOIN type_extra_item USING(extra_type_id)
                                 WHERE reservation_id = ?
                                 ORDER BY type_extra_item.extra_type ASC",[$reservationId]);
        $totalExtra = DB::table('reservation_extra')
                             ->where('reservation_id','=',$reservationId)
                             ->sum('extra_bill');
        // return $totalExtra;

        return view('reservationList.activeExtraItem',[
                    'extrasItem' => $extraItem,
                    'totalExtra' => $totalExtra
                  ]);
    }

    public function deleteReservationExtraItem(Request $request){
        $reservationExtraId = $request->reservationExtraId;

        try {
        DB::select('delete from reservation_extra
                    where reservation_extra_id = ?',[
                               $reservationExtraId
                   ]);
        } catch(\Illuminate\Database\QueryException $ex){
            return $ex->getMessage();
        }

        return 'berhasil';
    }


}
