<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class accountController extends Controller
{

      public function addAccount(Request $request){
          $username = $request->username;
          $password = $request->password;
          $level = $request->level;

          $cekUsername = DB::table('account')
                        ->select('username')
                        ->where('username','=',$username)
                        ->first();
          if($cekUsername != null){
             return 'aleady-exist';
          }

          DB::select('call insert_account(?,?,?)',[
              $username,$password,$level
          ]);
      }

      public function deleteAccount(Request $request){
          $username = $request->username;

          DB::select('call delete_account(?)',[$username]);
      }
}
