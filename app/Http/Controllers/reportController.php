<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class reportController extends Controller
{
    public function index(){
        return view('report.report');
    }

    public function tabRevenue(){
        return view('report.tab-revenue.tab-revenue');
    }

    public function tableRevenue(Request $request){
        $searchBy = $request->searchBy;
        $dateFrom = $request->dateFrom;
        $dateTo = $request->dateTo;

        return view('report.tab-revenue.table-report-revenue');
    }

    public function tabCheckin(){
        return view('report.tab-checkin.tab-checkin');
    }

    public function tableCheckin(Request $request){
        $dateFrom = $request->dateFrom;
        $dateTo = $request->dateTo;
        return view('report.tab-checkin.table-report-checkin');
    }

    public function tabPayment(){
        return view('report.tab-payment.tab-payment');
    }

    public function tablePayment(Request $request){
        // $dateFrom = $request->dateFrom;
        // $dateTo = $request->dateTo;
        return view('report.tab-payment.table-report-payment');
    }

    public function tabExtraItem(){
        return view('report.tab-extra-item.tab-extra-item');
    }

    public function tableExtraItem(Request $request){
        // $dateFrom = $request->dateFrom;
        // $dateTo = $request->dateTo;
        return view('report.tab-extra-item.table-report-extra-item');
    }

    public function tabStatistic(){
        return view('report.tab-statistic.tab-statistic');
    }

    public function tableStatistic(Request $request){
        // $dateFrom = $request->dateFrom;
        // $dateTo = $request->dateTo;
        return view('report.tab-statistic.table-report-statistic');
    }

    public function tabDailyTransaction(){
        return view('report.tab-daily-transaction.tab-daily-transaction');
    }

    public function tableDailyTransaction(Request $request){
        // $dateFrom = $request->dateFrom;
        // $dateTo = $request->dateTo;
        return view('report.tab-daily-transaction.table-report-daily-transaction');
    }


    public function tabGuest(){
        return view('report.tab-guest.tab-guest');
    }

    public function tableGuest(Request $request){
        // $dateFrom = $request->dateFrom;
        // $dateTo = $request->dateTo;
        return view('report.tab-guest.table-report-guest');
    }
}
