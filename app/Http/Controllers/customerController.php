<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class customerController extends Controller
{
    public function index(){
        return 'ini index customer';
    }

    public function updateCusomer(Request $request){
        $customerId = $request->customerId;
        $identityId = $request->identityId;
        $firstName = $request->firstName;
        $lastName = $request->lastName;
        $email = $request->email;
        $phone = $request->phone;
        $street = $request->street;
        $kelurahan = $request->kelurahan;
        $city = $request->city;
        $province = $request->province;
        $country = $request->country;

        DB::select('call update_customer(?,?,?,?,?,
                                         ?,?,?,?,?,?)',[
          $customerId,$identityId,$firstName,$lastName,$email,$phone,
          $street,$kelurahan,$city,$province,$country
        ]);
    }

    public function addCustomer(Request $request){
        $identityId = $request->identityId;
        $firstName = $request->firstName;
        $lastName = $request->lastName;
        $gender = $request->gender;
        $email = $request->email;
        $phoneNumber = $request->phoneNumber;
        $dateOfBirth = date("Y-m-d",strtotime($request->dateOfBirth));
        $organisation = $request->organisation;
        $street = $request->street;
        $city = $request->city;
        $province = $request->province;
        $postcode = $request->postcode;
        $country = $request->country;

        DB::select('call insert_customer(?,?,?,?,?,
                                         ?,?,?,?,?,
                                         ?,?,?)',[
          $identityId,$firstName,$lastName,$gender,$email,
          $phoneNumber,$dateOfBirth,$organisation,$street,$city,
          $province,$postcode,$country
        ]);
    }
}
