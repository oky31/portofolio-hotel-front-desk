<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
class extraItemController extends Controller
{
    public function addTypeExtraItem(Request $request){
        $typeExtraItem = $request->typeExtraItem;

        try {
          DB::select('call insert_type_extra_item(?)',[
               $typeExtraItem
          ]);
        } catch(\Illuminate\Database\QueryException $ex){
          return $ex->getMessage();
        }
    }

    public function addExtraItem(Request $request){
        $typeExtraItemId = $request->typeExtraItemId;
        $item = $request->item;
        $price = $request->price;

        try {
          DB::select('call insert_extra_item(?,?,?)',[
               $item,$typeExtraItemId,$price
          ]);
        } catch(\Illuminate\Database\QueryException $ex){
          return $ex->getMessage();
        }
    }
}
