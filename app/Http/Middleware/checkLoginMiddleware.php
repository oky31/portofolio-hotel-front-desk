<?php

namespace App\Http\Middleware;

use Closure;

class checkLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (!$request->session()->exists('username') &&
          !$request->session()->exists('account_id') &&
          !$request->session()->exists('level')
         ) {
          return redirect('/login');
      }
        return $next($request);
    }
}
