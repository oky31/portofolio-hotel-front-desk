<?php

namespace App\Http\Middleware;

use Closure;

class satpamMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $level = '1';
        if($request->session()->get('level') != $level ){
            return redirect('/');
        }

        return $next($request);
    }
}
