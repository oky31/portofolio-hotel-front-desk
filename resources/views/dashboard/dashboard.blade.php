@extends('master.master')

@section('title')
  Dasboard
@endsection

@section('navbar')
  @parent
@endsection

@section('content')
  <div class="row">
    <div class="span12">
      <div class="widget">
        <div class="widget-header"> <i class="icon-bookmark"></i>
          <h3>Important Shortcuts</h3>
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
          <div class="shortcuts">
              <a href="/reservation" class="shortcut">
                <i class="shortcut-icon icon-pencil"></i>
                <span class="shortcut-label">Create Reservation/Booking</span>
              </a>
              {{-- <a href="booking.html" class="shortcut">
                <i class="shortcut-icon icon-circle-arrow-left"></i>
                <span class="shortcut-label">Checkout</span>
              </a> --}}
              {{-- <a href="javascript:;" class="shortcut">
                <i class="shortcut-icon icon-wrench"></i>
                <span class="shortcut-label">Service</span>
              </a> --}}
              <a href="/reservationList" class="shortcut">
                <i class="shortcut-icon icon-list-alt"></i>
                <span class="shortcut-label">Reservation List</span>
              </a>
              {{-- <a href="/payment" class="shortcut">
                <i class="shortcut-icon icon-money"></i>
                <span class="shortcut-label">Payment</span>
              </a> --}}
              <a href="/report" class="shortcut">
                <i class="shortcut-icon icon-briefcase"></i>
                <span class="shortcut-label">Report</span>
              </a>
              <a href="/controlPanel" class="shortcut">
                <i class="shortcut-icon icon-th"></i>
                <span class="shortcut-label">Control Panel</span>
              </a>
              <a href="/calendar" class="shortcut">
                <i class="shortcut-icon icon-calendar"></i>
                <span class="shortcut-label">Calendar</span>
              </a>
              <a href="/percobaan/listPriceRoom" class="shortcut">
                <i class="shortcut-icon icon-calendar"></i>
                <span class="shortcut-label">list Price</span>
              </a>
          </div>
          <!-- /shortcuts -->
        </div>
        <!-- /widget-content -->
      </div>
    </div>

    {{-- <div class="span12">
      <div class="widget widget-nopad"> --}}
        {{-- <div class="widget-header"> <i class="icon-list-alt"></i>
          <h3> Deluxe Class</h3>
        </div> --}}
        <!-- /widget-header -->
        {{-- <div class="widget-content"> --}}
          {{-- <div id='calendar'>
          </div> --}}
        {{-- </div> --}}
        <!-- /widget-content -->
      {{-- </div> --}}

    {{-- </div> --}}

  </div>
</div>
@endsection
