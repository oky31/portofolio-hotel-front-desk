<!-- Start table room type -->
<table class="table">
  <thead>
    <tr>
      <th>Type</th>
      <th>Max Adult</th>
      <th>Max Child</th>
      <th>Price</th>
      <th>Price breakfast</th>
      <th>Action</th>
    </tr>
  </thead>
    <tbody>
      @foreach ($roomsType as $roomType)
        <tr>
          <td>{{ $roomType->type }}</td>
          <td>{{ $roomType->max_adult }}</td>
          <td>{{ $roomType->max_child }}</td>
          <td>{{ $roomType->default_price }}</td>
          <td>{{ $roomType->default_price_breakfast }}</td>
          <td>
            <button type="button"
                    name="button"
                    class="btn btn-small 	btn btn-danger"
                    onclick="deleteRoomType({{$roomType->type_id}})">
              <i class="icon-remove" style="margin-right:5px; color: #fff;"></i>
              Delete
            </button>

            <button href="#modalUpdateRoomType" onclick="getRoomUpdate({{$roomType->type_id}})"
                    role="button"
                    class="btn btn-small btn-success"
                    data-toggle="modal">
               <i class="icon-ok" style="margin-right:5px; color: #fff;"></i>
               <span class="save" style="color:#fff;">
                 Update
               </span>
             </button>
          </td>
        </tr>
      @endforeach
  </tbody>
</table>


<!-- Modal -->
  <div id="modalUpdateRoomType"
       class="modal hide fade"
       tabindex="-1"
       role="dialog"
       aria-labelledby="myModalLabel"
       aria-hidden="true">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel2">Update Room Type</h3>
        <input type="text" id="typeId" value="">
      </div>
      <div class="modal-body">
        <div class="row-fluid">
          <div class="span4">
            <label for="">Type</label>
            <input class="input-medium" type="text" placeholder="Type" id="updateType">
            <label for="">Price</label>
            <input class="input-medium" type="text" placeholder="Price" id="updateDefaultPrice">
          </div>
          <div class="span4">
            <label for="">Max adult</label>
            <input class="input-medium" type="text" placeholder="Max adult" id="updateMaxAdult">
            <label for="">Price Breakfast</label>
            <input class="input-medium" type="text" placeholder="Price breakfast"  id="updateDefaultPriceBreakfast">
          </div>
          <div class="span4">
            <label for="">Max child</label>
            <input class="input-medium" type="text" placeholder="Max child" id="updateMaxChild">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary btn-success" id="btnUpdateRoomType">
          Update
        </button>
      </div>
  </div>
</div> <!-- /controls -->
<!-- End modals -->
<!-- End table room type -->
<script type="text/javascript">
$.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
});
// process update room Type
$('#btnUpdateRoomType').click(function(){
   var typeId = $('#typeId').val();
   var updateType = $('#updateType').val();
   var updateDefaultPrice = $('#updateDefaultPrice').val();
   var updateMaxAdult = $('#updateMaxAdult').val();
   var updateMaxChild = $('#updateMaxChild').val();
   var updateDefaultPriceBreakfast = $('#updateDefaultPriceBreakfast').val();

   $.post('/room/editTypeRoom',{
      'typeId' : typeId,
      'type' : updateType,
      'defaultPrice' : updateDefaultPrice,
      'maxAdult' : updateMaxAdult,
      'maxChild': updateMaxChild,
      'defaultPriceBreakfast' : updateDefaultPriceBreakfast
   }).done(function(response){
        console.log(response);
        $('#modalUpdateRoomType').modal('hide');
          $('#showListRoomType').load('/controlPanel/showListRoomType');
   });
})
</script>
