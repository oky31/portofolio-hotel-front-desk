<div class="row-fluid">
  <form id="formAddSpecialPrice">

  <div class="span3">
    <label><strong>Room Type</strong></label>
    <select name="typeId">
      @foreach ($typesId as $key => $typeId)
        <option value="{{ $typeId->type_id }}">{{ $typeId->type }}</option>
      @endforeach
    </select>
    <label><strong>Price</strong></label>
    <input name="price" type="text">
  </div>

  <div class="span3">
    <label><strong>Time Begin</strong></label>
    <input name="timeBegin" type="date">
    <label><strong>Time End</strong></label>
    <input name="timeEnd" type="date">
  </div>
  <div class="span3">
    <label> <br></label>
    <button class="btn btn-mini pull-left" id="addSpecialPrice" type="button" style="background-color:#970067; color:#fff">
    <i class="icon-plus" style="margin-right:5px;"></i>
    <span>Add Speial Price</span>
  </button>
  </div>
</form>
</div>
<hr>

<div id="showTableListSpecialPrice">

</div>


<script type="text/javascript">
$('#addSpecialPrice').click(function(){
    var data = $('#formAddSpecialPrice').serialize();
    $.ajax({
       method : 'post',
       url : '/room/addSpecialPrice',
       data : data,
       success : function(response){
         // console.log(response);
         $('#formAddSpecialPrice').trigger('reset');
         $('#showTableListSpecialPrice').load('/controlPanel/tableListSpecialPrice');
       }
    });
})

$('#showTableListSpecialPrice').load('/controlPanel/tableListSpecialPrice');
</script>
