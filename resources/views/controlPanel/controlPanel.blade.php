@extends('master.master')

@section('title')
  Control Panel
@endsection

@section('navbar')
  @parent
@endsection

@section('content')
{{-- Begin Container --}}
<div class="container">
  {{-- Begin Row --}}
  <div class="row">
    {{-- Begin span12 --}}
    <div class="span12">
      {{-- Begin widget --}}
      <div class="widget ">
        {{-- widget Header --}}
        <div class="widget-header">
          <i class="icon-book"></i>
          <h3>Control Panel</h3>
        </div>
        {{-- End widget Header --}}

        {{-- widget Content --}}
        <div class="widget-content">
          <div class="row-fluid">
            <div class="span12">

              {{-- strat tabbable --}}
              <div class="tabbable">
                {{-- begin nav nav-tabs  --}}
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a href="#tabCustomer" data-toggle="tab" id="linkCustomer">Customer</a>
                  </li>
                  <li>
                    <a href="#tabRoom" data-toggle="tab" id="linkRoom">Room</a>
                  </li>
                  <li>
                    <a href="#formAccount" data-toggle="tab" id="linkAccount">Account</a>
                  </li>
                  @if (session('level') == '1')
                    <li>
                      <a href="#tabRoomRate" data-toggle="tab" id="linkRoomRate">Room Rate</a>
                    </li>
                  @endif
                  <li>
                    <a href="#formExtraItem" data-toggle="tab" id="linkExtraItem">Extra Item</a>
                  </li>
                  <li>
                    <a href="#tabExtraItem" data-toggle="tab" id="linkAccount">Company</a>
                  </li>
                </ul>
                {{-- begin nav nav-tabs  --}}


                {{-- Begin Tab-content --}}
                <div class="tab-content">
                    {{-- Strat Tab Customer --}}
                    <div class="tab-pane active" id="tabCustomer">
                      <div id="showTabCustomer">
                      </div>
                    </div>
                    {{-- End Tab Customer --}}


                    {{-- Strat Tab Room --}}
                    <div class="tab-pane" id="tabRoom">
                      <div class="row-fluid">
                        <div id="showTabRoom">
                        </div>
                      </div>
                    </div>
                    {{-- End Tab Room --}}


                    {{-- Strat Tab Account --}}
                    <div class="tab-pane" id="formAccount">
                      <div class="row-fluid">
                        <div id="showTabAccount">
                        </div>
                      </div>
                    </div>
                    {{-- End Tab Account --}}

                    {{-- Strat Tab Extra Item --}}
                    <div class="tab-pane" id="formExtraItem">
                      <div class="row-fluid">
                        <div id="showTabExtraItem">
                        </div>
                      </div>
                    </div>
                    {{-- End Tab Extra Item --}}

                    {{-- Strat Tab Room Rate --}}
                    <div class="tab-pane" id="tabRoomRate">
                      <div class="row-fluid">
                        <div id="showTabRoomRate">
                        </div>
                      </div>
                    </div>
                    {{-- End Tab Room Rate --}}
                </div>
                {{-- End Tab-content --}}

              </div>
              {{-- strat tabbable --}}

            </div>
          </div>
        </div>
        {{-- End Widget-content --}}

      </div>
      {{-- Begin widget --}}
    </div>
    {{-- end Span12 --}}
  </div>
{{-- End Row --}}
</div>
{{-- End Container --}}


<script type="text/javascript">
$('#showTabCustomer').load('/controlPanel/tabCustomer');
$('#linkCustomer').click(function(){
  $('#showTabCustomer').load('/controlPanel/tabCustomer');
});

$('#linkRoom').click(function(){
  $('#showTabRoom').load('/controlPanel/tabRoom');
});

$('#linkAccount').click(function(){
  $('#showTabAccount').load('/controlPanel/tabAccount');
});


$('#linkExtraItem').click(function(){
  $('#showTabExtraItem').load('/controlPanel/tabExtraItem');
});


$('#linkRoomRate').click(function(){;
  $('#showTabRoomRate').load('/controlPanel/tabRoomRate');
});

</script>
@endsection
