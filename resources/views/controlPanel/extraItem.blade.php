<div class="row-fluid">
  <input id="typeExtraItemId" type="hidden" value="{{$typeExtraId}}">

  <div class="span3">
    <label><strong>Item</strong></label>
    <input id="item" type="text">
  </div>
  <div class="span3">
    <label><strong>price</strong></label>
    <input id="price" type="text">
  </div>
  <div class="span3">
    <label> <br></label>
    <button class="btn btn-mini pull-left" id="addItem" type="button" style="background-color:#970067; color:#fff">
    <i class="icon-plus" style="margin-right:5px;"></i>
    <span>Add Item</span>
  </button>
  </div>
</div>

<div id="showListExtraItems">

</div>
{{--
@foreach ($extraItems as $key => $extraItem)
    {{ $extraItem->item }}
@endforeach --}}

<script type="text/javascript">
  var typeExtraItemId  = $('#typeExtraItemId').val();

  // console.log(typeExtraItemId);

  $.get('/controlPanel/showListExtraItems',{
    'typeExtraItemId':typeExtraItemId
  }).done(function(response){
    // console.log(response);
    $('#showListExtraItems').html(response);
  });

  $('#addItem').click(function(){
    var typeExtraItemId = $('#typeExtraItemId').val();
    var item = $('#item').val();
    var price = $('#price').val();

    $.get('/extraItem/addExtraItem',{
      'typeExtraItemId':typeExtraItemId,
      'item' : item,
      'price' : price
    }).done(function(response){
      console.log(response);
      $.get('/controlPanel/showListExtraItems',{
        'typeExtraItemId':typeExtraItemId
      }).done(function(response){
        // console.log(response);
        $('#showListExtraItems').html(response);
        $('#item').val(' ');
        $('#price').val(' ');
      });
    });

  });
</script>
