{{-- tabAccount --}}
<div class="row-fluid">
  <div class="span3">
    <label><strong>username</strong></label>
    <input id="username"  type="text" placeholder="Username">
  </div>
  <div class="span3">
    <label><strong>password</strong></label>
    <input id="password"  type="text" placeholder="password">
  </div>
  <div class="span3">
    <label><strong>Level</strong></label>
    <select name="level" id="level">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
    </select>
  </div>
  <div class="span3">
    <label> <br></label>
    <button class="btn btn-mini pull-left"
            id="addAccount"
            type="button"
            style="background-color:#970067; color:#fff">
    <i class="icon-plus" style="margin-right:5px;"></i>
    <span>Add account</span>
  </button>
  </div>
</div>

<div id="showListAccount">

</div>

<script type="text/javascript">
$('#showListAccount').load('/controlPanel/listAccount')
$('#addAccount').click(function(event){
  event.preventDefault();
  var username = $('#username').val();
  var password = $('#password').val();
  var level = $('#level').val();

  $.get('/account/addAccount',{
    'username' : username,
    'password' : password,
    'level' : level
  }).done(function(response){
    if(response == 'aleady-exist'){
       alert('username already exists !');
    }
    $('#showListAccount').load('/controlPanel/listAccount');

    $('#username').val(' ');
    $('#password').val(' ');
    $('#level').val(' ');
  });
})
</script>
