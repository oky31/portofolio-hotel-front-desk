<div class="form-actions">
  <table class="table" id="special-price-table">
    <thead>
        <tr>
          <th>Type</th>
          <th>Time begin</th>
          <th>Time end</th>
          <th>Price</th>
          <th>Create At</th>
        </tr>
    </thead>
    </tbody>
  </table>
</div>

<script type="text/javascript">
$('#special-price-table').DataTable({
     processing: true,
     serverSide: true,
     ajax: '/controlPanel/datatablesSpecialPrice'
 });
</script>
