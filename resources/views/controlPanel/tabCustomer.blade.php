<div class="row-fluid">
  <div class="span3">
    <label for="">IdentityId</label>
    <input class="" type="text" placeholder="Identity Id" id="identityId">
  </div>
  <div class="span3">
  </div>
  <div class="span3">
  </div>
  <div class="span3">
  </div>
</div>


<div class="row-fluid">
  <div class="span3">
    <label>First Name</label>
    <input class="" type="text" placeholder="first name" id="firstName">
    <label>Address</label>
    <input class="" type="text" placeholder="Organisation" id="organisation">
    <input class="" type="text" placeholder="Street Address" id="street">
  </div>
  <div class="span3">
    <label>Last Name</label>
    <input class="" type="text" placeholder="last Name" id="lastName">
    <label><br></label>
    <input class="" type="text" placeholder="City" id="city">
    <input class="" type="text" placeholder="Province" id="province">
    <input class="" type="text" placeholder="Post code" id="postcode">
    <input class="" type="text" placeholder="Country" id="country">
  </div>
  <div class="span3">
    <label>Email</label>
    <input class="" type="email" placeholder="E-mail" id="email">
    <label>Date of birth</label>
    <input class="" type="email" placeholder="dd-mm-yy" id="dateOfBirth">
  </div>
  <div class="span3">
    <label>Phone Number</label>
    <input class="" type="text" placeholder="Phone Number" id="phoneNumber">
    <label>Gender</label>
    <select class="" id="gender">
      <option value="male">Male</option>
      <option value="female">Female</option>
    </select>
  </div>
</div>


<button class="btn btn-default btn-primary" id="addCustomer">
  <i class="icon-plus" style="margin-right:5px; color:#000;"></i>
  <span id="add-customer" style="color:#000;">
    Add Customer
  </span>
</button>

<hr>
<fieldset>
  <h3>Customer List</h3>
</fieldset>
<hr>
<div class="form-actions">

<table class="table" id="customer-table">
  <thead>
      <tr>
        <th>Identity ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>E-mail</th>
        <th>Phone</th>
        {{-- <th>Street</th>
        <th>City/District</th>
        <th>Province</th>
        <th>Country</th>
        <th>Action</th> --}}
      </tr>
  </thead>
  </tbody>
</table>
</div>

  <hr>
  <hr>
  <hr>


{{-- Modal For Edit Customer --}}
<div id="myModalCustomer" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  {{-- Model Header --}}
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel2">Update Customer dddd</h3>
  </div>
  {{-- End Model Header --}}


  {{-- Modal Body --}}
  <div class="modal-body">

      <div class="row-fluid">
        <div class="span3">
          <label>First Name</label>
          <input class="" type="text" placeholder="first name" id="editFirstName">
          <label>Address</label>
          <input class="" type="text" placeholder="Organisation" id="editOrganisation">
          <input class="" type="text" placeholder="Street Address" id="editStreet">
        </div>
        <div class="span3">
          <label>Last Name</label>
          <input class="" type="text" placeholder="last Name" id="editLastName">
          <label><br></label>
          <input class="" type="text" placeholder="City" id="editCity">
          <input class="" type="text" placeholder="Province" id="editProvince">
          <input class="" type="text" placeholder="Post code" id="editPostcode">
          <input class="" type="text" placeholder="Country" id="editCountry">
        </div>
        <div class="span3">
          <label>Email</label>
          <input class="" type="email" placeholder="E-mail" id="editEmail">
          <label>Date of birth</label>
          <input class="" type="email" placeholder="dd-mm-yy" id="editDateOfBirth">
        </div>
        <div class="span3">
          <label>Phone Number</label>
          <input class="" type="text" placeholder="Phone Number" id="editPhoneNumber">
          <label>Gender</label>
          <select class="" id="editGender">
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
        </div>
      </div>
  </div>
  {{-- End Modal Body --}}


  <div class="modal-footer">
    <button class="btn btn-primary btn-success">Update</button>
  </div>
</div>
<!-- End modals -->

<script type="text/javascript">
$('#customer-table').DataTable({
     processing: true,
     serverSide: true,
     ajax: '/controlPanel/dataTablesCustomer'
 });

 $('#addCustomer').click(function(){
   var identityId = $('#identityId').val();
   var firstName = $('#firstName').val();
   var lastName = $('#lastName').val();
   var email = $('#email').val();
   var phoneNumber = $('#phoneNumber').val();

   var organisation = $('#organisation').val();
   var street = $('#street').val();
   var city = $('#city').val();
   var province = $('#province').val();
   var postcode = $('#postcode').val();

   var country = $('#country').val();
   var gender = $('#gender').val();
   var dateOfBirth = $('#dateOfBirth').val();

   // console.log(identityId + ' ' +
   //             firstName + ' ' +
   //             lastName + ' ' +
   //             email + ' ' +
   //             phoneNumber + ' ' +
   //             organisation + ' ' +
   //             street + ' ' +
   //             city + ' ' +
   //             province + ' ' +
   //             postcode + ' ' +
   //             country + ' ' +
   //             gender + ' ' +
   //             dateOfBirth);

   $.get('/customer/addCustomer',
     { 'identityId' : identityId,
       'firstName' : firstName,
       'lastName' : lastName,
       'email' : email,
       'phoneNumber' : phoneNumber,
       'organisation' : organisation,
       'street' : street,
       'city' : city,
       'province' : province,
       'postcode' : postcode,
       'country' : country,
       'gender' : gender,
       'dateOfBirth' : dateOfBirth
      }).done(function(response){
        console.log(response);
        window.location = '/controlPanel';

      });


 })
</script>
