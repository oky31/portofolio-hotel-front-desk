<div class="shortcuts">
  <a href="#" class="shortcut" id="room">
    <i class="shortcut-icon icon-bookmark-empty" id="spanRoom"></i>
    <span class="shortcut-label">Room</span>
  </a>
  <a href="#" class="shortcut" id="roomType">
    <i class="shortcut-icon icon-bookmark"></i>
    <span class="shortcut-label">Room Type</span>
  </a>
</div>


<div id="show">

</div>

<script type="text/javascript">
$('#room').click(function(event){
  event.preventDefault();
  $('#show').load('/controlPanel/showRoom');
});

$('#roomType').click(function(){
  $('#show').load('/controlPanel/showRoomType');
});
</script>
