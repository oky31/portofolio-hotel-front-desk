  <div class="form-actions">
    <table class="table">
      <thead>
        <tr>
          <th>Item Name</th>
          <th>Price</th>
          <th>Options</th>
        </tr>
      </thead>
      <tbody>

        @foreach ($extraItems as $key => $extraItem)
        <tr>
          <td>{{ $extraItem->item }}</td>
          <td>{{ $extraItem->price }}</td>
          <td>

          </td>
        </tr>
      @endforeach

      </tbody>
    </table> <!-- End table account -->
  </div>
