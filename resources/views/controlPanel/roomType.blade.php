<div class="form-actions">
    <h3> Room Type </h3>
    <form id="formAddType">
      <div class="form-inline">
        <input class="input-medium" type="text" placeholder="Type" name ="type">
        <input class="input-medium" type="text" placeholder="Max adult" name ="maxAdult">
        <input class="input-medium" type="text" placeholder="Max child" name ="maxChild">
        <input class="input-medium" type="text" placeholder="Price" name ="defaultPrice">
        <input class="input-medium" type="text" placeholder="Price breakfast"  name ="defaultPriceBreakfast">
        <button class="btn btn-small btn-inverse"
                style="margin-left:5px;" type="button" id="btnAddRoomType">
                <i class="icon-plus" style="margin-right:5px; color: #fff;"></i>
                <span class="save" style="color:#fff;">Add Room Type</span>
        </button>
      </div>
    </form>
  <hr>

  <div id="showListRoomType">
  </div>
</div>


<script type="text/javascript">
$.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
});

$('#showListRoomType').load('/controlPanel/showListRoomType');
$('#btnAddRoomType').click(function(){
   var data = $('#formAddType').serialize();
   $.post('/room/addRoomType',data).done(function(response){
      if(response == 'berhasil'){
        $('#showListRoomType').load('/controlPanel/showListRoomType');
        $('#formAddType').trigger('reset');
      }else{
        console.log(response);
      }
   });
})

// delete room Type
function deleteRoomType(idType){
  $.post('/room/deleteRoomType',{'typeId' : idType
  }).done(function(response){
    $('#showListRoomType').load('/controlPanel/showListRoomType');
  });
}

// get value update room Type
function getRoomUpdate(idType){
  $.get('/controlPanel/getRoomUpdate',{'typeId' : idType
  }).done(function(response){
    $('#typeId').val(response[0].type_id);
    $('#updateType').val(response[0].type);
    $('#updateMaxAdult').val(response[0].max_adult);
    $('#updateMaxChild').val(response[0].max_child);
    $('#updateDefaultPrice').val(response[0].default_price);
    $('#updateDefaultPriceBreakfast').val(response[0].default_price_breakfast);
    // console.log(response[0].type);
  });
}
</script>
