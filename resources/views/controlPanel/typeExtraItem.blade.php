<div class="row-fluid">

  <div id="showListTypeExtraItems">

  </div>

  <div class="span5">
    <center>
    <div class="form-actions">
      <div class="row-fluid">
        <div class="span6">
          <label>Type Extra Items</label>
          <input type="text" name="" value="" id="typeExtraItem">
        </div>
        <div class="span6">
          <label><br></label>
          <button type="button"
                  name="button"
                  class="btn"
                  id="btnProcessAddTypeExtraItems">
            Tambah
          </button>
        </div>
      </div>
    </div>
    </center>
  </div>
</div>

<script type="text/javascript">
$('#showListTypeExtraItems').load('/controlPanel/showListTypeExtraItems');

$('#btnProcessAddTypeExtraItems').click(function(){
   var typeExtraItem = $('#typeExtraItem').val();
   $.get('/extraItem/addTypeExtraItem',{
     'typeExtraItem':typeExtraItem
   }).done(function(response){
     $('#showListTypeExtraItems').load('/controlPanel/showListTypeExtraItems');
   });
});
</script>
