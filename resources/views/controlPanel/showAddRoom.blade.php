<div class="row-fluid">

  <div id="showListRoom">

  </div>



  <div class="span5">
    <center>
    <div class="form-actions">
      <div class="row-fluid">
        <div class="span6">
          <label>Room Name</label>
          <input type="text" name="" value="" id="roomName">
          <input type="hidden" name="" value="{{$typeId}}" id="typeId">
        </div>
        <div class="span6">
          <label><br></label>
          <button type="button"
                  name="button"
                  class="btn"
                  id="btnProcessAddRoom">
            Tambah
          </button>
        </div>
      </div>
    </div>
    </center>
  </div>
</div>

<script type="text/javascript">
var globalTypeId = $('#typeId').val();
$.get('/controlPanel/showListRoom',{
  'typeId' : globalTypeId
}).done(function(response){
  $('#showListRoom').html(response);
})

$('#btnProcessAddRoom').click(function(){
  var typeId = $('#typeId').val();
  var roomName = $('#roomName').val();
    $.get('/room/addRoom',{
      'typeId' : typeId,
      'roomName' : roomName
    }).done(function(response){
      $.get('/controlPanel/showListRoom',{
        'typeId' : typeId
      }).done(function(response){
        $('#showListRoom').html(response);
      })
      // console.log(response);
    })
});
</script>
