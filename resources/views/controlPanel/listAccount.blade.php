<div class="form-actions">
  <table class="table">
    <thead>
        <tr>
            <th>Username</th>
            <th>Password</th>
            <th>Level</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
      @foreach ($accounts as $key => $account)

      <tr>
          <td>{{$account->username}}</td>
          <td>{{$account->password}}</td>
          <td>{{$account->level}}</td>
          <td>
              {{-- <a href="#myModalAccount"
                 role="button"
                 class="btn btn-small btn-success"
                 style="margin-left:5px; margin-bottom:5px;"
                 data-toggle="modal">
                 <i class="icon-ok" style="margin-right:5px; color: #fff;"></i>
                 <span class="save" style="color:#fff;">
                   Update
                 </span>
               </a>
	          <button class="btn btn-small btn-inverse"
                    style="margin-left:5px;"
                    type="submit">
                    <i class="icon-remove" style="margin-right:5px; color: #fff;"></i>
                    <span class="save" style="color:#fff;">
                      Delete
                    </span>
            </button> --}}
          </td>
      </tr>
      @endforeach
  </tbody>
</table> <!-- End table account -->
</div>


{{-- Modal Edit Account --}}
<div id="myModalAccount" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="myModalLabel2">Update Account</h3>
    </div>
    <div class="modal-body">
      <!-- Start modal update -->
      <h3>Account Detail</h3>
      <!-- Start modals -->
      <input name="username" class="span4" type="text" placeholder="Username">
      <input name="password" class="span4" type="text" placeholder="Password">
      <select name="level" class="span4">
      <option>Level</option>
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      </select>
            <!-- End modal update -->
    </div>
    <div class="modal-footer">
      <button class="btn btn-primary btn-success">Update</button>
    </div>
</div>
