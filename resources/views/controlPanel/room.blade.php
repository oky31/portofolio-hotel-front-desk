@foreach ($roomsType as $key => $roomType)
  <div class="form-actions">
    <div class="row-fluid">
      <div class="span4">
        <button type="button"
                name="button"
                class="btn btn-small"
                onclick="goToAddRoom({{$roomType->type_id}})"
                style="background-color:#970067; color:#ffffff">
          add room
        </button>
        <br>
        <h3 class="text-center">{{ $roomType->type }}</h3>
      </div>
      <div class="span8">
        <table class="table">
          <tr>
            <th>Price</th>
            <th>Price Breakfast</th>
            <th>Max Adult</th>
            <th>Max Child</th>
          </tr>
          <tr>
            <td>{{ $roomType->default_price }}</td>
            <td>{{ $roomType->default_price_breakfast }}</td>
            <td>{{ $roomType->max_adult }}</td>
            <td>{{ $roomType->max_child }}</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
@endforeach


{{--
<div class="form-actions">
  <h3> Delux</h3>
	<hr>
</div> --}}

<script type="text/javascript">
// add Room
$('#btnAddRoom').click(function(){
   $('#showListRoom').load('/controlPanel/showListRoom');
})

function goToAddRoom(typeId){
  $.get('/controlPanel/showAddRoom',{
    'typeId':typeId
  }).done(function(response){
    $('#show').html(response);
  });
}

function deleteRoom(roomId){
  console.log(roomId);
}
</script>
