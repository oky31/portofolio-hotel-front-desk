@extends('master.master')

@section('title')
  Payment
@endsection

@section('navbar')
  @parent
@endsection

@section('content')
  <div class="container">
      <div class="row">
        <div class="span12">
            <div class="widget">
                <div class="widget-content">
                <!-- Start tab payments-->
                    {{-- <div class="form-actions">
                      <div class="form-inline"> --}}
                          <b>
                            {{-- <span class="rest-bill pull-left" style="font-size:12px;">
                              Total Outstanding : Rp.<span ></span>
                            </span> --}}
                          </b>
                          {{-- <b>
                            <span class="deposite pull-right" style="font-size:12px;">
                                Deposite : Rp.<span id="deposit"></spand>
                             </span>
                          </b> --}}
                      {{-- </div>
                    </div> --}}

                    <input type="hidden" name=""  id="totalOutstandingTetap">
                    <input type="hidden" id="reservationId" name="" value="{{$reservationId}}">
                    <div class="row">
                      <div class="span7">
                        <div class="form-actions">
                          <div class="row">
                            <div class="span2">
                              <label>Transaction Type</label>
                              <select class="span2" id="transactionType">
                                <option>Payment</option>
                                <option>Deposit</option>
                              </select>
                            </div>
                            <div class="span2">
                              <label>Payment Type</label>
                              <select class="span2" id="paymentType">
                                <option>Cash</option>
                                <option>Transfer</option>
                              </select>
                            </div>
                            <div class="span2">
                              <label>Descripton</label>
                              <input type="text" placeholder="description" id="description">
                            </div>
                          </div>
                          <div class="row">
                            <div class="span2">
                              <label>Total Pay</label>
                              <input class="span2" type="text" placeholder="Total Pay" id="totalPayment" >
                            </div>
                            {{-- <div class="span2">
                              <label>Acceptance</label>
                              <input class="span2" type="text" placeholder="Acceptance" id="acceptance">
                            </div> --}}
                            {{-- <div class="span2">
                              <label>Change</label>
                              <input class="span2" type="text" readonly id="change">
                            </div> --}}
                            <div class="span2">
                              <label><br></label>
                              <button class="btn btn-small pull-left"  id="btn-pay"
                                      style="margin-left:5px; background-color: #970067;"
                                      type="button">
                                      <i class="icon-ok" style="margin-right:5px; color: #fff;"></i>
                                      <span class="save" style="color:#fff;">
                                        confirm
                                      </span>
                              </button>
                            </div>
                          </div>
                          </div>
                        </div>
                          <!-- Start booking summary -->
                          <div class="span4">
                            <div class="new-form">
                              <h3>Booking Summary</h3>
                              <br>
                              <div class="form-inline">
                                <span class="room-header pull-left">Room Bill :</span>
                                <span class="pull-right" id="roomBill">Rp.{{ $roomBill[0]->total }}</span>
                              </div>
                              <br>
                              <div class="form-inline">
                                <span class="room-header pull-left">Extra Bill :</span>
                                <span class="pull-right">
                                  Rp. <span id="extraBill">{{ $extraBill[0]->extra_bill }}</span>
                                </span>
                              </div>
                              <br>
                              <hr>
                              <div class="form-inline">
                                <span class="room-header pull-left">Total :</span>
                                <span class="pull-right">
                                  Rp.<span>{{$total}}</span>
                                </span>
                              </div>
                              <br>
                              <div class="form-inline">
                                <span class="room-header pull-left">Total Received:</span>
                                <span class="pull-right">Rp.<span id="totalReceived"></span></span>
                              </div>
                              <hr>
                              <div class="form-inline">
                                <span class="room-header pull-left" style="color:red">
                                  <strong>Total Outstanding</strong>
                                </span>
                                <span class="pull-right" style="color:red">
                                  <strong>
                                    Rp.<span id="totalOutstanding"></span>
                                  </strong>
                                </span>
                              </div>
                              <br>
                            </div>
                          </div>
                          <!-- End booking summary -->
                        </div>

                        <div id="showHistoryPaymentPerReservation">

                        </div>
                        <hr>
                  </div>
            </div>
        </div>

        <div id="showList">

        </div>

      </div><!-- /row -->
  </div><!-- /container -->

<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('#showListReservation').click(function(){
  var dateBegin = $('#dateBegin').val();
  var dateEnd = $('#dateEnd').val();

  $.get('/payment/showListReservation',
    {'dateBegin' : dateBegin,
     'dateEnd' : dateEnd}
  ).done(function(response){
    $('#showList').html(response);
    // console.log(response);
  });

})

// process pay
$('#btn-pay').click(function(){
   var transactionType = $('#transactionType').val();
   var description = $('#description').val();
   var paymentType = $('#paymentType').val();
   var acceptance = $('#acceptance').val();
   var totalPayment = $('#totalPayment').val();
   var totalBill = $('#totalBill').val();
   var reservationId = $('#reservationId').val();

   // console.log(transactionType + ' ' +
   //             description + ' ' +
   //             paymentType + ' ' +
   //             acceptance + ' ' +
   //             totalPayment + ' ' +
   //             totalBill + ' ' +
   //             reservationId
   //           )
   $.get('/payment/processPayment',{
     'reservationId' : reservationId,
     'transactionType' : transactionType,
     'description' : description,
     'paymentType' : paymentType,
     'acceptance' : acceptance,
     'totalBill' : totalBill,
     'totalPayment' : totalPayment
   }).done(function(response){
       console.log(response);
       // Get Total Outstanding
       $.get('/payment/getOutstanding/',{
         'reservationId' : globalReservationId
       }).done(function(response){
         $('#totalOutstanding').text(response);
         $('#totalOutstandingTetap').val(response);
       });

       // kosongkan form
       $('#description').val('');
       $('#acceptance').val('');
       $('#totalPayment').val('');

       // Get Hitory payment per reservation
       $.get('/payment/historyPaymentPerReservation/',{
         'reservationId' : globalReservationId
       }).done(function(response){
         $('#showHistoryPaymentPerReservation').html(response);
         // console.log(response);
       });

       // Get deposit
       $.get('/payment/getDeposit/',{
         'reservationId' : globalReservationId
       }).done(function(response){
         $('#deposit').text(response);
       });

       // Get Received
       $.get('/payment/getTotalReceived/',{
         'reservationId' : globalReservationId
       }).done(function(response){
         $('#totalReceived').html(response);
         console.log(response);
       });

   });
});

$('#totalPayment').keyup(function(){
    var totalOutstanding =  $('#totalOutstandingTetap').val();
    var totalPayment = $(this).val();
    // console.log(totalOutstanding +' ' + acceptance)
    $('#totalOutstanding').text(totalOutstanding - totalPayment);
});

// menghitung kembalian
$('#acceptance').keyup(function(){
     // console.log('helo');
    var totalPayment = $('#totalPayment').val();
    var acceptance = $(this).val();
    $('#change').val(acceptance - totalPayment);
});




// Get Total Outstanding
var globalReservationId = $('#reservationId').val();
$.get('/payment/getOutstanding/',{
  'reservationId' : globalReservationId
}).done(function(response){
  $('#totalOutstanding').text(response);
  $('#totalOutstandingTetap').val(response);
});

// Get Hitory payment per reservation
$.get('/payment/historyPaymentPerReservation/',{
  'reservationId' : globalReservationId
}).done(function(response){
  $('#showHistoryPaymentPerReservation').html(response);
  // console.log(response);
});

// Get Received
$.get('/payment/getTotalReceived/',{
  'reservationId' : globalReservationId
}).done(function(response){
  $('#totalReceived').html(response);
  console.log(response);
});

// Get deposit
$.get('/payment/getDeposit/',{
  'reservationId' : globalReservationId
}).done(function(response){
  $('#deposit').text(response);
});
</script>

@endsection
