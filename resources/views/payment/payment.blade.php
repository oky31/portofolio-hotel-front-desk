@extends('master.master')

@section('title')
  Payment
@endsection

@section('navbar')
  @parent
@endsection

@section('content')
  <div class="container">
      <div class="row">
        <div class="span12">
            <div class="widget">

                <!-- /widget-header -->
                <div class="widget-content">
                  <input type="date" name="" value="{{date('Y-m-d')}}" id="dateBegin"> --
                  <input type="date" name="" value="{{date('Y-m-d')}}" id="dateEnd"><br>
                  <button id="showListReservation" type="button" name="button" class="btn">
                    serach
                  </button>
                </div><!-- /widget -->
            </div>
        </div>

        <div id="showList">

        </div>
      </div><!-- /row -->
  </div><!-- /container -->

<script type="text/javascript">
$('#showListReservation').click(function(){
  var dateBegin = $('#dateBegin').val();
  var dateEnd = $('#dateEnd').val();

  $.get('/payment/showListReservation',
    {'dateBegin' : dateBegin,
     'dateEnd' : dateEnd}
  ).done(function(response){
    $('#showList').html(response);
    // console.log(response);
  });
})

$('#showList').load('payment/showAllListReservation');
</script>

@endsection
