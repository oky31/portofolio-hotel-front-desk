<div class="span12" >
  <div class="widget">
    <div class="widget-header"> <i class="icon-bookmark"></i>
      <h3 id="dateCheckinTime"></h3>
    </div>
    <!-- /widget-header -->
    <div class="widget-content">
      <div class="shortcuts">
        @foreach ($allReservations as $key =>$reservation)
        <a href="/payment/reservationDetail/{{$reservation->reservation_id }}" class="shortcut">
          <span class="shortcut-label">
            {{ $reservation->reservation_id }}<br>
            {{ $reservation->first_name.' '.$reservation->last_name }}
            <hr>
            room
          </span>
        </a>
        @endforeach
      </div><!-- /shortcuts -->
    </div><!-- /widget -->
  </div>
</div>
