
<div class="row">
  <div class="span7">
    <table class="table">
      <thead>
        <tr>
          <th>Transaction type</th>
          <th>Payment type</th>
          <th class="text-right">Total pay </th>
          <th>Descripton</th>
          <th class="text-center">Time</th>
          <th>Username</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($historyPayment as $payment)
          <tr>
            <td>{{$payment->transaction_type}}</td>
            <td>{{$payment->payment_type}}</td>
            <td class="text-right">{{$payment->total}} </td>
            <td>{{$payment->description}} </td>
            <td>{{$payment->create_at}} </td>
            <td>{{$payment->username}}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
