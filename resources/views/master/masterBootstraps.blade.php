<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Calendar</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <!-- Full Calendar Plugins css
    ================================================== -->
    <link href="{{ asset('js/full-calendar/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/fullcalendar-scheduler-1.9.3/scheduler.css') }}" rel="stylesheet">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('vendor/jQuery/jquery-3.3.1.min.js') }}" ></script>
    <script src="{{ asset('vendor/bootstrap/dist/js/popper.min.js')}}"></script>
    <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Full Calendar Plugins JavaScript
    ================================================== -->
    <script src="{{ asset('js/full-calendar/lib/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/full-calendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('js/full-calendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('vendor/fullcalendar-scheduler-1.9.3/scheduler.js') }}"></script>

  </head>

  <body>
    @section('navbar')
      <nav class="navbar navbar-expand-md bg-elit-hotel fixed-top">
        <a class="navbar-brand text-white" href="#">Calendar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
          <ul class="navbar-nav mr-auto">
            {{-- <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li> --}}

        </ul>
      </div>
    </nav>
    @show

    <main role="main" class="container">
      @yield('mainContent')
    </main><!-- /.container -->
  </body>


</html>
