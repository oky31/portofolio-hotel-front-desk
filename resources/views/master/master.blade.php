<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>@yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-responsive.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">

{{-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet"> --}}
<link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/pages/dashboard.css') }}" rel="stylesheet">
{{-- Datatables css --}}
<link href="{{ asset('vendor/datatables/DataTables-1.10.16/css/jquery.dataTables.min.css') }}" rel="stylesheet">
{{-- Full calender css  --}}
<link href="{{ asset('js/full-calendar/fullcalendar.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/fullcalendar-scheduler-1.9.3/scheduler.css') }}" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<meta name="csrf-token" content="{!! csrf_token() !!}" />
<script src="{{ asset('js/jquery-1.7.2.min.js') }}"></script>
<script src="{{ asset('js/excanvas.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/tooltip.js') }}"></script>
<script src="{{ asset('js/popover.js') }}"></script>
{{-- Data tables --}}
<script src="{{ asset('vendor/datatables/DataTables-1.10.16/js/jquery.dataTables.min.js') }}"></script>
</head>
<body>

{{-- <img src="img/Header/logofullhitam.png" class="img-circle" style="width: 110px; height:53px;"> --}}



@section('header')
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a class="brand" href="index.html">
          <img src="{{ asset('img/Header/logofullhitam.png')}}" class="img-circle" style="width: 110px; height:45px;">
        </a>
        <div class="nav-collapse">
          <ul class="nav pull-right">
            {{-- <li class="dropdown" style="margin-top: 20px;"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
              class="icon-cog"></i> Account <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="javascript:;">Settings</a></li>
                <li><a href="javascript:;">Help</a></li>
              </ul>
            </li> --}}
            <li class="dropdown" style="margin-top: 20px;"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                class="icon-user"></i> {{ session('username') }} <b class="caret"></b></a>
              <ul class="dropdown-menu">
                {{-- <li><a href="javascript:;">Profile</a></li> --}}
                <li><a href="/login/logout">Logout</a></li>
              </ul>
            </li>
          </ul>
        </div>
        <!--/.nav-collapse -->
      </div>
      <!-- /container -->
    </div>
    <!-- /navbar-inner -->
  </div>

@section('navbar')
  <!-- /navbar -->
  <div class="subnavbar">
    <div class="subnavbar-inner">
      <div class="container">
        <ul class="mainnav">
          <li>
            <a href="/">
              <i class="icon-dashboard"></i>
              <span>Dashboard</span>
            </a>
          </li>
          <li>
            <a href="/reservation">
              <i class="icon-pencil"></i>
              <span>Create Reservation</span>
            </a>
          </li>
          <li>
            <a href="/reservationList">
              <i class="icon-list-alt"></i>
              <span>Reservation List</span>
            </a>
          </li>
          {{-- <li>
            <a href="/payment">
              <i class="icon-money"></i>
              <span>Payment</span>
            </a>
          </li> --}}
          <li>
            <a href="/report">
              <i class="icon-briefcase"></i>
              <span>Report</span>
            </a>
          </li>
            <li>
              <a href="/controlPanel">
                <i class="icon-th"></i>
                <span>Control Panel</span>
              </a>
            </li>
        </ul>
      </div>
      <!-- /container -->
    </div>
    <!-- /subnavbar-inner -->
  </div>
  <!-- /subnavbar -->
@show

  <div class="main">
    <div class="main-inner">
      <div class="container">
        @yield('content')
      </div>
      <!-- /container -->
    </div>
    <!-- /main-inner -->
  </div>
  <!-- /main -->


@section('footer')
  {{-- footer --}}
  <div class="extra">
    <div class="extra-inner">
      <div class="container">
        <div class="row">
          <div class="span3">
            <a class="brand" href="index.html">
              <img src="{{asset('img/elitehotel.gif')}}" class="img-circle" style="width: 110px; height:45px; margin-bottom:15px;">
            </a>
            <ul>
              <li><a href="javascript:;">Address : Abdul Manaf, St.. , Tembilahan </a></li>
              <li><a href="javascript:;">Email : elitehotels@outlook.com</a></li>
              <li><a href="javascript:;">Phone : +62-813-0986-3462</a></li>
            </ul>
          </div>
          <!-- /span3 -->
          <div class="span3">
            <h3>About Us</h3>
            <ul>
              <li><a href="javascript:;">Frequently Asked Questions</a></li>
              <li><a href="javascript:;">Ask a Question</a></li>
              <li><a href="javascript:;">Video Tutorial</a></li>
              <li><a href="javascript:;">Feedback</a></li>
            </ul>
          </div>
          <!-- /span3 -->
          <div class="span3">
            <h3>Something Legal</h3>
            <ul>
              <li><a href="javascript:;">Read License</a></li>
              <li><a href="javascript:;">Terms of Use</a></li>
              <li><a href="javascript:;">Privacy Policy</a></li>
            </ul>
          </div>
          <!-- /span3 -->
          <div class="span3">
            <h3>Our Service</h3>
            <ul>
              <li><a href="">Meeting Room</a></li>
              <li><a href="">Guest House</a></li>
              <li><a href="">Restaurant</a></li>
              <li><a href="">Laundry</a></li>
              <li><a href="">Technology Support</a></li>
              <li><a href="">Internet Access</a></li>
            </ul>
          </div>
          <!-- /span3 -->
        </div>
        <!-- /row -->
      </div>
      <!-- /container -->
    </div>
    <!-- /extra-inner -->
  </div>
  <!-- /extra -->
  <div class="footer">
    <div class="footer-inner">
      <div class="container">
        <div class="row">
          <div class="span12">
            <center>
              <a href="index.html">&copy;Elite Hotels Management</a>
            </center>
          </div>
          <!-- /span12 -->
        </div>
        <!-- /row -->
      </div>
      <!-- /container -->
    </div>
    <!-- /footer-inner -->
  </div>
  <!-- /footer -->

</body>
</html>
