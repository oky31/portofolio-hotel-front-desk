@extends('master.master')

@section('title')
  Payment
@endsection

@section('navbar')
  @parent
@endsection

@section('content')
  <div class="container">
      <div class="row">
        <div class="span12">
            <div class="widget">
              {{-- widget-header --}}
              <div class="widget-header">
                <i class="icon-book"></i>
                <h3>Control Panel</h3>
              </div>
              {{-- end widget-header --}}


              {{--  Widget-content --}}
              <div class="widget-content">
                <div class="row-fluid">
                  <div class="span12">

                    {{-- Begin tabbable --}}
                    <div class="tabbable">
                      {{-- begin nav-for tabbable --}}
                      <ul class="nav nav-tabs">
                        <li>
                          <a href="#tab-report-revenue" class="active" data-toggle="tab" id="link-tab-report-revenue">
                            Revenue
                          </a>
                        </li>
                        <li>
                          <a href="#tab-report-checkin" data-toggle="tab" id="link-tab-report-checkin">
                            Check In
                          </a>
                        </li>
                        <li>
                          <a href="#tab-report-payment" data-toggle="tab" id="link-tab-report-payment">
                            Payment
                          </a>
                        </li>
                        <li>
                          <a href="#tab-report-extra-item" data-toggle="tab" id="link-tab-report-extra-item">
                            Extras
                          </a>
                        </li>
                        <li>
                          <a href="#tab-report-statistic" data-toggle="tab" id="link-tab-report-statistic">
                            Statistics
                          </a>
                        </li>
                        <li>
                          <a href="#tab-report-daily-transaction" data-toggle="tab" id="link-tab-report-daily-transaction">
                            Daily Transaction
                          </a>
                        </li>
                        <li>
                          <a href="#tab-report-guest" data-toggle="tab" id="link-tab-report-quest">
                            Guest
                          </a>
                        </li>
                      </ul>
                      {{-- End nav-for tabbable --}}

                      <br>
                      {{-- Begin Tab Content --}}
                      <div class="tab-content">
                          {{-- Tab revenue  --}}
                          <div class="tab-pane" id="tab-report-revenue">
                          </div>
                          {{-- End Tab revenue  --}}

                          {{-- Tab check In  --}}
                          <div class="tab-pane" id="tab-report-checkin">
                          </div>
                          {{-- End Tab check In  --}}

                          {{-- Tab payment  --}}
                          <div class="tab-pane" id="tab-report-payment">
                          </div>
                          {{-- End Tab payment  --}}

                          {{-- Tab extra item  --}}
                          <div class="tab-pane" id="tab-report-extra-item">
                          </div>
                          {{-- End Tab extra item  --}}

                          {{-- Tab statistics  --}}
                          <div class="tab-pane" id="tab-report-statistic">
                          </div>
                          {{-- End Tab statistics  --}}

                          {{-- Tab Daily transaction  --}}
                          <div class="tab-pane" id="tab-report-daily-transaction">
                          </div>
                          {{-- End Tab Daily transaction --}}

                          {{-- Tab Guest  --}}
                          <div class="tab-pane" id="tab-report-guest">
                          </div>
                          {{-- End Tab Guest --}}

                      </div>
                      {{--  End tab content --}}

                    </div>

                  </div>
                  <!-- End span12 -->
                </div>
                <!-- End row-fluid -->
              </div>
              {{-- End Widget-Content --}}
            </div>
        </div>

      </div>
      {{-- End Row --}}
  </div>
  {{-- End container --}}

  <script type="text/javascript">
      // show tab-report-revenue
      $('#link-tab-report-revenue').click(function(){
        $('#tab-report-revenue').load('report/tabRevenue');
      })

      // show tab-report-checkin
      $('#link-tab-report-checkin').click(function(){
        $('#tab-report-checkin').load('report/tabCheckin');
      })

      // show tab-report-payment
      $('#link-tab-report-payment').click(function(){
        $('#tab-report-payment').load('report/tabPayment');
      })

      // show tab-report-extra-item
      $('#link-tab-report-extra-item').click(function(){
        $('#tab-report-extra-item').load('report/tabExtraItem');
      })

      // show tab-report-statistic
      $('#link-tab-report-statistic').click(function(){
        $('#tab-report-statistic').load('report/tabStatistic');
      })

      // show tab-report-daily-transaction
      $('#link-tab-report-daily-transaction').click(function(){
        $('#tab-report-daily-transaction').load('report/tabDailyTransaction');
      })

      // show tab-report-daily-transaction
      $('#link-tab-report-quest').click(function(){
        $('#tab-report-guest').load('report/tabGuest');
      })
  </script>
@endsection
