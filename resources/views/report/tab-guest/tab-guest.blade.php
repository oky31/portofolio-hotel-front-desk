<div class="form-actions">
  <div class="row-fluid">
    <div class="span3">
      <label>Date From</label>
      <div class="input-append date">
        <input class="span2" size="16" type="date" value="" name="dateFrom">
      </div>
    </div>
    <div class="span3">
      <label>Date To</label>
      <div class="input-append date">
        <input class="span2" size="16" type="date" value="" name="dateTo">
      </div>
    </div>
    <label><br></label>
    <button class="btn btn-small btn-primary pull-right"
            style="margin-left:5px;"
            type="button"
            id = "btn-run-report-guest">
            <i class="icon-book" style="margin-right:5px; color: #fff;"></i>
            <span class="save" style="color:#fff;">
              Run Report
            </span>
    </button>
  </div>
</div>

{{-- Show table guest --}}
<div id="show-table-guest">

</div>

<script type="text/javascript">
$('#btn-run-report-guest').click(function(){
  var data = $('#form-filter-report-guest').serialize();

  $.ajax({
      url : 'report/tableGuest',
      method : 'get',
      data : data,
      success : function(response){
        $('#show-table-guest').html(response);
      }
  })
})
</script>
