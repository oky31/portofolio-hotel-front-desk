<div class="form-actions">
  <h3>Guest</h3>
  <hr>
  <table class="table table-striped">
          <thead class="tableHead" style="border-top:solid 2px #000; border-bottom:solid 2px #000;">
              <tr>
                <th>Check-In Date</th>
                <th>Check-Out Date</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone</th>
            </tr>
          </thead>
            <tbody>
              <tr>
                  <td><b>14-02-2018</b></td>
                  <td><b>17-02-2018</b></td>
                  <td>Mirza</td>
                  <td>Sholihul</td>
                  <td>mirza@gmail.com</td>
                  <td><b>+62-097-8787-985</b></td>
              </tr>
            </tbody>
      </table>
      <button class="btn btn-small btn-success pull-right" style="margin-left:5px;" type="submit"><i class="icon-print" style="margin-right:5px; color: #fff;"></i><span class="save" style="color:#fff;">Export</span>
  </button>
      <div class="btn-group pull-left">
    <button class="btn"><i class="icon-chevron-left" style="color: #000;"></i></button>
    <button class="btn">1</button>
    <button class="btn">2</button>
    <button class="btn">3</button>
    <button class="btn">4</button>
    <button class="btn">5</button>
    <button class="btn"><i class="icon-chevron-right" style="color: #000;"></i></button>
  </div>
  </div><!-- End form-action -->
<!-- End tableGuest -->
<button class="btn btn-small btn-inverse pull-right" style="margin-left:5px;" type="submit"><i class="icon-print" style="margin-right:5px; color: #fff;"></i><span class="save" style="color:#fff;">Print</span>
</button>
