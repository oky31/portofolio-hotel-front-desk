<div class="form-actions">
  <form id="form-filter-report-statistic">
  <div class="row-fluid">
    <div class="span2">
      <label>Search By</label>
      <select class="span2">
        <option>Stay date</option>
        <option>Booked-on</option>
      </select>
    </div>
    <div class="span3">
      <label>Date From</label>
      <div class="input-append date">
        <input class="span2" size="16" type="date" value="" name="dateFrom">
      </div>
    </div>
    <div class="span3">
      <label>Date To</label>
      <div class="input-append date">
        <input class="span2" size="16" type="date" value="" name="dateTo">
      </div>
    </div>
    {{-- <div class="span2">
      <label class="checkbox inline">
        <input type="checkbox" id="inlineCheckbox1" value="option1"> Revenue Include Tax
      </label>
    </div> --}}
    <label><br></label>
    <button class="btn btn-small btn-primary pull-right"
            style="margin-left:5px;"
            type="button"
            id = "btn-run-report-statistic">
            <i class="icon-book" style="margin-right:5px; color: #fff;"></i>
            <span class="save" style="color:#fff;">
              Run Report
            </span>
    </button>
  </div>
  </form>
</div>


{{-- Show table statistic --}}
<div id="show-table-statistic">

</div>

<script type="text/javascript">
$('#btn-run-report-statistic').click(function(){
  var data = $('#form-filter-report-statistic').serialize();

  $.ajax({
      url : 'report/tableStatistic',
      method : 'get',
      data : data,
      success : function(response){
        $('#show-table-statistic').html(response);
      }
  })
})
</script>
