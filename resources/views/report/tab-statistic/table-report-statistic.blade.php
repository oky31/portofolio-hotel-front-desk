<div class="form-actions">
  <table class="table table-striped">
          <thead class="tableHead" style="border-top:solid 2px #000; border-bottom:solid 2px #000;">
              <tr>
                <th>Daily Statistics</th>
                <th></th>
            </tr>
          </thead>
            <tbody>
              <tr>
                  <td>Total Room Night</td>
                  <td>Rp0</td>
              </tr>
              <tr>
                  <td>Close Room</td>
                  <td>Rp0</td>
              </tr>
              <tr>
                  <td>Occupied Room</td>
                  <td>Rp0</td>
              </tr>
            </tbody>
      </table>
      <button class="btn btn-small btn-success pull-right" style="margin-left:5px;" type="submit"><i class="icon-print" style="margin-right:5px; color: #fff;"></i><span class="save" style="color:#fff;">Export</span>
  </button>
      <div class="btn-group pull-left">
    <button class="btn"><i class="icon-chevron-left" style="color: #000;"></i></button>
    <button class="btn">1</button>
    <button class="btn">2</button>
    <button class="btn">3</button>
    <button class="btn">4</button>
    <button class="btn">5</button>
    <button class="btn"><i class="icon-chevron-right" style="color: #000;"></i></button>
  </div>
  </div><!-- End form-action -->


  <button class="btn btn-small btn-inverse pull-right" style="margin-left:5px;" type="submit"><i class="icon-print" style="margin-right:5px; color: #fff;"></i><span class="save" style="color:#fff;">Print</span>
  </button>
