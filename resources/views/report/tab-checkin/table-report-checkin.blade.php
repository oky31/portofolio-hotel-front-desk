<div class="form-actions">
  <h3>Check In</h3>
  <br>
  <table class="table table-striped">
          <thead class="tableHead" style="border-top:solid 2px #000; border-bottom:solid 2px #000;">
              <tr>
                <th>Reservation Number</th>
                <th>Invoice Number</th>
                <th>Guest Name</th>
                <th>Check In</th>
                <th>LoS</th>
                <th>Room Number</th>
                <th>Adult/Children/Infants</th>
                <th>Extra Person</th>
                <th>Outstanding Balance</th>
                <th>Total Amount</th>
                <th>ETA</th>
                <th>Notes</th>
              </tr>
          </thead>
            <tbody>
              <tr>
                  <td>LH7657158129</td>
                  <td>6859-2</td>
                  <td>Sholihul,Mirza</td>
                  <td>14 Mar 2018</td>
                  <td>1</td>
                  <td>Room 1</td>
                  <td>2/0/0</td>
                  <td>0</td>
                  <td>Rp.0</td>
                  <td>Rp.1.100.000</td>
                  <td></td>
                  <td></td>
              </tr>
            </tbody>
  </table>
      <button class="btn btn-small btn-inverse pull-right" style="margin-left:5px;" type="submit"><i class="icon-print" style="margin-right:5px; color: #fff;"></i><span class="save" style="color:#fff;">Print Registration Form</span>
  </button>
  <button class="btn btn-small btn-info pull-right" style="margin-left:5px;" type="submit"><i class="icon-print" style="margin-right:5px; color: #fff;"></i><span class="save" style="color:#fff;">Export</span>
  </button>
      <div class="btn-group pull-left">
    <button class="btn"><i class="icon-chevron-left" style="color: #000;"></i></button>
    <button class="btn">1</button>
    <button class="btn">2</button>
    <button class="btn">3</button>
    <button class="btn">4</button>
    <button class="btn">5</button>
    <button class="btn"><i class="icon-chevron-right" style="color: #000;"></i></button>
  </div>
  </div><!-- End form-action -->
  <div class="form-actions">
  <h3>Check Out</h3>
  <br>
  <table class="table table-striped">
          <thead class="tableHead" style="border-top:solid 2px #000; border-bottom:solid 2px #000;">
              <tr>
                <th>Reservation Number</th>
                <th>Invoice Number</th>
                <th>Guest Name</th>
                <th>Check Out</th>
                <th>LoS</th>
                <th>Room Number</th>
                <th>Adult/Children/Infants</th>
                <th>Extra Person</th>
                <th>Outstanding Balance</th>
                <th>Total Amount</th>
                <th>Notes</th>
              </tr>
          </thead>
            <tbody>
              <tr>
                  <td>LH7657158129</td>
                  <td>6859-2</td>
                  <td>Sholihul,Mirza</td>
                  <td>14 Mar 2018</td>
                  <td>1</td>
                  <td>Room 1</td>
                  <td>2/0/0</td>
                  <td>0</td>
                  <td>Rp.0</td>
                  <td>Rp.1.100.000</td>
                  <td></td>
              </tr>
            </tbody>
      </table>
      <button class="btn btn-small btn-inverse pull-right" style="margin-left:5px;" type="submit"><i class="icon-print" style="margin-right:5px; color: #fff;"></i><span class="save" style="color:#fff;">Print Registration Form</span>
  </button>
  <button class="btn btn-small btn-info pull-right" style="margin-left:5px;" type="submit"><i class="icon-print" style="margin-right:5px; color: #fff;"></i><span class="save" style="color:#fff;">Export</span>
  </button>
      <div class="btn-group pull-left">
    <button class="btn"><i class="icon-chevron-left" style="color: #000;"></i></button>
    <button class="btn">1</button>
    <button class="btn">2</button>
    <button class="btn">3</button>
    <button class="btn">4</button>
    <button class="btn">5</button>
    <button class="btn"><i class="icon-chevron-right" style="color: #000;"></i></button>
  </div>
  </div><!-- End form-action -->
<!-- End table Check In -->
</div><!-- End formCheckIn -->
