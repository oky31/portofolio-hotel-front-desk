<form id="form-filter-report-checkin">
  <div class="form-actions">
    <div class="row-fluid">
      <div class="span3">
        <label>Date From</label>
        <div class="input-append date">
          <input class="span2" size="16" type="date" value="" name="dateFrom">
        </div>
      </div>
      <div class="span3">
        <label>Date To</label>
        <div class="input-append date">
          <input class="span2" size="16" type="date" value="" name="dateTo">
        </div>
      </div>
      {{-- <div class="span2">
        <label class="checkbox inline">
          <input type="checkbox" id="inlineCheckbox1" value="option1"> Revenue Include Tax
        </label>
      </div> --}}
      <label><br></label>
      <button class="btn btn-small btn-primary pull-right"
              style="margin-left:5px;"
              type="button"
              id = "btn-run-report-checkin">
              <i class="icon-book" style="margin-right:5px; color: #fff;"></i>
              <span class="save" style="color:#fff;">
                Run Report
              </span>
      </button>
    </div>
  </div>
</form>

{{-- Show table checkin --}}
<div id="show-table-checkin">

</div>

<script type="text/javascript">
$('#btn-run-report-checkin').click(function(){
  var data = $('#form-filter-report-checkin').serialize();

  $.ajax({
      url : 'report/tableCheckin',
      method : 'get',
      data : data,
      success : function(response){
        $('#show-table-checkin').html(response);
      }
  })
})
</script>
