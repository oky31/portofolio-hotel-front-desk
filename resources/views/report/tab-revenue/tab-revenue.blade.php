<div class="form-actions">
  <form id="form-filter-report-revenue">
    <div class="row-fluid">
      <div class="span2">
        <label>Search By</label>
        <select class="span2" name="searchBy">
          <option>Stay date</option>
          <option>Booked-on</option>
        </select>
      </div>
      <div class="span3">
        <label>Date From</label>
        <div class="input-append date">
          <input class="span2" size="16" type="date" value="" name="dateFrom">
        </div>
      </div>
      <div class="span3">
        <label>Date To</label>
        <div class="input-append date" >
          <input class="span2" size="16" type="date" value="" name="dateTo">
        </div>
      </div>
      {{-- <div class="span2">
        <label><br></label>
        <label class="checkbox inline">
          <input type="checkbox" id="inlineCheckbox1" value="option1"> Revenue Include Tax
        </label>
      </div> --}}

      <label><br></label>
      <button class="btn btn-small btn-primary pull-right"
              style="margin-left:5px;"
              type="button"
              id="btn-run-report-revenue">
                <i class="icon-book" style="margin-right:5px; color: #fff;"></i>
                <span class="save" style="color:#fff;">Run Report</span>
      </button>
    </form>
  </div>
</div>

{{-- Show table revenue --}}
<div id="show-table-revenue">

</div>

<script type="text/javascript">
$('#btn-run-report-revenue').click(function(){
  var data = $('#form-filter-report-revenue').serialize();

  $.ajax({
      url : 'report/tableRevenue',
      method : 'get',
      data : data,
      success : function(response){
        $('#show-table-revenue').html(response);
      }
  })
})
</script>
