<div class="form-actions">
  <table class="table table-striped">
          <thead class="tableHead" style="border-top:solid 2px #000; border-bottom:solid 2px #000;">
              <tr>
                <th>Date</th>
                <th>Time</th>
                <th>User</th>
                <th>Reservation Number</th>
                <th>Guest Name</th>
                <th>Transaction Type</th>
                <th>Payment Type</th>
                <th>Card Type</th>
                <th>Description</th>
                <th>Total</th>
              </tr>
          </thead>
            <tbody>
              <tr>
                  <td>Rp.3.000.000</td>
                  <td>Rp0</td>
                  <td>Rp0</td>
                  <td>2</td>
                  <td>5</td>
                  <td>17%</td>
                  <td>Rp.300.000</td>
                  <td>0</td>
                  <td>1</td>
                  <td>Rp.100.000</td>
              </tr>
            </tbody>
      </table>

  <button class="btn btn-small btn-inverse pull-right" style="margin-left:5px;" type="submit"><i class="icon-print" style="margin-right:5px; color: #fff;"></i><span class="save" style="color:#fff;">Print</span>
  </button>

  <button class="btn btn-small btn-info pull-right" style="margin-left:5px;" type="submit"><i class="icon-th" style="margin-right:5px; color: #fff;"></i><span class="save" style="color:#fff;">Export</span>
  </button>

      <div class="btn-group pull-left">
    <button class="btn"><i class="icon-chevron-left" style="color: #000;"></i></button>
    <button class="btn">1</button>
    <button class="btn">2</button>
    <button class="btn">3</button>
    <button class="btn">4</button>
    <button class="btn">5</button>
    <button class="btn"><i class="icon-chevron-right" style="color: #000;"></i></button>
  </div>
  </div><!-- End form-action -->
<!-- End table Payment -->
</div><!-- End formPayment -->
