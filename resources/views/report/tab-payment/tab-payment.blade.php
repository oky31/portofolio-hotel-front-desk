
  <div class="form-actions">
    <form id="form-filter-report-payment">
    <div class="row-fluid">
      <div class="span3">
        <label>Date From</label>
        <div class="input-append date">
          <input class="span2" size="16" type="date" value="" name="dateFrom">
        </div>
      </div>
      <div class="span3">
        <label>Date To</label>
        <div class="input-append date">
          <input class="span2" size="16" type="date" value="" name="dateTo">
        </div>
      </div>
      <div class="span2">
        <label>Transaction Type</label>
        <select class="span2">
          <option>Cash</option>
          <option>Credit</option>
        </select>
      </div>
      <div class="span2">
        <label>Payment Type</label>
        <select class="span2">
          <option>Master Card</option>
          <option>Link</option>
        </select>
      </div>
      <div class="span2">
        <label>Card Type</label>
        <select class="span2">
          <option>Debit Card</option>
          <option>Credit Card</option>
        </select>
      </div>
      <button class="btn btn-small btn-primary pull-right"
              style="margin-left:5px;"
              type="button"
              id="btn-run-report-payment">
                <i class="icon-book" style="margin-right:5px; color: #fff;"></i>
                <span class="save" style="color:#fff;">Run Report</span>
      </button>
    </div>
  </form>
  </div>
{{-- Show table payment --}}
<div id="show-table-payment">

</div>

<script type="text/javascript">
$('#btn-run-report-payment').click(function(){
  var data = $('#form-filter-report-payment').serialize();

  $.ajax({
      url : 'report/tablePayment',
      method : 'get',
      data : data,
      success : function(response){
        $('#show-table-payment').html(response);
      }
  })
})
</script>
