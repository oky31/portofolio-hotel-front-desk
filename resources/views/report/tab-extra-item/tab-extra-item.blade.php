<div class="form-actions">
  <form id="form-filter-report-extra-item">
    <div class="row-fluid">
      <div class="span3">
        <label>Date From</label>
        <div class="input-append date">
          <input class="span2" size="16" type="date" value="" name="dateFrom">
        </div>
      </div>
      <div class="span3">
        <label>Date To</label>
        <div class="input-append date">
          <input class="span2" size="16" type="date" value="" name="dateTo">
        </div>
      </div>
      <div class="span2">
        <label>Extra</label>
        <select class="span2">
          <option>Towel</option>
          <option>Toleteries</option>
        </select>
      </div>
      <button class="btn btn-small btn-primary pull-right"
              style="margin-left:5px;"
              type="button"
              id = "btn-run-report-extra-item">
              <i class="icon-book" style="margin-right:5px; color: #fff;"></i>
              <span class="save" style="color:#fff;">
                Run Report
              </span>
      </button>
    </div>
  </div>
</div>

{{-- Show table checkin --}}
<div id="show-table-extra-item">
</div>

<script type="text/javascript">
$('#btn-run-report-extra-item').click(function(){
  var data = $('#form-filter-report-extra-item').serialize();

  $.ajax({
      url : 'report/tableExtraItem',
      method : 'get',
      data : data,
      success : function(response){
        $('#show-table-extra-item').html(response);
      }
  })
})
