<!-- /widget-header -->
<div class="widget-content">
  <table class="table">
    {{-- <caption>tabel</caption> --}}
    <thead>
      <tr>
        <th>Status </th>
        <th>Name </th>
        <th>Invoice</th>
        <th>CheckIn </th>
        <th>CheckOut </th>
        <th>Room </th>
        <th>Total </th>
        <th>Process</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($dataReservation as $key => $reservation)
        <tr>
          <td>
            {{$reservation->status}}
          </td>
          <td>
            <a href="#" onclick="modal({{$reservation->reservation_id}})">
              {{ $reservation->first_name }}
            </a>
          </td>
          <td>
            <a href="detail/invoice/{{$reservation->reservation_id}}">
              <i class="icon-file" style="margin-right:5px;"></i>
              {{ $reservation->invoice }}
            </a>
          </td>
          <td>
            {{ $reservation->checkin_date }}
          </td>
          <td>
            {{ $reservation->checkout_date }}
          </td>
          <td>
            @for ($i=0; $i < count($roomListPerReservation[$key]); $i++)
                {{$roomListPerReservation[$key][$i]->room_number}}
            @endfor
            {{-- {{ var_dump($roomListPerReservation[$key]) }}
            {{ count($roomListPerReservation[$key]) }} --}}
            {{-- {{ $roomListPerReservation[0][0]->room_number }} --}}
            {{-- @foreach ($roomListPerReservation as  $key2=>$value)
                {{ $value[$key][$key2]->room_number }}

            @endforeach --}}
          </td>
          <td>
            {{ $reservation->total }}
          </td>
          <th>
            @if ($reservation->status == 'booked')
              <a  href="#" onclick="checkin({{$reservation->reservation_id}})"
                  class="btn btn-small btn-success" style="margin-left:5px;">
                <i class="icon-ok" style="margin-right:5px; color: #fff;"></i>
                <span class="save" style="color:#fff;">
                  Check In
                </span>
              </a>
            @endif

            @if ($reservation->status == 'checkin')
              <a  href="#" onclick="checkout({{$reservation->reservation_id}})"
                class="btn btn-small btn-primary" style="margin-left:5px;">
                <i class="icon-off" style="margin-right:5px; color: #fff;"></i>
                <span class="save" style="color:#fff;">Check Out</span>
              </a>
            @endif

          </th>
        </tr>
      @endforeach
    </tbody>
  </table>

  {{-- <div class="layoutPoover">
    ini adalah pooper
  </div> --}}
</div><!-- /widget -->

<script type="text/javascript">

</script>
