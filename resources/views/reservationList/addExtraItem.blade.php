@extends('master.master')

@section('title')
  extra Item
@endsection

@section('navbar')
  @parent
@endsection

@section('content')
  <div class="container">
      <div class="row">
        <div class="span12">
            <div class="widget">
              <!-- /widget-header -->
              <div class="widget-header"> <i class="icon-bookmark"></i>
                  <h3>Extra Items Transaction</h3>
              </div>

              <div class="widget-content">
                <div class="row-fluid">
                  <div class="span6">
                    <hr>
                    <div class="row-fluid">
                      <div class="span4">
                        <label>Type</label>
                        <input type="hidden" name="" value="{{ $idReservation }}" id="reservationId">
                        <select class="span2" id="typeExtraItem">
                          @foreach ($typesExtra as $value)
                            <option value="{{$value->extra_type_id}}"> {{ $value->extra_type }} </option>
                          @endforeach
                        </select>

                        {{-- tampil input type qty --}}
                        <label>
                          <span class="badge" style="background-color:#970067">!</span>
                          Qty
                        </label>
                        <input type="text" class="span2" placeholder="Quantity " id="qty" required>
                      </div>
                      <div class="span4">
                        {{-- tampil select type item --}}
                        <div id="showOptionExtraItem">

                        </div>

                        {{-- tampil total --}}
                          <label>Total</label>
                          <input type="text"  class="span2" placeholder="Rp.0" id="total" readonly>

                      </div>
                      <div class="span4">
                        {{-- tampil input type price --}}
                        <label>Price</label>
                        <input type="text" class="span2" id="price" readonly>

                        <label><br></label>
                        <button class="btn btn-small btn-success" id="btn-addItem"
                                style="margin-left:5px; background-color:#970067;" type="submit">
                                <i class="icon-book" style="margin-right:5px; color: #fff; "></i>
                                <span class="save" style="color:#fff;">
                                  Add Item
                                </span>
                        </button>
                      </div>
                    </div>
                  </div>

                  <div class="span6">
                    <div id="showAddExtraItem">

                    </div>
                  </div>
                </div>
                <hr>
              </div>

          </div>
        </div>
      </div>
    </div><!-- /row -->
  </div><!-- /container -->

<script type="text/javascript">
$.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
});

//  menampilkan type extra item
var globalreservationId = $('#reservationId').val();
var globalExtraTypeId = $('#typeExtraItem').val();

$.get('/reservationList/showOptionExtraItem',{
  'extraTypeId' : globalExtraTypeId
}).done(function(response){
  $('#showOptionExtraItem').html(response);
});

$('#typeExtraItem').click(function(){
    var extraTypeId = $('#typeExtraItem').val();
    console.log(extraTypeId);
    $.get('/reservationList/showOptionExtraItem',{
      'extraTypeId' : extraTypeId,
    }).done(function(response){
      $('#showOptionExtraItem').html(response);
    });
});

// menampilkan extra item yang telah di tambah
$.get('/reservationList/activeExtraItem',{
  'reservationId' : globalreservationId
}).done(function(response){
  $('#showAddExtraItem').html(response);
  // console.log(response);
});


//  penambahan extra item
$('#btn-addItem').click(function(){
  var reservationId = $('#reservationId').val();
  var extraId = $('#extraId').val();
  var qty = $('#qty').val();

  $.get('/reservationList/processAddItem',{
    'reservationId' : reservationId,
    'extraId' : extraId,
    'qty' : qty
  }).done(function(response){
    console.log(response);
    $.get('/reservationList/activeExtraItem',{
      'reservationId' : reservationId
    }).done(function(response){
      $('#showAddExtraItem').html(response);
    });
  });
})


function hapusExtraItem(id){
  $.get('/reservationList/deleteReservationExtraItem',{
    'reservationExtraId' : id
  }).done(function(response){
    $.get('/reservationList/activeExtraItem',{
      'reservationId' : globalreservationId
    }).done(function(response){
      $('#showAddExtraItem').html(response);
    });
  });
}
</script>

@endsection
