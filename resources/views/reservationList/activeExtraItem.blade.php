<div class="form-actions">
  <table class="table">
    <caption>
      <strong>Extra Item Sumary</strong><br>
    </caption>
    <tr>
      <th>type</th>
      <th>item</th>
      <th  style="text-align:center" >qty</th>
      <th style="text-align:right">price</th>
      <th style="text-align:center">time</th>
      <th></th>
    </tr>
    @foreach ($extrasItem as $value)
    <tr>
      <td>{{$value->extra_type}}</td>
      <td>{{$value->item}}</td>
      <td  style="text-align:center">{{$value->qty}}</td>
      <td style="text-align:right">Rp.{{$value->extra_bill}}</td>
      <td>
        {{$value->create_at}}
      </td>
      <td style="text-align:center">
        @if ($value->create_at == date('Y-m-d'))
        <a href="#" onclick="hapusExtraItem({{$value->reservation_extra_id}})">
          <i class="icon-remove" style="margin-right:5px; color: #fb423d; "></i>
        </a>
        @else
          <i class="icon-lock" style="margin-right:5px; color: #000000"></i>
        @endif


      </td>
    </tr>
    @endforeach
    <tr>
      <td></td>
      <td></td>
      <td><strong>Total</strong></td>
      <td style="text-align:right"><strong>Rp.{{$totalExtra}}</strong></td>
      <td></td>
      <td></td>
    </tr>
  </table>
</div>

<script type="text/javascript">

</script>
