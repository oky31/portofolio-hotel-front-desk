
  <label>Items</label>
  <select class="span2" id="extraId">
    @foreach ($items as $value)
      <option value="{{$value->extra_id}}" >{{$value->item}}</option>
    @endforeach
  </select>

<script type="text/javascript">
// menampilkan harga per item
$('#extraId').click(function(){
    var extraId = $(this).val();
    $.get('/reservationList/getPriceExtraItem',{
      'extraId' : extraId
    }).done(function(response){
        $('#price').val(response);
    });
});

var globalExtraId = $('#extraId').val();
$.get('/reservationList/getPriceExtraItem',{
  'extraId' : globalExtraId
}).done(function(response){
  $('#price').val(response);
});

//  menghitung harga total itme
$('#qty').keyup(function(){
   var price = $('#price').val();
   var summary = $(this).val();
   $('#total').val(summary * price);
})

</script>
