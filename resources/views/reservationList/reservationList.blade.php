@extends('master.master')

@section('title')
  Reservation List
@endsection

@section('navbar')
  @parent
@endsection

@section('content')
  <div class="container">
      <div class="row">
        <div class="span12">
            <div class="widget">
              <!-- /widget-header -->
              <div class="widget-content">
                <div class="row">
                  <div class="span3">
                    <label for="">Begin</label>
                    <input type="date" name="" value="{{$today}}" id="dateBegin">
                  </div>
                  <div class="span3">
                    <label for="">End</label>
                    <input type="date" name="" value="{{$tomorow}}" id="dateEnd">
                  </div>
                  <div class="span3">
                    <label for=""><br></label>
                    <select class="" name="" id="sortBy">
                      <option value="all">All</option>
                      <option value="guess-in-house">Guess-in-House</option>
                      <option value="checkin">Checkin</option>
                      <option value="booked">Booked</option>
                    </select>
                  </div>
                  <div class="span1">
                    <label for=""><br></label>
                    <button id="showListReservation" type="button" name="button"
                            class="btn" style="background-color:#970067; color:#fff">
                      serach
                    </button>
                  </div>
                </div>
              </div><!-- /widget -->
            </div>

            <div id="showList">

            </div>
        </div>

        <!-- Modal -->
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-header">
            <button type="button" class="close"
                    data-dismiss="modal" aria-hidden="true">
              ×
            </button>
            <h3 class="text-center">Options</h3>

          </div>

          <div class="modal-body" style="height:75px">
            <center>
              <input type="hidden" name="" value="" id="idReservation">
              <button class="btn btn-danger" onclick="payment()">
                Payment
              </button>
              <button class="btn btn-warning" onclick="extraItem()">
                Extra items
              </button>
              {{-- <button class="btn btn-primary" onclick="guest()">
                Guest
              </button> --}}
            <center>
          </div>
        </div>

        {{-- <div id="showList">

        </div> --}}
        {{-- <a href="#" data-toggle="tooltip" title="first tooltip">hover over me</a> --}}
      </div><!-- /row -->

  </div><!-- /container -->

<script type="text/javascript">
function modal(id){
  $('#myModal').modal('show');
  $('#idReservation').val(id);
}

function payment(){
  var reservationId = $('#idReservation').val();
  window.location = '/payment/reservationDetail/'+reservationId;
  // console.log(reservationId);

}

function extraItem(){
  var reservationId = $('#idReservation').val();
  window.location = '/reservationList/addExtraItem/'+reservationId;
  // console.log(reservationId);

}

var globalDateBegin = $('#dateBegin').val();
var globalDateEnd = $('#dateEnd').val();
var globalSortBy = $('#sortBy').val();

$.get('/reservationList/list',
    {'dateBegin' : globalDateBegin,
     'dateEnd' : globalDateEnd,
     'sortBy' : globalSortBy
     }
  ).done(function(response){
    $('#showList').html(response);
  });


$('#showListReservation').click(function(){
  var dateBegin = $('#dateBegin').val();
  var dateEnd = $('#dateEnd').val();
  var sortBy = $('#sortBy').val();

  $.get('/reservationList/list',
      {'dateBegin' : dateBegin,
       'dateEnd' : dateEnd,
       'sortBy' : sortBy
       }
    ).done(function(response){
      $('#showList').html(response);
      // console.log(response);
    });
});

function checkin(noReservatoin){
  var dateBegin = $('#dateBegin').val();
  var dateEnd = $('#dateEnd').val();
  var sortBy = $('#sortBy').val();

  $.get('/reservationList/processCheckin/'+noReservatoin
  ).done(function(response){
    if (response == 'checkin only in date checkin'){
        console.log('checkin only in date checkin')
    }

    if (response == 'not yet paid'){
        alert('not yet paid');
    }

    if (response == 'successful') {
        // reload reservation list
        $.get('/reservationList/list',
            {'dateBegin' : dateBegin,
             'dateEnd' : dateEnd,
             'sortBy' : sortBy
             }
        ).done(function(response){
            $('#showList').html(response);
        });
    }

  });
}

function checkout(noReservatoin){
  var dateBegin = $('#dateBegin').val();
  var dateEnd = $('#dateEnd').val();
  var sortBy = $('#sortBy').val();

  $.get('/reservationList/processCheckout/'+noReservatoin
  ).done(function(response){
    if (response == 'not yet paid off'){
        alert('not yet paid off');
    } else if (response == 'successful') {
        // reload reservation list
        $.get('/reservationList/list',
            {'dateBegin' : dateBegin,
             'dateEnd' : dateEnd,
             'sortBy' : sortBy
             }
        ).done(function(response){
            $('#showList').html(response);
        });
    } else {
        console.log(response);
    }
  });
}

// $('#showListReservation').click(function(){
//   var dateBegin = $('#dateBegin').val();
//   var dateEnd = $('#dateEnd').val();
//
//   $.get('/payment/showListReservation',
//     {'dateBegin' : dateBegin,
//      'dateEnd' : dateEnd}
//   ).done(function(response){
//     $('#showList').html(response);
//     // console.log(response);
//   });
// })
//
// $('#showList').load('payment/showAllListReservation');
</script>

@endsection
