<!-- Start tab Check In -->
<div class="tab-pane" id="formCheckIn">
<div class="form-actions">
  <div class="row-fluid">
    <div class="span3">
      <label>Date From</label>
      <div class="input-append date" id="dp1" data-date="12-02-2012" data-date-format="dd-mm-yyyy">
        <input class="span2" size="16" type="text" value="10-02-2018">
        <span class="add-on"><i class="icon-th"></i></span>
      </div>
    </div>
    <div class="span3">
      <label>Date To</label>
      <div class="input-append date" id="dp1" data-date="12-02-2012" data-date-format="dd-mm-yyyy">
        <input class="span2" size="16" type="text" value="12-02-2018">
        <span class="add-on"><i class="icon-th"></i></span>
      </div>
    </div>
    <div class="span2">
      <label class="checkbox inline">
        <input type="checkbox" id="inlineCheckbox1" value="option1"> Revenue Include Tax
      </label>
    </div>
    <button class="btn btn-small btn-primary pull-right" style="margin-left:5px;" type="submit"><i class="icon-book" style="margin-right:5px; color: #fff;"></i><span class="save" style="color:#fff;">Run Report</span>
    </button>
  </div>
</div>
