<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Invoice</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-responsive.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/font-awesome.css" rel="stylesheet') }}">
  <link href="/css/style.css" rel="stylesheet">

  <script src="{{ asset('js/jquery-1.7.2.min.js') }}"></script>


</head>

<body>
  <div class="page">
    <div class="row-fluid">
      <div class="span6">
        <div class="container">
          <a class="brand pull-left" href="index.html">
              <img src="/img/Footer/logofullmaincolor.png" class="img-circle" style="width: 110px; height:53px; margin-bottom:15px;">
            </a>
        </div>
        <span name="street">
          {{$company->street}}
        </span>
        <br>
        <span name="province_and_post_code">
          {{ $company->province }} {{ $company->post_code  }}
        </span>
        <br>
        <span name="country">
          {{ $company->country }}
        </span>
      </div>

      <div class="span6">
        <span id="companyPhone" style="float:right">
          Phone : {{ $company->phone }}
        </span>
        <br>
        <span id="companyEmail" style="float:right">
          E-mail : {{ $company->email }}
        </span>
      </div>

    </div>
    <hr>

    {{-- Reservation id --}}
    <input type="hidden" name="" value="{{$reservationId}}" id="reservationId">

    <div class="row-fluid">
      <div class="span6">
        <h4 id="guestDetail">Guest Detail</h4>
        <br>
        <span name="cutomer_name">{{ $customer->first_name }} {{ $customer->last_name }}</span>
        <br>
        <span name="customer_Addres">{{ $customer->street }}</span>
        <br>
        <span name="customer_postCode">{{ $customer->post_code }}</span>
        <br>
        <span name="country">{{ $customer->country }}</span>
      </div>
      <div class="span6">
        <h5 name="invoiceNumber" style="float:right">Invoice Number : {{ $reservation->invoice }}</h5>
        <br>
        <span id="invoiceDate" style="float:right">Invoice Date : {{ date('d-m-Y H:i') }}</span>
      </div>
    </div>

    <div class="row-fluid">

    </div>
    <br>

    <div class="row-fluid">
      <div class="span12">
        <h4 name="reservationDetail">Reservation Detail</h4>
        <br>
        {{-- <span id="reservationNumber pull-left">Reservation Number : LH 1289 78 90776</span> --}}
        {{-- <br> --}}
        <span name="reservationStatus">Reservation Status : {{ $reservation->status }}</span>
        <br>
        <div class="row-fluid">
          <div class="span4">
            <span id="CheckinDate">Checkin Date : {{ date('d-m-Y',strtotime($reservation->checkin_date)) }}</span>
            <br>
            <span id="CheckOutDate">Checkout Date : {{ date('d-m-Y',strtotime($reservation->checkout_date)) }}</span>
          </div>

          <div class="span2">
            <span id="guestAdult pull-left">Adult : {{ $people->adult }}</span>
            <br>
            <span id="guestChildren pull-left">Children : {{ $people->child }}</span>
          </div>
          <div class="span2">
            {{-- <span id="guestInfant pull-left">Infant : 0</span> --}}
            <br>
            <span id="guestNight pull-left">Night : {{$night->night}}</span>
          </div>

        </div>
      </div>
    </div>
    <br>
    <hr>
    <table class="table table-striped">
      <caption style = "text-align:left"><b>Bill Rooms</b></caption>
      <thead class="tableHead">
        <tr>
          <th style="width:200px">Room Type</th>
          <th style="width:200px;text-align:center" colspan="2">Room Number</th>
          <th style="width:200px;text-align:right" colspan="2">Night Rate</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($detailReservation as $key => $detail)
          <tr>
            <td>{{ $detail->type }}</td>
            <td colspan="2" style="text-align:center">{{ $detail->room_number }}</td>
            <td colspan="2" style="text-align:right">Rp. {{ $detail->night_rate }}</td>
          </tr>
        @endforeach

        <tr id="totalTable">
          <td colspan="3" style="text-align:right">
            <b>Total</b>
          </td>
          <td style="text-align:right">
            <b>Rp. {{ $totalRoomBill->total_room_bill }}</b>
          </td>
        </tr>
      </tbody>
    </table>
    <br>
    {{-- ------------------------------batas ------------------------------------- --}}
    @if (!empty($detailExtra))

    <table class="table table-striped">
      <caption style = "text-align:left"><b>Bill Extra Item</b></caption>
      <thead class="tableHead">
        <tr>
          <th style="width:200px">Type Extra</th>
          <th style="width:200px">Extra Item</th>
          <th style="width:200px">Quantity</th>
          <th style="width:200px">Price</th>
          <th style="width:200px; text-align:right">Sub Total</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($detailExtra as $key => $value)
          <tr>
            <td>{{ $value->extra_type }}</td>
            <td>{{ $value->item }}</td>
            <td>{{ $value->qty }}</td>
            <td>Rp. {{ $value->price }}</td>
            <td style="text-align:right">Rp. {{ $value->subTotal}}</td>
          </tr>
        @endforeach

        <tr id="totalTable">
          <td colspan="4" style="text-align:right">
            <b>Total</b>
          </td>
          <td style="text-align:right">
            <b>Rp. {{ $totalReservationExtra->total_extra_bill }}</b>
          </td>
        </tr>
      </tbody>
    </table>
  @endif

      {{-- ------------------------------batas ------------------------------------- --}}
      <table class="table table-striped">
        <thead class="tableHead" style="border-top:solid 2px #000; border-bottom:solid 2px #000;">
          <tr>
            <th style="width:200px; text-align:right" colspan="4">Total Bill</th>
            <th style="width:200px; text-align:right" >
              Rp. {{ $totalReservationExtra->total_extra_bill +  $totalRoomBill->total_room_bill}}
            </th>
          </tr>
          <tr>
            <th style="width:200px; text-align:right" colspan="4">Total Payment</th>
            <th style="width:200px; text-align:right" >
              Rp. <span id="totalPayment"></span>
            </th>
          </tr>
          <tr>
            <th style="width:200px; text-align:right" colspan="4">Total Outstanding</th>
            <th style="width:200px; text-align:right" >
              Rp. <span id="totalOutstanding"></span>
            </th>
          </tr>
        </thead>
      </table>

    <br>
    <div class="row-fluid">
      <div class="span12">
        <h4 id="paymentPolicy pull-left">Payment Policy :</h4>
        <br>
        <span id="updatePaymentPolicy pull-left">Please Update Payment Policy</span>
      </div>
    </div>
  </div>

<script>
var reservationId = $('#reservationId').val();
$.ajax({
  url : '/payment/getOutstanding/',
  method : 'get',
  data : {'reservationId' : reservationId},
  success : function(response){
    $('#totalOutstanding').text(response);
  }
});

$.ajax({
  url : '/payment/getTotalReceived/',
  method : 'get',
  data : {'reservationId' : reservationId},
  success : function(response){
    $('#totalPayment').text(response);
  }
})
</script>
</body>

</html>
