@foreach ($dummyReservations as $key => $dummyReservation)
<div class="row">
  <div class="span2">
    <input class="input-mini span2" type="text" value="{{ $dummyReservation->room_type }}" readonly>
  </div>
  <div class="span1">
    <input class="input-mini " type="text" value="{{ $dummyReservation->room_number }}" readonly>
  </div>
  <div class="span1">
    <input class="input-mini span1" type="text"  value="{{ $dummyReservation->adult}}" readonly>
  </div>
  <div class="span1">
    <input class="input-mini " type="text" value="{{ $dummyReservation->child }}" readonly>
  </div>
  <div class="span1">
    <a href="#" onclick="hapusDummy({{$dummyReservation->room_id}})">
      <i class="icon-remove" style="margin-right:5px; color: #fb423d; "></i>
    </a>
  </div>
</div>
@endforeach
