@extends('master.master')
@section('title')
  reservation
@endsection

@section('navbar')
  @parent
@endsection

@section('content')
<div class="row">
  <div class="span12">
    <div class="widget">
      <div class="widget-header">
        <i class="icon-book"></i>
        <h3>Reservation</h3>
      </div>

      <div class="widget-content">

            <div class="row-fluid">
              <div class="span6">
                <div class="row-fluid">
                  <div class="span6">
                    <label for="">Checkin Date</label>
                    <input class="span2" size="16" type="date" value="{{ $today }}" id="checkinDate">

                  </div>
                  <div class="span6">
                    <label for="">Checkout Date</label>
                    <input class="span2" size="16" type="date" value="{{ $tomorow  }}" id="checkoutDate">

                  </div>
                </div>
              </div>
              <div class="span6">
                <br>
                <b><span class="length-day" Style="font-size:12px;">&nbsp;&nbsp;|New Booking  </span></b>
              </div>
            </div>


        <div class="form-actions">
          <div class="row">
            <div class="span8">
              <div class="row">
                <div class="span12">
                  <span id="errorRoom" style="color:red"><span>
                  <span id="errorEmptyAdult" style="color:red"><span>
                  <span id="errorEmptyChild" style="color:red"><span>
                </div>
              </div>

              <div class="row">
                  <div class="span2">
                    <label>Room Type</label>
                    <select class="span2" id="roomType">
                        @foreach ($roomTypes as $roomType)
                          <option value="{{ $roomType->type_id }}">{{ $roomType->type }}</option>
                        @endforeach
                    </select>
                  </div>
                  {{-- tampil pilihan nomor room --}}
                  <span id="showOptionChoseNumberRoom"></span>


              </div>
              {{-- tampil dummy reservation--}}
              <div id="showDummyReservations"></div>
            </div>

          </div>
        </div>


        <div class="row-fluid">
          <div class="span8">
            <div class="tabbable">
              <br>

              <div class="tab-content">
                <!-- Start tab detail -->
                <div class="tab-pane active" id="formDetails">
                  {{-- baris 1 --}}
                  <div class="row-fluid">
                    <h4>PRIMARY CONTACT</h4>
                  </div>
                  <hr>

                  {{-- baris 1 --}}
                  <div class="row-fluid">
                    <div class="span3">
                      <label for="">IdentityId</label>
                      <input class="span2" type="text" placeholder="Identity Id" id="identityId">
                    </div>
                    <div class="span3">
                        {{-- <a href="#myModal" role="button" class="icon-search" data-toggle="modal"></a> --}}
                    </div>
                    <div class="span3">

                    </div>
                    <div class="span3">

                    </div>
                  </div>

                  {{-- baris 2 --}}
                  <div class="row-fluid">
                    <div class="span3">
                      <label>First Name</label>
                      <input class="span2" type="text" placeholder="first name" id="firstName">
                      <label>Address</label>
                      <input class="span2" type="text" placeholder="Organisation" id="organisation">
                      <input class="span2" type="text" placeholder="Street Address" id="street">
                    </div>
                    <div class="span3">
                      <label>Last Name</label>
                      <input class="span2" type="text" placeholder="last Name" id="lastName">
                      <label><br></label>
                      <input class="span2" type="text" placeholder="City" id="city">
                      <div class="row-fluid">
                        <div class="span6">
                          <input class="span1" type="text" placeholder="Province" id="province">
                        </div>
                        <div class="span6">
                          <input class="span1" type="text" placeholder="Post code" id="postcode">
                        </div>
                      </div>
                      <input class="span2" type="text" placeholder="Country" id="country">
                    </div>
                    <div class="span3">
                      <label>Email</label>
                      <input class="span2" type="email" placeholder="E-mail" id="email">
                      <label>Date of birth</label>
                      <input class="span2" type="email" placeholder="yyyy-mm-dd" id="dateOfBirth">
                      <span id="errorDateOfBirth" style="color:red; font-size:10px"></span>
                    </div>
                    <div class="span3">
                      <label>Phone Number</label>
                      <input class="span2" type="text" placeholder="Phone Number" id="phoneNumber">
                      <label>Gender</label>
                      <select class="span2" id="gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                      </select>

                    </div>
                  </div>

                  {{-- baris 3 --}}
                  <div class="row-fluid">
                    <div class="span12">
                      <label>Guess comment</label>
                      <textarea class="span8" rows="4" id="note" placeholder="Guess Comment"></textarea>
                    </div>
                  </div>

                  <hr>
                </div>

                </div><!-- End tab-content -->
              </div><!-- End tabbable -->
            </div><!-- End span8 -->

          <!-- Start booking summary -->
          <div class="span4">
            <div class="new-form">
              <h3>Booking Summary</h3>
              <br>
              <form class="form-inline">
                <span class="room-header pull-left">Room Total :</span>
                <span class="pull-right">
                  Rp. <span id="roomTotal"></span>
                </span>
              </form>

              <br>
              <hr>
              <form class="form-inline">
                <span class="room-header pull-left">Total :</span>
                <span class="pull-right">Rp.
                  <span id="total"></span>
                </span>
              </form>
              <br>

              <br>
              <hr>

              <br>
              <form class="form-inline">
                <span class="room-header pull-left">Tax Total:</span>
                <span class="pull-right">Rp.0</span>
              </form>
            </div>
          </div>
          <!-- End booking summary -->
        </div><!-- End row-fluid -->

        <div class="form-actions">
          <form class="form-inline">
            <div class="row-fluid">
              <button type="button" class="btn btn-small btn-success pull-right"
                      style="margin-left:5px;  background-color:#970067;" id="saveReservation"
                      <i class="icon-save" style="margin-right:5px; color: #fff;"></i>
                      <span class="save" style="color:#fff;">
                        Save
                      </span>
              </button>

                </div>
                </form>
              </div>
              <!-- /widget-content -->
            </div>
            <!-- /widget -->
          </div>
          <!-- /span12 -->
        </div>
        <!-- /row -->
      </div>
      <!-- /container -->

    </div>
  </div>
</div>



<script type="text/javascript">
  $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
  });

  $('#checkinDate').change(function(){
    var checkinDate = $('#checkinDate').val();
    var checkoutDate = $('#checkoutDate').val();
    var idRoomType = $('#roomType').val();
    $.get('/reservation/getRoomNumber',{
      'idRoomType' : idRoomType,
      'checkinDate' : checkinDate,
      'checkoutDate' : checkoutDate
    }).done(function(response){
      $('#showOptionChoseNumberRoom').html(response);
    });
  })

  $('#checkoutDate').change(function(){
    var checkinDate = $('#checkinDate').val();
    var checkoutDate = $('#checkoutDate').val();
    var idRoomType = $('#roomType').val();
    $.get('/reservation/getRoomNumber',{
      'idRoomType' : idRoomType,
      'checkinDate' : checkinDate,
      'checkoutDate' : checkoutDate
    }).done(function(response){
      $('#showOptionChoseNumberRoom').html(response);
    });
  })


  // get room total
  $('#roomTotal').load('/reservation/getSumRoomBillDummyReservation');
  $('#total').load('/reservation/getSumRoomBillDummyReservation');
  // show dummy reservation
  $('#showDummyReservations').load('/reservation/showDummyRoomReservation');

  //  chose room type
  $('#roomType').click(function(){
      var checkinDate = $('#checkinDate').val();
      var checkoutDate = $('#checkoutDate').val();
      var idRoomType = $(this).val();
      $.get('/reservation/getRoomNumber',{
        'idRoomType' : idRoomType,
        'checkinDate' : checkinDate,
        'checkoutDate' : checkoutDate
      }).done(function(response){
        $('#showOptionChoseNumberRoom').html(response);
      });
  });

  var globalcheckinDate = $('#checkinDate').val();
  var globalcheckoutDate = $('#checkoutDate').val();
  var globalIdRoomType = $('#roomType').val();
  $.get('/reservation/getRoomNumber',{
    'idRoomType' : globalIdRoomType,
    'checkinDate' : globalcheckinDate,
    'checkoutDate' : globalcheckoutDate
  }).done(function(response){
    // console.log(response);
    $('#showOptionChoseNumberRoom').html(response);
  });

  // Add Room type
  function addRoom(){
    var roomType = $('#roomType').val();
    var roomId = $('#roomId').val();
    var checkinDate = $('#checkinDate').val();
    var checkBreakfast = $('#breakfast').is(":checked");
    var adult = $('#adult').val();
    var child = $('#child').val();
    var checkinDate = $('#checkinDate').val();
    var checkoutDate = $('#checkoutDate').val();

    // console.log(adult+' '+child);
    $('#errorRoom').text('');

    var breakfast = '';
    if(checkBreakfast){
      breakfast = 'yes';
    }else{
      breakfast = 'no';
    }

    // kirim ke dummy reservation room
    $.get('/reservation/addDummyRoomReservation',
        {'roomType' : roomType,
         'roomId' : roomId,
         'checkinDate' : checkinDate,
         'breakfast' : breakfast,
         'adult' : adult,
         'child' : child,
         'checkinDate' : checkinDate,
         'checkoutDate' : checkoutDate
       }).done(function(response){
         console.log(response);
         if (response == 'adult empty') {
           $('#errorRoom').text('Adult can not empty');
         }

         if (response == 'child empty') {
           $('#errorRoom').text('Child can not empty');
         }

         if(response.status == 'failedChild'){
           $('#errorChild').text(response.message);
         }else{
           $('#errorChild').text('');
         }

         if(response.status == 'failedAdult'){
           $('#errorAdult').text(response.message);
         }else{
           $('#errorAdult').text('');
         }

         $('#showDummyReservations').load('/reservation/showDummyRoomReservation');
         $('#roomTotal').load('/reservation/getSumRoomBillDummyReservation');
         $('#total').load('/reservation/getSumRoomBillDummyReservation');
         // tampil dummy reservation
       });
  }


  // Save Reservation
  $('#saveReservation').click(function(){
      var checkinDate = $('#checkinDate').val();
      var checkoutDate = $('#checkoutDate').val();

      var identityId = $('#identityId').val();
      var firstName = $('#firstName').val();
      var lastName = $('#lastName').val();
      var email = $('#email').val();
      var phoneNumber = $('#phoneNumber').val();

      var organisation = $('#organisation').val();
      var street = $('#street').val();
      var city = $('#city').val();
      var province = $('#province').val();
      var postcode = $('#postcode').val();

      var country = $('#country').val();
      var gender = $('#gender').val();
      var dateOfBirth = $('#dateOfBirth').val();
      var note = $('#note').val();

      $.get('/reservation/saveReservation',
        { 'checkinDate' : checkinDate,
          'checkoutDate': checkoutDate,
          'identityId' : identityId,
          'firstName' : firstName,
          'lastName' : lastName,
          'email' : email,
          'phoneNumber' : phoneNumber,
          'organisation' : organisation,
          'street' : street,
          'city' : city,
          'province' : province,
          'postcode' : postcode,
          'country' : country,
          'gender' : gender,
          'dateOfBirth' : dateOfBirth,
          'note' : note
         }).done(function(response){
           // console.log(response);
           if (response == 'dummy kosong'){
              $('#errorRoom').text('Room can not empty.');
              // alert('room tidak boleh kosong');
           } else if (response == 'birthofdate false'){
              $('#errorDateOfBirth').html('wrong format use (yyyy-mm-dd)');
              // alert('alert format tgl salah');
           }else{
             window.location = '/reservation';
           }

         });
  });


  function hapusDummy(id){
    $.get('/reservation/deleteDummyReservation/',{'roomId' : id}
    ).done(function(response){
      $('#showDummyReservations').load('/reservation/showDummyRoomReservation');
      $('#roomTotal').load('/reservation/getSumRoomBillDummyReservation');
      $('#total').load('/reservation/getSumRoomBillDummyReservation');
      // console.log(response);
    })
  }

  $('#identityId').keyup(function(event){
    var eventKey = event.which;
    var identityId = $(this).val();

    if (eventKey == 13) {
      $.get('/reservation/serachCustomer',{'identityId' : identityId}
      ).done(function(response){
        // console.log(response);
        if(response == 'not found'){
          alert('customer not found !');
        }else{
          $('#firstName').val(response.first_name);
          $('#lastName').val(response.last_name);
          $('#email').val(response.email);
          $('#phoneNumber').val(response.phone);
          $('#organisation').val(response.organisation);
          $('#street').val(response.street);
          $('#city').val(response.city);
          $('#province').val(response.province);
          $('#postcode').val(response.post_code);
          $('#country').val(response.country);
          $('#gender').val(response.gender);
          $('#dateOfBirth').val(response.date_of_birth);
        }

      })
    }
  });
</script>
@endsection
