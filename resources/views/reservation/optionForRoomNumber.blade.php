
<div class="span1">
  <label>Number</label>
  <select class="span1" id="roomId">

    @if (empty($rooms))
        tidak ada
    @else
      @foreach ($rooms as $room)
        <option value="{{$room->room_id}}" name="{{ $room->room_number }}">
            {{$room->room_number}}
        </option>
      @endforeach

    @endif

  </select>
</div>
<div class="span1">
  <label>Adult</label>
  <input class="input-mini span1" type="text"  value="" id="adult">
  <span id="errorAdult" style="color:red;"></span>
</div>
<div class="span1">
  <label>Child</label>
  <input class="input-mini " type="text" value="" id="child">
  <span id="errorChild" style="color:red;"></span>
</div>

<div class="span1">
    <label>Breakfast</label>
    <input type="checkbox" id="breakfast" >
</div>


<div class="span2">
  <br>
  <button class="btn btn-mini pull-left"
          id="addRoom" onclick="addRoom()"
          type="button" style="background-color:#970067; color:#fff">
    <i class="icon-ok" style="margin-right:5px;">
    </i><span>Confirm</span>
  </button>
</div>
