@extends('master.masterBootstraps')

@section('navbar')
  @parent
@endsection

@section('mainContent')
  <div class="container">
    <br>
    <br>
    <form>
      <div class="form-row">
        <div class="col">
          <input type="date" class="form-control" id="inputDateBegin" value="{{ $dateNow }}">
        </div>
        <div class="col">
          <input type="date" class="form-control" id="inputDateEnd" value="{{ $oneWeekFromNow }}">
        </div>
        <div class="col">
          <button class="btn" type="button" id="btnCallListPriceRoom">
            show
          </button>
        </div>
      </div>
    </form>
    <hr>

    <div id="showTableListPriceRoom">

    </div>



  </div>

<script type="text/javascript">
  $('#btnCallListPriceRoom').click(function(){
    var inputDateBegin = $('#inputDateBegin').val();
    var inputDateEnd = $('#inputDateEnd').val();

    // console.log(inputDateEnd);
    $.ajax({
        method : 'get',
        url : '/percobaan/tableListPriceRoom',
        data : {'inputDateBegin' :  inputDateBegin,
                'inputDateEnd' :  inputDateEnd
               },
        success : function(response){
          $('#showTableListPriceRoom').html(response);
          // console.log(response);
        }
    });
  });

</script>
@endsection
