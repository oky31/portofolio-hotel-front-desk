<table class="table table-striped table-bordered">
  <thead class="thead-dark">
    <tr>
      <th>Room</th>
      @foreach ($days as $key => $day)
        <th class="text-center">
          <?php
             $data = explode("-",$day);
             echo $data[2];
          ?>
        </th>
      @endforeach

    </tr>
  </thead>
  @inject('detail', 'App\Http\Controllers\detailController')

  @foreach ($rooms  as $key1 => $value)
  <tr>
    <td>{{ $value->room_number}}</td>
    @foreach ($days as $key2 => $day)
      <td class="text-center">{{$detail->getPriceRoom($value->room_id,$day)}}</td>
    @endforeach
  </tr>
  @endforeach
</table>
