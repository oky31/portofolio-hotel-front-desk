DELIMITER //
DROP PROCEDURE IF EXISTS update_room_type//
DROP PROCEDURE IF EXISTS update_special_price//
DROP PROCEDURE IF EXISTS update_room//
DROP PROCEDURE IF EXISTS update_customer//
DROP PROCEDURE IF EXISTS update_account//
DROP PROCEDURE IF EXISTS update_reservation//


-- --------- update Account ---------------
create procedure update_room_type(
    vtype_id INT,
    vtype VARCHAR(25),
    vmax_adult INT(2),
    vmax_child INT(2),
    vdefault_price FLOAT,
    vdefault_price_breakfast FLOAT
)
begin
    update room_type
      set type = vtype,
          max_adult = vmax_adult,
          max_child = vmax_child,
          default_price = vdefault_price,
          default_price_breakfast = vdefault_price_breakfast
    where type_id = vtype_id;
end //


-- --------- update Special price ---------------
create procedure update_special_price(
  vspecial_id INT,
  vtype_id INT,
  vtime_begin TIMESTAMP,
  vtime_end TIMESTAMP,
  vdefault_price FLOAT
)
begin
    update special_price
      set type_id = vtype_id,time_begin = vtime_begin,
          time_end = vtime_end, default_price = vdefault_price
    where special_id = vspecial_id;
end //

-- --------- update Room ---------------
create procedure update_room(
  vroom_id INT,
  vtype_id INT,
  vroom_number INT(3)
)
begin
update room
  set type_id = vtype_id, room_number = vroom_number
    where room_id = vroom_id;
end //

-- --------- update customer ---------------
create procedure update_customer(
  vcustomer_id INT,
  videntity_id VARCHAR(25),
  vfirst_name VARCHAR(100),
  vlast_name VARCHAR(100),
  vemail VARCHAR(100),
  vphone VARCHAR(100),
  vstreet VARCHAR(255),
  vkelurahan VARCHAR(100),
  vcity VARCHAR(100),
  vprovince VARCHAR(100),
  vcountry VARCHAR(100)
)
begin
    update customer
      set identity_id = videntity_id,
          first_name = vfirst_name,
          last_name = vlast_name,
          email = vemail,
          phone = vphone,
          street = vstreet,
          kelurahan = vkelurahan,
          city = vcity,
          province = vprovince,
          country = vcountry
    where customer_id = vcustomer_id;
end //


-- --------- update account ---------------
create procedure update_account(
  vusername VARCHAR(25),
  vpassword VARCHAR(255),
  vlevel varchar(20)
)
begin
    update account
      set password = vpassword, level = vlevel
    where username = vusername;
end //

-- --------- update reservation ---------------
create procedure update_reservation(
  vreservation_id INT,
  vcustomer_id INT,
  vusername VARCHAR(10),
  vcheckin_time TIMESTAMP,
  vcheckout_time TIMESTAMP
)
begin
    update reservation
      set checkin_time = vcheckin_time, checkout_time = vcheckout_time
    where reservation_id = vreservation_id AND
          customer_id = vcustomer_id AND
          username = vusername;
end //


-- ------------ update payment --------------------
create procedure update_payment(
  vpayment_id INT,
  vaccount_id INT,
  vtransaction_type ENUM('payment','deposit'),
  vpayment_type VARCHAR(20),
  vtotal DOUBLE,
  vacceptance DOUBLE,
  vdescription VARCHAR(255)
)
begin
	insert into customer_payment(payment_id,
                               account_id,
                               transaction_type,
                               payment_type,
                               total,
                               acceptance,
                               description)
         values (vpayment_id,
                 vaccount_id,
                 vtransaction_type,
                 vpayment_type,
                 vtotal,
                 vacceptance,
                 vdescription);
end //

--
-- -- --------- update reservation room ---------------
-- create procedure update_reservation_room(
--   vreservation_id INT,
--   vroom_id INT,
--   vroom_bill DOUBLE,
--   vroom_type VARCHAR(25),
--   vnote VARCHAR(255)
-- )
-- begin
--     update reservation_room
--       set room_id = vroom_id, room_bill = vroom_bill,
--           room_type = vroom_type, note = vnote
--     where reservation_id = vreservation_id AND
--           room_id = vroom_id;
-- end //
delimiter ;
