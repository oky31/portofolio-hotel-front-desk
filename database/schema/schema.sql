DROP DATABASE IF EXISTS elite_hotel;
CREATE DATABASE IF NOT EXISTS elite_hotel;

use elite_hotel;

DROP TABLE IF EXISTS customer,
                     account,
                     room_type,
                     special_price,
                     room,
                     reservation,
                     payment,
                     reservation_room,
                     type_extra_item,
                     extra_item,
                     reservaton_extra,
                     company_profile;

CREATE TABLE customer (
    customer_id INT AUTO_INCREMENT NOT NULL,
    identity_id VARCHAR(25) NOT NULL,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100),
    gender ENUM('male','female'),
    email VARCHAR(100),
    phone VARCHAR(100),
    date_of_birth DATE,
    organisation VARCHAR(100),
    street VARCHAR(255),
    city VARCHAR(100),
    province VARCHAR(100),
    post_code VARCHAR(30),
    country VARCHAR(100),
    create_at TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY (customer_id)
);

CREATE TABLE account (
    account_id INT AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(255) NOT NULL,
    level VARCHAR(20) NOT NULL,
    status ENUM('aktif','dihapus') DEFAULT 'aktif',
    create_at TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY (account_id),
    CONSTRAINT uniq_account UNIQUE (username)
);

CREATE TABLE room_type (
    type_id INT AUTO_INCREMENT NOT NULL,
    type VARCHAR(25) NOT NULL,
    max_adult INT(2) NOT NULL,
    max_child INT(2) NOT NULL,
    default_price FLOAT NOT NULL,
    default_price_breakfast FLOAT,
    status ENUM('aktif','dihapus') DEFAULT 'aktif',
    create_at TIMESTAMP DEFAULT NOW(),
    PRIMARY KEY (type_id)
);

CREATE TABLE special_price (
    special_id INT AUTO_INCREMENT NOT NULL,
    type_id INT,
    time_begin DATETIME NOT NULL,
    time_end DATETIME NOT NULL,
    default_price FLOAT NOT NULL,
    status ENUM('aktif','dihapus') DEFAULT 'aktif',
    create_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (type_id) REFERENCES room_type(type_id),
    PRIMARY KEY (special_id)
);


CREATE TABLE room (
    room_id INT AUTO_INCREMENT NOT NULL,
    type_id INT NOT NULL,
    room_number VARCHAR(25) NOT NULL,
    status ENUM('dirty','cleanup','clean','renovation') DEFAULT 'clean',
    status_insystem ENUM('aktif','dihapus') DEFAULT 'aktif',
    create_at TIMESTAMP DEFAULT NOW(),
    PRIMARY KEY (room_id),
    FOREIGN KEY (type_id) REFERENCES room_type(type_id)
);

CREATE TABLE reservation (
    reservation_id INT AUTO_INCREMENT NOT NULL,
    invoice VARCHAR(20),
    customer_id INT NOT NULL,
    account_id INT NOT NULL,
    checkin_date DATE NOT NULL,
    checkout_date DATE NOT NULL,
    checkin_time TIME,
    checkout_time TIME,
	  note VARCHAR(255),
    status ENUM('checkin','booked','checkout'),
    reservation_time TIMESTAMP DEFAULT NOW(),
    PRIMARY KEY (reservation_id),
    UNIQUE (invoice),
    FOREIGN KEY (customer_id) REFERENCES customer(customer_id),
    FOREIGN KEY (account_id) REFERENCES account(account_id)
);


CREATE TABLE payment (
	  payment_id INT AUTO_INCREMENT,
    reservation_id INT NOT NULL,
    card_id VARCHAR(25),
    card_name VARCHAR(100),
    card_exp DATE,
    payment_time TIMESTAMP DEFAULT NOW(),
    PRIMARY KEY (payment_id),
    FOREIGN KEY (reservation_id) REFERENCES reservation(reservation_id)
);

CREATE TABLE reservation_room (
    reservation_id INT NOT NULL,
    room_id INT NOT NULL,
    room_bill DOUBLE,
    default_price_breakfast FLOAT,
    room_type VARCHAR(25),
    room_number VARCHAR(25),
    adult INT(2),
    child INT(2),
    note VARCHAR(255) NULL,
    PRIMARY KEY (reservation_id,room_id),
    FOREIGN KEY (reservation_id) REFERENCES reservation(reservation_id),
    FOREIGN KEY (room_id) REFERENCES room(room_id)
);

CREATE TABLE dummy_reservation_room (
    room_id INT NOT NULL,
    room_bill DOUBLE,
    default_price_breakfast FLOAT,
    room_type VARCHAR(25),
    room_number VARCHAR(25),
    adult INT(2),
    child INT(2),
    checkin_date DATE,
    checkout_date DATE,
    PRIMARY KEY (room_id,room_type)
);

CREATE TABLE customer_payment(
	customer_payment_id INT AUTO_INCREMENT,
  account_id INT,
  transaction_type ENUM('payment','deposit'),
  payment_type VARCHAR(20),
	payment_id INT NOT NULL,
	total DOUBLE,
	acceptance DOUBLE,
	description VARCHAR(255),
  create_at TIMESTAMP DEFAULT NOW(),
	PRIMARY KEY(customer_payment_id),
	FOREIGN KEY(payment_id) REFERENCES payment(payment_id),
  FOREIGN KEY(account_id) REFERENCES account(account_id)
);

CREATE TABLE type_extra_item(
  extra_type_id INT AUTO_INCREMENT,
	extra_type VARCHAR(100),
  PRIMARY KEY (extra_type_id)
);

CREATE TABLE extra_item(
	extra_id INT AUTO_INCREMENT,
  item VARCHAR(100) NOT NULL,
  extra_type_id INT NOT NULL,
  price FLOAT NOT NULL,
  create_at TIMESTAMP DEFAULT NOW(),
	PRIMARY KEY(extra_id),
  FOREIGN KEY(extra_type_id) REFERENCES type_extra_item(extra_type_id)
);

CREATE TABLE reservation_extra(
  reservation_extra_id INT AUTO_INCREMENT,
  reservation_id INT,
  account_id INT,
	extra_id INT,
  item VARCHAR(100),
  qty INT,
  extra_bill FLOAT,
  create_at TIMESTAMP DEFAULT NOW(),
	PRIMARY KEY(reservation_extra_id),
  FOREIGN KEY(extra_id) REFERENCES extra_item(extra_id),
  FOREIGN KEY(reservation_id) REFERENCES reservation(reservation_id),
  FOREIGN KEY(account_id) REFERENCES account(account_id)
);

CREATE TABLE company_profile(
  company_id INT AUTO_INCREMENT,
  company_name VARCHAR(100) NOT NULL,
  street VARCHAR(1000),
  city VARCHAR(100),
  province VARCHAR(100),
  country VARCHAR(100),
  post_code VARCHAR(100),
  fax VARCHAR(100),
  phone VARCHAR(20),
  email VARCHAR(100),
  web VARCHAR(100),
  url_logo VARCHAR(255),
  PRIMARY KEY(company_id)
);

-- source dummy_data.dump;
source data_asli.sql;
source store_procedure_insert.sql;
source store_procedure_update.sql;
source store_procedure_delete.sql;
source store_function.sql;
