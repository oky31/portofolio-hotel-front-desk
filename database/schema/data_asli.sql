INSERT INTO room_type(type,max_adult,max_child,default_price,default_price_breakfast)
VALUES ('Super Delux',2,2,760000,0),
       ('Delux',2,2,640000,0),
       ('Superior',2,2,440000,0),
       ('Standard',2,2,336000,0);

INSERT INTO room(type_id,room_number,status)
VALUES
        -- room super delux
        (1,218,'dirty'),
        (1,236,'dirty'),
        -- room delux
        (2,201,'dirty'),
        (2,202,'dirty'),
        (2,203,'dirty'),
        (2,213,'dirty'),
        (2,214,'dirty'),
        (2,215,'dirty'),
        (2,216,'dirty'),
        (2,217,'dirty'),
        (2,231,'dirty'),
        (2,232,'dirty'),
        (2,233,'dirty'),
        (2,234,'dirty'),
        (2,235,'dirty'),
        -- room superior
        (3,204,'dirty'),
        (3,205,'dirty'),
        (3,206,'dirty'),
        (3,207,'dirty'),
        (3,208,'dirty'),
        (3,209,'dirty'),
        (3,210,'dirty'),
        (3,219,'dirty'),
        (3,220,'dirty'),
        (3,221,'dirty'),
        (3,222,'dirty'),
        (3,223,'dirty'),
        (3,224,'dirty'),
        (3,225,'dirty'),
        (3,226,'dirty'),
        (3,227,'dirty'),
        (3,228,'dirty'),
        (3,229,'dirty'),
        (3,230,'dirty'),
        -- room standart
        (4,211,'dirty'),
        (4,212,'dirty');

INSERT INTO `special_price` (`type_id`,`time_begin`,`time_end`,`default_price`,`status`) VALUES (1,'2018-05-04 00:00:00','2018-05-11 00:00:00',380000,'aktif');
INSERT INTO `special_price` (`type_id`,`time_begin`,`time_end`,`default_price`,`status`) VALUES (2,'2018-05-04 00:00:00','2018-05-11 00:00:00',320000,'aktif');
INSERT INTO `special_price` (`type_id`,`time_begin`,`time_end`,`default_price`,`status`) VALUES (3,'2018-05-04 00:00:00','2018-05-11 00:00:00',220000,'aktif');
INSERT INTO `special_price` (`type_id`,`time_begin`,`time_end`,`default_price`,`status`) VALUES (4,'2018-05-04 00:00:00','2018-05-11 00:00:00',168000,'aktif');


-- INSERT INTO special_price(type_id,time_begin,time_end,default_price)
-- VALUES (1,'2018-04-04','2018-04-11',380000),
--        (2,'2018-04-04','2018-04-11',320000),
--        (3,'2018-04-04','2018-04-11',220000),
--        (4,'2018-04-04','2018-04-11',168000);

INSERT INTO account(username,password,level)
VALUES ('resepsionis','12345','1');
