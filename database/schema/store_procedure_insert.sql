use elite_hotel;
DELIMITER //

DROP PROCEDURE IF EXISTS insert_customer//
DROP PROCEDURE IF EXISTS insert_account//
DROP PROCEDURE IF EXISTS insert_room_type//
DROP PROCEDURE IF EXISTS insert_special_price//
DROP PROCEDURE IF EXISTS insert_room//
DROP PROCEDURE IF EXISTS insert_reservation//
DROP PROCEDURE IF EXISTS insert_reservation_room//
DROP PROCEDURE IF EXISTS insert_payment//
DROP PROCEDURE IF EXISTS customer_payment//
DROP PROCEDURE IF EXISTS checkin//
DROP PROCEDURE IF EXISTS checkout//
DROP PROCEDURE IF EXISTS dirty//
DROP PROCEDURE IF EXISTS cleanup//
DROP PROCEDURE IF EXISTS renovation//
DROP PROCEDURE IF EXISTS available//
DROP PROCEDURE IF EXISTS insert_extra_item//

create procedure insert_customer(
    videntity_id VARCHAR(25),
    vfirst_name VARCHAR(100),
    vlast_name VARCHAR(100),
    vgender ENUM('male','female'),
    vemail VARCHAR(100),
    vphone VARCHAR(100),
    vdate_of_birth DATE,
    vorganisation VARCHAR(100),
    vstreet VARCHAR(255),
    vcity VARCHAR(100),
    vprovince VARCHAR(100),
    vpost_code VARCHAR(30),
    vcountry VARCHAR(100)
)
begin
    insert into customer(identity_id,
                         first_name,
                         last_name,
                         gender,
                         email,
                         phone,
                         date_of_birth,
                         organisation,
                         street,
                         city,
                         province,
                         post_code,
                         country)
           values(videntity_id,
                  vfirst_name,
                  vlast_name,
                  vgender,
                  vemail,
                  vphone,
                  vdate_of_birth,
                  vorganisation,
                  vstreet,
                  vcity,
                  vprovince,
                  vpost_code,
                  vcountry);
end //

-- --------- Tambah Account ---------------
create procedure insert_account(
  vusername VARCHAR(25),
  vpassword VARCHAR(255),
  vlevel VARCHAR(20)
)
begin
    insert into account(username,password,level)
           values(vusername,vpassword,vlevel);
end //


-- --------- Tambah room type ---------------
create procedure insert_room_type(
  vtype VARCHAR(25),
  vmax_adult INT(2),
  vmax_child INT(2),
  vdefault_price FLOAT,
  vdefault_price_breakfast FLOAT
)
begin
    insert into room_type(type,max_adult,max_child,default_price,default_price_breakfast)
           values(vtype,vmax_adult,vmax_child,vdefault_price,vdefault_price_breakfast);
end //

-- --------- Tambah special price ---------------
create procedure insert_special_price(
  vtype_id INT,
  vtime_begin DATETIME,
  vtime_end DATETIME,
  vdefault_price FLOAT
)
begin
    insert into special_price(type_id,
                              time_begin,
                              time_end,
                              default_price)
           values(vtype_id,
                  vtime_begin,
                  vtime_end,
                  vdefault_price);
end //

-- --------- Tambah room ---------------
create procedure insert_room(
  vtype_id INT,
  vroom_number INT(3)
)
begin
    insert into room(type_id,room_number)
           values(vtype_id,vroom_number);
end //

-- --------- Tambah reservation ---------------
create procedure insert_reservation(
  vcustomer_id INT,
  vaccount_id INT,
  vcheckin_date DATE,
  vcheckout_date DATE,
  vnote VARCHAR(255)
)
begin
    declare vstatus varchar(25);
    declare vlastId INT;
    declare vinvoice VARCHAR(255);
    declare vcheckin_time TIME;
    declare vcheckout_time TIME;

    start transaction;

    set vstatus = 'booked';
    set vcheckin_time = '12:00:00';
    set vcheckout_time = '12:00:00';

    insert into reservation(customer_id,
                            account_id,
                            checkin_date,
                            checkout_date,
                            checkin_time,
                            checkout_time,
                            status,
                            note)
           values(vcustomer_id,
                  vaccount_id,
                  vcheckin_date,
                  vcheckout_date,
                  vcheckin_time,
                  vcheckout_time,
                  vstatus,
                  vnote);

    -- select id terakhir
    select reservation_id into vlastId
    from reservation
    order by reservation_id DESC LIMIT 1;

    -- get Invoice from last insert id
    select concat(replace(date_format(reservation_time, '%Y-%m-%d'),'-',''),'-', reservation_id)
    into vinvoice
    from reservation
    where reservation_id = vlastId;

    -- update Invoice
    update reservation
      set invoice = vinvoice
    where reservation_id = vlastId;

    commit;

end //

-- --------- Tambah reservation room ---------------
create procedure insert_reservation_room(
  vreservation_id INT,
  vroom_id INT,
  vroom_bill DOUBLE,
  vprice_breakfast FLOAT,
  vroom_type VARCHAR(25),
  vroom_number CHAR(3),
  vadult INT(2),
  vchild INT(2)
)
begin
    -- tambah ke tabel room reservation
    insert into reservation_room(reservation_id,
                                 room_id,
                                 room_bill,
                                 default_price_breakfast,
                                 room_type,
                                 room_number,
                                 adult,
                                 child)
           values(vreservation_id,
                  vroom_id,
                  vroom_bill,
                  vprice_breakfast,
                  vroom_type,
                  vroom_number,
                  vadult,
                  vchild);

end //

-- --------- Tambah payment  ---------------
create procedure insert_payment(
  vreservation_id INT,
  vtransaction_type ENUM('payment','deposit'),
  vaccount_id INT,
  vpayment_type VARCHAR(20),
  vcard_id VARCHAR(25),
  vcard_name VARCHAR(100),
  vcard_exp DATE,
  vtotal DOUBLE,
	vacceptance DOUBLE,
	vdescription VARCHAR(255)
)
begin
    declare vpayment_id INT;

    start transaction;

    -- tambah dulu tabel payment
    insert into payment(reservation_id,
                        card_id,
                        card_name,
                        card_exp)
           values(vreservation_id,
                  vcard_id,
                  vcard_name,
                  vcard_exp);

    select payment_id into vpayment_id
    from payment  order by payment_time DESC LIMIT 1;

    -- tambah tabel customer payment
    insert into customer_payment(payment_id,
                                 account_id,
                                 transaction_type,
                                 payment_type,
                                 total,
                                 acceptance,
                                 description)
           values(vpayment_id,
                  vaccount_id,
                  vtransaction_type,
                  vpayment_type,
                  vtotal,
                  vacceptance,
                  vdescription);

    commit;
end //



-- --------- insert dummy ---------------
create procedure insert_dummy_reservation(
  vroom_id INT,
  vbreakfast ENUM('yes','no'),
  vcheckin_date DATE,
  vcheckout_date DATE,
  vadult INT(2),
  vchild INT(2)
)

begin
    declare vroom_price float;
    declare vroom_price_breakfast float;
    declare vroom_type varchar(25);
    declare vroom_number varchar(25);
    declare checkSpecialPrice int;
    declare specialPrice double;

    start transaction;

    select type,
           default_price,
           room_number
    into vroom_type,
         vroom_price,
         vroom_number
    from room join room_type using(type_id)
    where room_id = vroom_id;

    if (vbreakfast = 'yes') then
        select default_price_breakfast
        into vroom_price_breakfast
        from room join room_type using(type_id)
        where room_id = vroom_id;
    else
        set vroom_price_breakfast = 0;
    end if;

    -- pengecekan special price
    select  room_id,
        		special_price.default_price
    into checkSpecialPrice,
         specialPrice
    from room join room_type using(type_id)
              join special_price using(type_id)
    where (date_format(time_begin,'%Y-%m-%d') <= vcheckin_date
           and date_format(time_end,'%Y-%m-%d') >= vcheckin_date)
    and room_id = vroom_id
    order by special_price.special_id DESC
    limit 1;

    if (checkSpecialPrice is not null) then
        set vroom_price = specialPrice;
    end if;


    insert into dummy_reservation_room(room_id,
                                       room_bill,
                                       default_price_breakfast,
                                       room_type,
                                       room_number,
                                       adult,
                                       child,
                                       checkin_date,
                                       checkout_date)
      values(vroom_id,
             vroom_price,
             vroom_price_breakfast,
             vroom_type,
             vroom_number,
             vadult,
             vchild,
             vcheckin_date,
             vcheckout_date);

    commit;
end //

-- --------- insert extra item -----------------
create procedure insert_extra_item(
  vitem VARCHAR(100),
  vextra_type_id INT,
  vprice FLOAT
)
begin
  insert into extra_item(item,extra_type_id,price)
         values(vitem,vextra_type_id,vprice);
end //


-- --------- insert reservation_extra -----------------
create procedure insert_reservation_extra(
  vreservation_id INT,
  vaccount_id INT,
	vextra_id INT,
  vqty INT
)
begin
  declare vitem varchar(100);
  declare vprice FLOAT;

  -- di dapat dari hasil qty * price di tabel extra item
  declare vextra_bill DOUBLE;

  start transaction;

  select item,price into vitem,vprice
  from extra_item
  where extra_id = vextra_id;

  set vextra_bill = vprice * vqty;

  insert into reservation_extra(reservation_id,
                                account_id,
                                extra_id,
                                item,
                                qty,
                                extra_bill)
         values(vreservation_id,
                vaccount_id,
                vextra_id,
                vitem,
                vqty,
                vextra_bill);

  commit;
end //


-- --------- insert type extra item -----------------
create procedure insert_type_extra_item( vextra_type VARCHAR(100) )
begin
  insert into type_extra_item(extra_type)
         values(vextra_type);
end //

-- --------- checkin ---------------
create procedure checkin(vreservation_id INT)
begin
  update reservation
  set status = 'checkin'
  where reservation_id = vreservation_id;
end //

-- create procedure dirty(vreservation_id INT)
-- begin
--     update reservation
--     set status = 'checkin'
--     where reservation_id = vreservation_id;
-- end //

create procedure checkout(vreservation_id INT)
begin
  update reservation
  set status = 'checkout'
  where reservation_id = vreservation_id;
end //

-- create procedure renovation(vroom_id INT)
-- begin
--     update room
--     set status = 'renovation'
--     where room_id = vroom_id;
-- end //


-- create procedure available(vroom_id INT)
-- begin
--     update room
--     set status = 'available'
--     where room_id = vroom_id;
-- end //

delimiter ;
