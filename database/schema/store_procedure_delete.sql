DELIMITER //
DROP PROCEDURE IF EXISTS delete_reservation_room//


-- --------- delete reservation room ---------------
create procedure delete_reservation_room(
    vreservation_id INT,
    vroom_id INT
)
begin
    delete from reservation_room
    where reservation_id = vreservation_id
          and
          room_id = vroom_id;
end //

-- --------- delete room ---------------
create procedure delete_room(vroom_id INT)
begin
    update room
      set status_insystem = 'dihapus'
    where room_id = vroom_id;
end //

-- --------- delete room_type ---------------
create procedure delete_room_type(vtype_id INT)
begin
    update room_type
      set status = 'dihapus'
    where type_id = vtype_id;
end //

-- --------- delete room_type ---------------
create procedure delete_special_price(vspecial_id INT)
begin
    update special_price
      set status = 'dihapus'
    where special_id = vspecial_id;
end //


-- --------- delete reservation ---------------
create procedure delete_reservation(vreservation_id INT)
begin
      delete from reservation
      where reservation_id = vreservation_id;
end //

-- --------- delete account ---------------
create procedure delete_account(vusername varchar(10))
begin
    update account
      set status = 'dihapus'
    where username = vusername;
end //

DELIMITER ;
