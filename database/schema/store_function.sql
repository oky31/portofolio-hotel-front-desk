DELIMITER //

DROP FUNCTION IF EXISTS getTotalBill//
DROP FUNCTION IF EXISTS getTotalCustomerPayment//
DROP FUNCTION IF EXISTS getPriceRoom//


CREATE FUNCTION getTotalBill ( vreservation_id INT )
RETURNS DOUBLE

BEGIN

   DECLARE tot_room_bill DOUBLE;
   DECLARE tot_extra_bill DOUBLE;
   DECLARE total_bill DOUBLE;

   SELECT sum(room_bill + default_price_breakfast) * (checkout_date - checkin_date)
   INTO tot_room_bill
   FROM reservation JOIN reservation_room ON reservation.reservation_id = reservation_room.reservation_id
   WHERE reservation.reservation_id = vreservation_id
   GROUP BY checkout_date,checkin_date;

   SELECT if(sum(extra_bill) is null,0,sum(extra_bill)) INTO tot_extra_bill
   FROM reservation JOIN reservation_extra ON reservation.reservation_id = reservation_extra.reservation_id
   WHERE reservation.reservation_id = vreservation_id;

   SET total_bill =  tot_room_bill + tot_extra_bill;

   RETURN total_bill;

END //


CREATE FUNCTION getTotalCustomerPayment ( vreservation_id INT )
RETURNS DOUBLE

BEGIN

   DECLARE total_received DOUBLE;

   SELECT if(sum(total) is null,0,sum(total))
   INTO total_received
   FROM customer_payment JOIN payment USING(payment_id)
   WHERE reservation_id = vreservation_id;

   RETURN total_received;

END //

-- --------- Get Price Room ---------------
CREATE FUNCTION getPriceRoom(vroom_id INT, vdate DATE)
RETURNS DOUBLE

BEGIN
  declare checkSpecialPrice int;
  declare specialPrice double;
  declare vroom_price double;

  -- pengecekan special price
  select  room_id,
          special_price.default_price
  into checkSpecialPrice,
       specialPrice
  from room join room_type using(type_id)
            join special_price using(type_id)
  where (date_format(time_begin,'%Y-%m-%d') <= date_format(vdate,'%Y-%m-%d')
         and date_format(time_end,'%Y-%m-%d') >= date_format(vdate,'%Y-%m-%d'))
  and room_id = vroom_id
  order by special_price.special_id DESC
  limit 1;

  if (checkSpecialPrice is not null) then
      set vroom_price = specialPrice;
  else
      select  default_price
      into vroom_price
      from room join room_type using(type_id)
      where room_id = vroom_id;
  end if;

  return vroom_price;
END //


DELIMITER ;
