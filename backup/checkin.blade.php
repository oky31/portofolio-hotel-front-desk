@extends('master.master')
@section('title')
  Checkin Pages
@endsection

@section('navbar')
  @parent
@endsection

@section('content')
<div class="row">
  <div class="span12">
    <div class="widget">
      <div class="widget-header">
        <i class="icon-book"></i>
        <h3>Checkin / Checkout </h3>
      </div>

      <div class="widget-content">
        <div class="form-actions">
          <div class="row-fluid">
            <div class="span2">
              <button class="btn btn-inverse"><a href="#"><i class="icon-chevron-left" style="margin-right:5px; color:#fff;"></i><span class="button-back" style="color:#fff;">Back</span> </a> </button>
            </div>
            <div class="span8">
              <h3>Booking Report | Booking Status : New Booking</h3>
            </div>
          </div>
        </div>

        <div class="form-actions">
          <form class="form-inline">
            <div class="input-append date" id="checkinTime" data-date="{{ date('d-m-Y')}}" data-date-format="dd-mm-yyyy">
              <input class="span2" size="16" type="text" value="{{ date('Y-m-d',strtotime($reservationDate->checkin_time)) }}">
              <span class="add-on"><i class="icon-th"></i></span>
            </div>
            <div class="input-append date" id="checkoutTime" data-date="{{ date('d-m-Y')}}" data-date-format="dd-mm-yyyy">
              <input class="span2" size="16" type="text" value="{{ date('Y-m-d',strtotime($reservationDate->checkout_time))  }}">
              <span class="add-on"><i class="icon-th"></i></span>
            </div>
            <b><span class="length-day" Style="font-size:12px;">&nbsp;&nbsp;| Length of Day : </span></b>
            <b><span class="summary-night" Style="font-size:20px;">3 Night</span></b>
          </form>
        </div>

        <form class="form-inline">
          <button class="btn btn-small btn-success pull-right" style="margin-left:5px;" type="submit">
            <i class="icon-ok" style="margin-right:5px; color: #fff;"></i>
            <span class="save" style="color:#fff;">
              Check In
            </span>
          </button>
          {{-- <button class="btn btn-small btn-inverse pull-right" style="margin-left:5px;" type="submit">
            <i class="icon-remove" style="margin-right:5px; color: #fff;"></i>
            <span class="save" style="color:#fff;">
              Cancel Reservation
            </span>
          </button> --}}
          {{-- <button class="btn btn-small btn-primary pull-right" style="margin-left:5px;" type="submit">
            <i class="icon-off" style="margin-right:5px; color: #fff;">
            </i><span class="save" style="color:#fff;">
              Check Out
            </span>
          </button> --}}
          <button class="btn btn-small pull-right" id="addRoom" type="button">
            <i class="icon-plus" style="margin-right:5px;">
            </i><span>Add Room</span>
          </button>
          <br>
        </form>

        <div class="form-actions">
          <div class="row-fluid">
            <div class="span11">
              <div class="row">
                <div class="span2">
                  <label>Room Type</label>
                  <select class="span2" id="roomType">
                      {{-- @foreach ($roomTypes as $roomType)
                        <option value="{{ $roomType->type_id }}">{{ $roomType->type }}</option>
                      @endforeach --}}
                  </select>
                </div>
                {{-- tampil pilihan nomor room --}}
                <span id="showOptionChoseNumberRoom"></span>
              </div>
              {{-- tampil dummy reservation--}}

              <input type="hidden" name="" value="{{$reservationId}}" id="reservationId">
              <div id="showRoomReservations"></div>
            </div>


            {{-- <div class="span3">
              <form class="form-inline">
                <span class="room-header pull-left">Room :</span>
                <input class="input-mini pull-right" type="text" placeholder="0">
              </form>
              <br>
              <form class="form-inline">
                <span class="room-header pull-left">Extra Person :</span>
                <span class="pull-right">Rp.0</span>
              </form>
              <br>
              <form class="form-inline">
                <span class="room-header pull-left">Discount :</span>
                <input class="input-mini pull-right" type="text" placeholder="0">
                <input class="input-mini pull-right" style="margin-right: 5px;" type="text" placeholder="0">
              </form>
            </div> --}}

          </div>
        </div>


        <div class="row-fluid">
          <div class="span8">
            <div class="tabbable">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#formDetails" data-toggle="tab">Detail</a>
                </li>
                <li>
                  <a href="#formPayments" data-toggle="tab">Payments</a>
                </li>
              </ul>
              <br>

              <div class="tab-content">
                <!-- Start tab detail -->
                <div class="tab-pane active" id="formDetails">
                  <div class="row-fluid">
                    <div class="span3">
                      <h3>Guest Detail</h3>
                      <!-- Start modals -->
                      <div class="controls">
                        <!-- Button to trigger modal -->
                        <a href="#myModal" role="button" class="icon-search" data-toggle="modal"></a>
                      </div>
                      <!-- /controls -->
                      <br>
                      <input class="span2" type="text" placeholder="Identity Id" id="identityId">
                      <input class="span2" type="text" placeholder="first name" id="firstName">
                      <input class="span2" type="text" placeholder="last Name" id="lastName">
                      <input class="span2" type="text" placeholder="E-mail" id="email">
                      <input class="span2" type="text" placeholder="Phone Number" id="phoneNumber">
                    </div>
                    <div class="span3">
                      <h3>Address</h3>
                      <input class="span2" type="text" placeholder="Street Address" id="street">
                      <input class="span2" type="text" placeholder="kelurahan" id="kelurahan">
                      <input class="span2" type="text" placeholder="City" id="city">
                      <input class="span2" type="text" placeholder="Province" id="province">
                      <input class="span2" type="text" placeholder="Country" id="country">

                    </div>
                    <div class="span3">
                      {{-- <h3>Payment Detail</h3>
                      <select class="span2">
                        <option>Visa</option>
                        <option>PayPal</option>
                        <option>Master Card</option>
                        <option>Western Union</option>
                        <option>Bank Transfer</option>
                      </select>
                      <input class="span2" type="text" placeholder="Number Card">
                      <input class="span2" type="text" placeholder="Account Name"> --}}
                      {{-- <select class="span2">
                        <option>January</option>
                      </select> --}}
                      {{-- <select class="span2">
                        <option>2018</option>
                      </select> --}}
                    </div>
                    <div class="span3">
                      <h3>Other Detail</h3>
                      {{-- <select class="span2">
                        <option>01:00 PM</option>
                      </select> --}}
                      <textarea rows="9" id="note"></textarea>
                      {{-- <select class="span2">
                        <option>Referral</option>
                        <option>Residence</option>
                       </select> --}}
                    </div>
                  </div>
                </div>
                <!-- End tab details -->
                <!-- Start tab payments-->
                <div class="tab-pane" id="formPayments">
                  <div class="form-actions">
                    <button class="btn btn-primary btn-small pull-left"><a href="#"><i class="icon-plus" style="margin-right:5px; color:#fff;"></i><span style="color:#fff;">Add Payments</span></a> </button>
                  </div>
                  <div class="form-actions">
                    <table class="table">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Transaction Type</th>
                          <th>Description</th>
                          <th>Payment Type</th>
                          <th>Amount</th>
                          <th>Surcharge</th>
                          <th>Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><a href="#"><i class="icon-remove"></i></td>
                          <td>
                            <select class="span2">
                              <option>Transfer</option>
                              <option>Deposite</option>
                              <option>Credit</option>
                              <option>Debit</option>
                            </select>
                          </td>
                          <td>
                            <input class="span2" type="text" placeholder="Description">
                          </td>
                          <td>
                            <select class="span2">
                              <option>Visa</option>
                              <option>PayPal</option>
                              <option>Master Card</option>
                              <option>Western Union</option>
                              <option>Bank Transfer</option>
                            </select>
                          </td>
                          <td>
                            <input class="span1" type="text" placeholder="Amount">
                          </td>
                          <td>
                            <input class="span1" type="text" placeholder="Surcharge">
                          </td>
                          <td>
                            <input class="span1" type="text" placeholder="Total">
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <br>
                    <label class="checkbox pull-right">
                        <input type="checkbox" value="">Confirm
                    </label>
                  </div> <!-- End form-actions -->
                  </div> <!-- End tab-pane id=formPayment -->
                <!-- End tab payment -->
                </div><!-- End tab-content -->
              </div><!-- End tabbable -->
            </div><!-- End span8 -->

          <!-- Start booking summary -->
          <div class="span4">
            <div class="new-form">
              {{-- <h3>Booking Summary</h3>
              <br>
              <form class="form-inline">
                <span class="room-header pull-left">Room Total :</span>
                <span class="pull-right">Rp.0</span>
              </form>
              <form class="form-inline">
                <span class="room-header pull-left">Extra Person Total :</span>
                <span class="pull-right">Rp.0</span>
              </form>
              <br>
              <form class="form-inline">
                <span class="room-header pull-left">Extra Total :</span>
                <span class="pull-right">Rp.0</span>
              </form>
              <form class="form-inline">
                <span class="room-header pull-left">Discount Total :</span>
                <span class="pull-right">Rp.0</span>
              </form>
              <br>
              <form class="form-inline">
                <span class="room-header pull-left">Credit Card Surcharges :</span>
                <span class="pull-right">Rp.0</span>
              </form>
              <br>
              <hr>
              <form class="form-inline">
                <span class="room-header pull-left">Total :</span>
                <span class="pull-right">Rp.0</span>
              </form>
              <br>
              <form class="form-inline">
                <span class="room-header pull-left">Total Received:</span>
                <span class="pull-right">Rp.0</span>
              </form>
              <br>
              <hr>
              <form class="form-inline">
                <span class="room-header pull-left">Total Outstanding:</span>
                <span class="pull-right">Rp.0</span>
              </form>
              <br>
              <form class="form-inline">
                <span class="room-header pull-left">Tax Total:</span>
                <span class="pull-right">Rp.0</span>
              </form>--}}
            </div>
          </div>
          <!-- End booking summary -->
        </div><!-- End row-fluid -->

        <div class="form-actions">
          <form class="form-inline">
            <div class="row-fluid">
              {{-- <button type="button" class="btn btn-small btn-success pull-right"
                      style="margin-left:5px;" id="saveReservation"
                      <i class="icon-save" style="margin-right:5px; color: #fff;"></i>
                      <span class="save" style="color:#fff;">
                        Save
                      </span>
              </button> --}}
              <button class="btn btn-small btn-success pull-right"
                      style="margin-left:5px;" type="button">
                      <i class="icon-edit" style="margin-right:5px; color: #fff;"></i>
                      <span class="save" style="color:#fff;">
                      Update
                    </span>
              </button>
              <div class="controls">
                <!-- Button to trigger modal -->
                <a href="#myModal2" role="button"
                   class="btn btn-small btn-inverse pull-right"
                   style="margin-left:5px;" data-toggle="modal">
                      <i class="icon-print" style="margin-right:5px; color: #fff;"></i>
                      <span class="save" style="color:#fff;">Print Invoice
                  </span>
                </a>
              </div>
                  {{-- <!-- controls -->
                  <div class="controls">
                    <!-- Button to trigger modal -->
                    <a href="#myModal3" role="button" class="btn btn-small btn-inverse pull-right" style="margin-left:5px;" data-toggle="modal"><i class="icon-print" style="margin-right:5px; color: #fff;"></i><span class="save" style="color:#fff;">Print Booking Summary</span></a>
                  </div>
                  <!-- /controls --> --}}

                  {{-- <div class="controls">
                    <!-- Button to trigger modal -->
                    <a href="#myModal4" role="button" class="btn btn-small btn-inverse pull-right" style="margin-left:5px;" data-toggle="modal">
                    <i class="icon-print" style="margin-right:5px; color: #fff;"></i>
                    <span class="save" style="color:#fff;">
                      Print Registration Form
                    </span>
                    </a>
                  </div> --}}
                </div>
                </form>
              </div>
              <!-- /widget-content -->
            </div>
            <!-- /widget -->
          </div>
          <!-- /span12 -->
        </div>
        <!-- /row -->
      </div>
      <!-- /container -->

    </div>
  </div>
</div>


{{-- -------------------------- MODALL ------------------------------------------- --}}
{{--
<!-- Modal -->
<div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel2">Invoice</h3>
  </div>
  <div class="modal-body">
    <!-- Start invoice -->
    <h3>No.3455</h3>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Room Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <form class="form-inline">
      <span class="room-header pull-left">Extra Person Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Extra Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <form class="form-inline">
      <span class="room-header pull-left">Discount Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Credit Card Surcharges :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <hr>
    <form class="form-inline">
      <span class="room-header pull-left">Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Total Received:</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <hr>
    <form class="form-inline">
      <span class="room-header pull-left">Total Outstanding:</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Tax Total:</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <!-- End invoice -->
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button class="btn btn-primary">Print</button>
  </div>
</div>
 --}}

<!-- Modal -->
{{-- <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Search Returning Guest</h3>
  </div>
  <div class="modal-body">
    <div class="row-fluid">
      <div class="span12">
        <div class="span2">
          <label>First Name</label>
          <input class="span2" type="text" placeholder="First name">
        </div>
        <div class="span2">
          <label>Last Name</label>
          <input class="span2" type="text" placeholder="Last name">
        </div>
        <div class="span2">
          <label>E-mail</label>
          <input class="span2" type="text" placeholder="E-mail">
        </div>
        <div class="span2">
          <label>Phone</label>
          <input class="span2" type="text" placeholder="Phone">
        </div>
        <div class="span2">
          <input class="btn" type="submit" value="Search">
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary">Save changes</button>
  </div>
</div> --}}
{{--
<!-- Modal -->
<div id="myModal4" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel4">Print Registration Form</h3>
  </div>
  <div class="modal-body">
    <!-- Start invoice -->
    <h3>John Martin</h3>
    <h4>St.Andrews, California</h4>
    <h4>johnmartin@outlook.com</h4>
    <h4>+1-563-098</h4>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Room Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <form class="form-inline">
      <span class="room-header pull-left">Extra Person Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Extra Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <form class="form-inline">
      <span class="room-header pull-left">Discount Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Credit Card Surcharges :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <hr>
    <form class="form-inline">
      <span class="room-header pull-left">Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Total Received:</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <hr>
    <form class="form-inline">
      <span class="room-header pull-left">Total Outstanding:</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Tax Total:</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <!-- End invoice -->
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button class="btn btn-primary">Print</button>
  </div>
</div> --}}

<!-- Modal -->
{{-- <div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel3">Booking Summary</h3>
  </div>
  <div class="modal-body">
    <!-- Start invoice -->
    <h3>John Martin</h3>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Room Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <form class="form-inline">
      <span class="room-header pull-left">Extra Person Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Extra Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <form class="form-inline">
      <span class="room-header pull-left">Discount Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Credit Card Surcharges :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <hr>
    <form class="form-inline">
      <span class="room-header pull-left">Total :</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Total Received:</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <hr>
    <form class="form-inline">
      <span class="room-header pull-left">Total Outstanding:</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <br>
    <form class="form-inline">
      <span class="room-header pull-left">Tax Total:</span>
      <span class="pull-right">Rp.0</span>
    </form>
    <!-- End invoice -->
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button class="btn btn-primary">Print</button>
  </div>
</div> --}}




<script type="text/javascript">
  $.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
 });

  $('#checkinTime').datepicker();
  $('#checkoutTime').datepicker();


  var globalReservationId = $('#reservationId').val();
  $.get('/reservationList/showRoomReservation',{
        'reservationId' : globalReservationId,
      }).done(function(response){
        $('#showRoomReservations').html(response);
  });

  // //  chose room type
  // $('#roomType').change(function(){
  //     var checkinTime = $('#checkinTime').data('date');
  //     var checkoutTime = $('#checkoutTime').data('date');
  //     var idRoomType = $(this).val();
  //     $.post('/reservation/getRoomNumber',{
  //       'idRoomType' : idRoomType,
  //       'checkinTime' : checkinTime,
  //       'checkoutTime' : checkoutTime
  //     }).done(function(response){
  //       $('#showOptionChoseNumberRoom').html(response);
  //     });
  // });
  //
  // // Add Room type
  // $('#addRoom').click(function(){
  //     var roomType = $('#roomType').val();
  //     var roomId = $('#roomId').val();
  //     var checkBreakfast = $('#breakfast').is(":checked");
  //     var breakfast = '';
  //     if(checkBreakfast){
  //       breakfast = 'yes';
  //     }else{
  //       breakfast = 'no';
  //     }
  //
  //     // kirim ke dummy room
  //     $.post('/reservation/addDummyRoomReservation',
  //         {'roomType' : roomType,
  //          'roomId' : roomId,
  //          'breakfast' : breakfast
  //        }).done(function(response){
  //          $('#showDummyReservations').load('/reservation/showDummyRoomReservation');
  //          // tampil dummy reservation
  //        });
  //
  // });
  //
  //
  // // Save Reservation
  // $('#saveReservation').click(function(){
  //     var checkinTime = $('#checkinTime').data('date');
  //     var checkoutTime = $('#checkoutTime').data('date');
  //     var identityId = $('#identityId').val();
  //     var firstName = $('#firstName').val();
  //     var lastName = $('#lastName').val();
  //     var email = $('#email').val();
  //     var phoneNumber = $('#phoneNumber').val();
  //     var street = $('#street').val();
  //     var kelurahan = $('#kelurahan').val();
  //     var city = $('#city').val();
  //     var province = $('#province').val();
  //     var country = $('#country').val();
  //     var note = $('#note').val();
  //
  //     $.get('/reservation/saveReservation',
  //       { 'checkinTime' : checkinTime,
  //         'checkoutTime': checkoutTime,
  //         'identityId' : identityId,
  //         'firstName' : firstName,
  //         'lastName' : lastName,
  //         'email' : email,
  //         'phoneNumber' : phoneNumber,
  //         'street' : street,
  //         'kelurahan' : kelurahan,
  //         'city' : city,
  //         'province' : province,
  //         'country' : country,
  //         'note' : note
  //        }).done(function(response){
  //          console.log(response);
  //          window.location = '/reservation';
  //        });
  // });
  //

</script>
@endsection
